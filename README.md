# Information
## Database
```
# Host: localhost
# User: newuser
# Password: password
# DB: DBNew
```

## Usage
```
# Step 1: clone the repo
git clone https://github.com/vanloiidk/test.git

# Step 2: install necessary modules
cd test

npm i

# Step 3: set environment variables
cp .env.example .env
```

## File Sql
```
Path: docs/mysql-docs/qlytreem.sql

## Available scripts
Start the app
```
npm start
```
Development
```
# run server in watch mode
npm run dev
```

Documentation
```
# generate APIs documentation
npm run apidoc

# open APIs documentation
npm run postdoc

# generate and open APIs documentation
npm run docs
```
