## Before coding
```
git pull master
```

## Conventions
### Branch naming
1. Test:
`test/example-branch`

2. Development:
`dev/example-branch`

3. Production:
`prod/example-branch`

4. Refactor:
`refactor/example-branch`

### Commit format
1. Prefix:

    `feature`: new feature

    `dev`: function or feature that is on work

    `improvement`: change or improvement in current feature

    `fix`: ISSUE resolvement

    `doc`: documentation

    `refactor`: refactoring

2. Commit message example:

    `feature: sign in api`

    `dev ${branchName}`: documenting apis`

    `improvement: change method from GET -> POST`

    `fix: #1`

    `doc: api documentation for GET /v1/auth/signin`

    `refactor: refactor GET /v1/auth/signin`

## Committing
1. Add changes to staging area
``` bash
git checkout -b `${yourBranchName}`

git add `${yourFileName}`

git commit -m `${commitMessage}`
```

2. Push commits in staging area to repo
```
git push origin `${yourBranchName}`
```

3. Require merge
Go to repo and create `PULL REQUEST`