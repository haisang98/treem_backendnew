const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');

class DanhSachHoGiaDinh {
    constructor(sqlConn) {
        this.sqlConn = sqlConn;
    }

    async getData_HoGiaDinh(getData) {
        try {
            let sql = `SELECT person.id_person as id_treem,
                        gd.id_giadinh,
                        (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                        (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                        (SELECT prs.hoten FROM chitietgiadinh as ctgd
                        JOIN person as prs ON prs.id_person = ctgd.id_person
                        WHERE ctgd.id_giadinh = gd.id_giadinh 
                        AND ctgd.id_person <> person.cha 
                        AND ctgd.id_person <> person.me
                        AND ctgd.trangthainuoiduong = 1
                        LIMIT 1
                        ) as nguoinuoiduong,
                        gd.diachi
                        FROM thon as t
                        JOIN giadinh as gd ON t.id_thon = gd.id_thon AND t.daxoa = 0
                        JOIN chitietgiadinh ON chitietgiadinh.id_giadinh = gd.id_giadinh AND chitietgiadinh.id_chucdanh=7
                        JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1)
                        JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
                        JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
                        JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
                        WHERE ${getData}
                        GROUP BY gd.id_giadinh ORDER BY gd.id_giadinh ASC;`;
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async search_HoGiaDinh(search) {
        try {
            let sql = ` SELECT * FROM (
                            SELECT person.id_person as id_treem, 
                            gd.id_giadinh, 
                            (SELECT prs.hoten FROM chitietgiadinh as ctgd
                                JOIN person as prs ON prs.id_person = ctgd.id_person
                                WHERE ctgd.id_giadinh = gd.id_giadinh 
                                AND ctgd.id_person <> person.cha 
                                AND ctgd.id_person <> person.me
                                AND ctgd.trangthainuoiduong = 1
                                LIMIT 1
                            ) as nguoinuoiduong,
                            (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                            (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme, 
                            gd.diachi,
                            person.hoten,
                            ct.id_tinhthanhpho,
                            qh.id_quanhuyen,
                            px.id_phuongxa,
                            t.id_thon

                            FROM thon as t
                            JOIN giadinh as gd ON t.id_thon = gd.id_thon AND t.daxoa = 0
                            JOIN chitietgiadinh ON chitietgiadinh.id_giadinh = gd.id_giadinh AND chitietgiadinh.id_chucdanh=7
                            JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1)
                            JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
                            JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
                            JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho) as info WHERE ${search} 
                            GROUP BY info.id_giadinh ORDER BY info.id_giadinh ASC;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async searchFather(object) {
        try {

            let search = ``
            for(const [key, value] of Object.entries(object)){
                if(value && key !== 'hoten'){
                    search += `person.${key} = ${value} AND `;
                }else if(value && key === 'hoten'){
                    search += `person.${key} like '%${value}'% AND `;
                }
            }

            // let sql = ` SELECT * FROM (
            //                 SELECT person.id_person, 
            //                 gd.id_giadinh, 
            //                 gd.diachi,
            //                 person.hoten,
            //                 person.gioitinh,
            //                 person.ngaysinh,
            //                 person.sodienthoai,
            //                 person.trinhdohocvan,
            //                 person.dantoc,
            //                 phuongxa.id_phuongxa,
            //                 t.id_thon, t.tenthon

            //                 FROM thon as t
            //                 JOIN giadinh as gd ON t.id_thon = gd.id_thon AND t.daxoa = 0
            //                 JOIN phuongxa ON phuongxa.id_phuongxa=t.id_phuongxa
            //                 JOIN chitietgiadinh ON chitietgiadinh.id_giadinh = gd.id_giadinh
            //                 JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1))
            //                 as info WHERE ${search}gioitinh='Nam' AND year(curdate())-year(ngaysinh) - (right(curdate(),5) < right(ngaysinh,5)) >= 20 
            //                 AND id_person IN (SELECT ctgd.id_person FROM chitietgiadinh as ctgd WHERE ctgd.id_chucdanh <> 1) 
            //                 ORDER BY info.id_person ASC;`
            let sql = `SELECT * FROM person WHERE ${search} person.gioitinh='Nam'
            AND year(curdate())-year(person.ngaysinh) - (right(curdate(),5) < right(person.ngaysinh,5)) >= 20
            AND person.id_person NOT IN (SELECT person.id_person FROM person JOIN chitietgiadinh ON person.id_person = chitietgiadinh.id_person
            WHERE person.gioitinh='Nam' AND chitietgiadinh.id_chucdanh = 1 AND year(curdate())-year(person.ngaysinh) - (right(curdate(),5) < right(person.ngaysinh,5)) >= 20);`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async searchMother(object) {
        try {

            let search = ``
            for(const [key, value] of Object.entries(object)){
                if(value && key !== 'hoten'){
                    search += `${key} = ${value} AND `;
                }else if(value && key === 'hoten'){
                    search += `${key} like '%${value}'% AND `;
                }
            }

            // let sql = ` SELECT * FROM (
            //                 SELECT person.id_person, 
            //                 gd.id_giadinh, 
            //                 gd.diachi,
            //                 person.hoten,
            //                 person.gioitinh,
            //                 person.ngaysinh,
            //                 person.sodienthoai,
            //                 person.trinhdohocvan,
            //                 person.dantoc,
            //                 phuongxa.id_phuongxa,
            //                 t.id_thon, t.tenthon

            //                 FROM thon as t
            //                 JOIN giadinh as gd ON t.id_thon = gd.id_thon AND t.daxoa = 0
            //                 JOIN phuongxa ON phuongxa.id_phuongxa=t.id_phuongxa
            //                 JOIN chitietgiadinh ON chitietgiadinh.id_giadinh = gd.id_giadinh
            //                 JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1))
            //                 as info WHERE ${search}gioitinh='Nữ' AND year(curdate())-year(ngaysinh) - (right(curdate(),5) < right(ngaysinh,5)) >= 18 
            //                 AND id_person IN (SELECT ctgd.id_person FROM chitietgiadinh as ctgd WHERE ctgd.id_chucdanh <> 2)
            //                 ORDER BY info.id_person ASC;`
            let sql = `SELECT * FROM person WHERE person.gioitinh='Nữ'
            AND year(curdate())-year(person.ngaysinh) - (right(curdate(),5) < right(person.ngaysinh,5)) >= 18
            AND person.id_person NOT IN (SELECT person.id_person FROM person JOIN chitietgiadinh ON person.id_person = chitietgiadinh.id_person
            WHERE person.gioitinh='Nữ' AND chitietgiadinh.id_chucdanh = 2 AND year(curdate())-year(person.ngaysinh) - (right(curdate(),5) < right(person.ngaysinh,5)) >= 18);`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async searchNguoiNuoi(object) {
        try {

            let search = ``
            for(const [key, value] of Object.entries(object)){
                if(value && key !== 'hoten'){
                    search += `${key} = ${value} AND `;
                }else if(value && key === 'hoten'){
                    search += `${key} like '%${value}'% AND `;
                }
            }

            // let sql = ` SELECT * FROM (
            //                 SELECT person.id_person, 
            //                 gd.id_giadinh, 
            //                 gd.diachi,
            //                 person.hoten,
            //                 person.gioitinh,
            //                 person.ngaysinh,
            //                 person.sodienthoai,
            //                 person.trinhdohocvan,
            //                 person.dantoc,
            //                 phuongxa.id_phuongxa,
            //                 t.id_thon, t.tenthon

            //                 FROM thon as t
            //                 JOIN giadinh as gd ON t.id_thon = gd.id_thon AND t.daxoa = 0
            //                 JOIN phuongxa ON phuongxa.id_phuongxa=t.id_phuongxa
            //                 JOIN chitietgiadinh ON chitietgiadinh.id_giadinh = gd.id_giadinh
            //                 JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1))
            //                 as info WHERE ${search}gioitinh='Nữ' AND year(curdate())-year(ngaysinh) - (right(curdate(),5) < right(ngaysinh,5)) >= 18 
            //                 AND id_person IN (SELECT ctgd.id_person FROM chitietgiadinh as ctgd WHERE ctgd.id_chucdanh <> 2)
            //                 ORDER BY info.id_person ASC;`
            let sql = `SELECT * FROM person WHERE
            AND year(curdate())-year(person.ngaysinh) - (right(curdate(),5) < right(person.ngaysinh,5)) >= 18
            AND person.id_person NOT IN (SELECT person.id_person FROM person JOIN chitietgiadinh ON person.id_person = chitietgiadinh.id_person
            WHERE person.gioitinh='Nữ' AND chitietgiadinh.id_chucdanh = 2 AND year(curdate())-year(person.ngaysinh) - (right(curdate(),5) < right(person.ngaysinh,5)) >= 18);`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
}
module.exports = DanhSachHoGiaDinh;


