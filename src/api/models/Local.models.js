
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');

class Local {
    constructor(sqlConn) {
        this.city = "tinhthanhpho";
        this.district = "quanhuyen";
        this.ward = "phuongxa";
        this.village = "thon";
        this.sqlConn = sqlConn;
    }

    async showListDistrict(thanhpho) {
        try {
            let sql = ` SELECT * 
                        FROM(
                            SELECT 
                                c.id_tinhthanhpho,
                                c.ten_tinhthanhpho,
                                d.id_quanhuyen,
                                d.ten_quanhuyen
                            FROM ${this.city} as c
                            JOIN ${this.district} as d ON c.id_tinhthanhpho = d.id_tinhthanhpho
                        )as info WHERE info.id_tinhthanhpho = ${thanhpho}`

            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async showListWard(huyen) {
        try {
            let sql = ` SELECT * 
                        FROM(
                            SELECT 
                                d.id_quanhuyen,
                                d.ten_quanhuyen,
                                w.id_phuongxa,
                                w.ten_phuongxa
                            FROM ${this.district} as d
                            JOIN ${this.ward} as w ON w.id_quanhuyen = d.id_quanhuyen
                        )as info WHERE info.id_quanhuyen = ${huyen}`

            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async showListVillage(xa) {
        try {
            let sql = ` SELECT * 
                        FROM(
                            SELECT 
                                w.id_phuongxa,
                                w.ten_phuongxa,
                                v.id_thon,
                                v.tenthon,
                                v.daxoa
                            FROM ${this.ward} as w
                            JOIN ${this.village} as v ON w.id_phuongxa = v.id_phuongxa
                        )as info WHERE info.id_phuongxa = ${xa} AND info.daxoa = 0`

            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
}

module.exports = Local;
