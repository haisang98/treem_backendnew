const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');

class Account {
    constructor(sqlConn) {
        this.sqlConn = sqlConn;
    }

    async getListAccount(getData) {
        try {
            let sql = ` SELECT 
                            tk.id_taikhoan,
                            tk.tentaikhoan,
                            tk.tenhienthi,
                            tk.email,
                            tk.dakhoa,
                            dv.id_phuongxa,
                            dv.id_quanhuyen,
                            dv.id_tinhthanhpho
                        FROM taikhoan AS tk
                        JOIN phongban AS pb ON tk.id_phongban = pb.id_phongban
                        JOIN donvi AS dv ON pb.id_donvi = dv.id_donvi
                        WHERE ${getData}
                    `
                    console.log(sql)
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async updateAccount(getData, data) {
        try {
            let sql = ` UPDATE taikhoan, phongban, donvi
                        SET taikhoan.tentaikhoan = "${data.tentaikhoan}", taikhoan.tenhienthi = "${data.tenhienthi}", taikhoan.email = "${data.email}", taikhoan.dakhoa = ${data.dakhoa} 
                        WHERE taikhoan.id_phongban = phongban.id_phongban AND
                        phongban.id_donvi = donvi.id_donvi AND
                        ${getData} AND taikhoan.id_taikhoan = ${data.id_taikhoan}`

            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async updatePasswordAccount(data) {
        try {
            let sql = `UPDATE taikhoan SET matkhau='${data.newPassword}' WHERE id_taikhoan=${data.id_taikhoan};`

            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
}
module.exports = Account;


