const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');


class DanhMuc {
    constructor(sqlConn) {
        this.sqlConn = sqlConn;
    }

    async getData_DM_HCDB() {
        try {
            let sql = `SELECT   id_chitietloaihoancanh as MA_CHI_TIET_HOAN_CANH,
                                ten_hoancanh as TEN_CHI_TIET_HOAN_CANH,
                                cha as ID_CHA,
                                id_loaihoancanh as MA_LOAI_HOAN_CANH
                        FROM chitietloaihoancanh WHERE id_loaihoancanh = 165`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return rows;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getData_DM_NCHCDB() {
        try {
            let sql = `SELECT   id_chitietloaihoancanh as MA_CHI_TIET_HOAN_CANH,
                                ten_hoancanh as TEN_CHI_TIET_HOAN_CANH,
                                cha as ID_CHA,
                                id_loaihoancanh as MA_LOAI_HOAN_CANH
                        FROM chitietloaihoancanh WHERE id_loaihoancanh = 166`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return rows;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getData_DM_HCK() {
        try {
            let sql = `SELECT   id_chitietloaihoancanh as MA_CHI_TIET_HOAN_CANH,
                                ten_hoancanh as TEN_CHI_TIET_HOAN_CANH,
                                cha as ID_CHA,
                                id_loaihoancanh as MA_LOAI_HOAN_CANH
                        FROM chitietloaihoancanh WHERE id_loaihoancanh = 167`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return rows;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getData_DM_HTTG() {
        try {
            let sql = `SELECT   id_hinhthuc as MA_CHI_TIET_HINH_THUC,
                                tenhinhthuc as TEN_CHI_TIET_HINH_THUC,
                                chahinhthuc as ID_CHA
                        FROM hinhthuctrogiup`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return rows;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
}
module.exports = DanhMuc;
