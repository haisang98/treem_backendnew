
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');


class User {
    constructor(sqlConn) {
        this.sqlConn = sqlConn;
    }
    
    async CheckUsername(user) {
        try {
            let Sql = `SELECT * FROM taikhoan WHERE tentaikhoan='${user}'`;
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isLockUser(user) {
        try {
            let Sql = `SELECT * FROM taikhoan WHERE tentaikhoan='${user}' AND dakhoa = 0`;
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async CheckPassword(username, password) {
        try {
            let Sql = `SELECT * FROM taikhoan WHERE tentaikhoan='${username}' AND matkhau='${password}'`;
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async GetLocationUser(userID) {
        try {
            let sqlTinh = `SELECT tinhthanhpho.id_tinhthanhpho as id_tinh, tinhthanhpho.ten_tinhthanhpho as thanhpho
                        FROM taikhoan
                            JOIN phongban ON taikhoan.id_phongban = phongban.id_phongban 
                            JOIN donvi ON phongban.id_donvi = donvi.id_donvi
                            JOIN tinhthanhpho ON tinhthanhpho.id_tinhthanhpho = donvi.id_tinhthanhpho
                        WHERE taikhoan.id_taikhoan = ${userID}`

            let sqlQuan = `SELECT quanhuyen.id_quanhuyen as id_quan, quanhuyen.ten_quanhuyen as quanhuyen
                        FROM taikhoan
                            JOIN phongban ON taikhoan.id_phongban = phongban.id_phongban 
                            JOIN donvi ON phongban.id_donvi = donvi.id_donvi
                            JOIN quanhuyen ON quanhuyen.id_quanhuyen = donvi.id_quanhuyen
                        WHERE taikhoan.id_taikhoan = ${userID}`

            let sqlXa = `SELECT phuongxa.id_phuongxa as id_xa, phuongxa.ten_phuongxa as phuongxa
                        FROM taikhoan
                            JOIN phongban ON taikhoan.id_phongban = phongban.id_phongban 
                            JOIN donvi ON phongban.id_donvi = donvi.id_donvi
                            JOIN phuongxa ON phuongxa.id_phuongxa = donvi.id_phuongxa
                        WHERE taikhoan.id_taikhoan = ${userID}`
            const [tinh, tinhFields] = await this.sqlConn.execute(sqlTinh);
            const [quan, quanFields] = await this.sqlConn.execute(sqlQuan);
            const [xa, xaFields] = await this.sqlConn.execute(sqlXa);

            /* Handle Array Empty */
            if(_.isEmpty(quan)){
                quan.push({id_quan:null, quanhuyen:null})
            }

            if(_.isEmpty(xa)){
                xa.push({id_xa:null, phuongxa:null})
            }

            return [tinh[0], quan[0], xa[0]];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async CreateAndUpdateRefreshToken(username, refreshToken) {
        try {
            let Sql = `UPDATE taikhoan SET RefreshToken = '${refreshToken}' WHERE tentaikhoan='${username}'`;
            await this.sqlConn.execute(Sql);
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async CheckToken(token) {
        try {
            let Sql = `SELECT * FROM taikhoan WHERE RefreshToken='${token}'`;
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async registerUser(data) {
        try {
            let sql = ` INSERT INTO taikhoan(tentaikhoan, matkhau, tenhienthi, email, id_phongban) 
                        VALUES ("${data.username}", "${data.password}", "${data.name}", "${data.email}", ${data.id_phongban})
                        `
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async AddAccessTokenIntoBlacklist(accessToken, exp) {
        try {

            //handle format date
            let d = new Date(0)
            d.setUTCSeconds(exp)
            let year = d.getFullYear()
            let month = d.getMonth()+1
            let day = d.getDate()
            let hours = d.getHours()
            let minute = d.getMinutes()
            let second = d.getSeconds()
            let myDate = `${year}-${month}-${day} ${hours}:${minute}:${second}`

            let sql = ` INSERT INTO blacklist(accessToken, expired) VALUES('${accessToken}','${myDate}')
                        `
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getLocalUser(id) {
        try {
            let sql = ` SELECT * 
                        FROM taikhoan as tk JOIN phongban as pb ON tk.id_phongban = pb.id_phongban JOIN donvi as dv ON pb.id_donvi = dv.id_donvi
                        WHERE tk.id_taikhoan = ${id}
                        `
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async changePassword(id, newPassword) {
        try {
            let sql = `UPDATE taikhoan SET matkhau = '${newPassword}' WHERE id_taikhoan=${id}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

}

module.exports = User;
