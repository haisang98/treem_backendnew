
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');


class NguyCoHCDB {
    constructor(sqlConn) {
        this.person = 'person';
        this.chitietgiadinh = 'chitietgiadinh';
        this.giadinh = 'giadinh';
        this.hoancanh = 'hoancanh';
        this.cthoancanh = 'chitietloaihoancanh';
        this.thon = 'thon';
        this.phuongxa = 'phuongxa';
        this.quanhuyen = 'quanhuyen';
        this.tinhthanhpho = 'tinhthanhpho';
        this.sqlConn = sqlConn;
    }

    async getData_NguyCoHCDB(data) {
        try {
            
            let sql =  `SELECT prs.id_person as id_treem, 
                        prs.hoten, 
                        prs.ngaysinh, 
                        gd.id_giadinh, 
                        (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                        (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                        (SELECT person.hoten FROM chitietgiadinh
                        JOIN person ON person.id_person = chitietgiadinh.id_person
                        WHERE chitietgiadinh.id_giadinh = gd.id_giadinh 
                        AND chitietgiadinh.id_person <> prs.cha 
                        AND chitietgiadinh.id_person <> prs.me
                        AND chitietgiadinh.trangthainuoiduong = 1
                        LIMIT 1
                        ) as nguoinuoiduong,
                        prs.dantoc, 
                        prs.gioitinh,
                        t.tenthon,
                        px.ten_phuongxa,
                        qh.ten_quanhuyen,
                        ct.ten_tinhthanhpho,
                        hc.id_hoancanh

                        FROM ${this.person} as prs
                        JOIN ${this.chitietgiadinh} as ctgd ON ctgd.id_person=prs.id_person AND ctgd.id_chucdanh=7
                        JOIN ${this.giadinh} as gd ON ctgd.id_giadinh = gd.id_giadinh
                        JOIN ${this.thon} as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
                        JOIN ${this.phuongxa} as px ON t.id_phuongxa = px.id_phuongxa
                        JOIN ${this.quanhuyen} as qh ON px.id_quanhuyen = qh.id_quanhuyen
                        JOIN ${this.tinhthanhpho} as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
                        JOIN ${this.cthoancanh} as cthc ON prs.id_person = cthc.id_person
                        JOIN ${this.hoancanh} as hc ON hc.id_hoancanh = cthc.id_hoancanh 
                        AND hc.id_loaihoancanh = 166
                        WHERE ${data}
                        ORDER BY prs.id_person ASC`;
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getData_DM_NCHCDB() {
        try {
            let sql = "SELECT * FROM hoancanh WHERE cha = 0 AND id_loaihoancanh = 166"
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getData_DM_CT_NCHCDB() {
        try {
            let sql = `SELECT * FROM chitietloaihoancanh JOIN hoancanh ON hoancanh.id_hoancanh=chitietloaihoancanh.id_hoancanh WHERE hoancanh.id_loaihoancanh = 166 ORDER BY chitietloaihoancanh.id_person ASC`;
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async searchData_NCHCDB(search) {
        
        try {
            let sql = ` SELECT * FROM (SELECT prs.id_person as id_treem, 
                prs.hoten, 
                prs.ngaysinh, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                prs.dantoc, 
                prs.gioitinh,
                t.tenthon,t.id_thon,
                px.ten_phuongxa, px.id_phuongxa,
                qh.ten_quanhuyen, qh.id_quanhuyen,
                ct.ten_tinhthanhpho, ct.id_tinhthanhpho,
                hc.id_hoancanh

            FROM ${this.person} as prs
            JOIN ${this.chitietgiadinh} as ctgd ON ctgd.id_person=prs.id_person AND ctgd.id_chucdanh=7
            JOIN ${this.giadinh} as gd ON ctgd.id_giadinh = gd.id_giadinh
            JOIN ${this.thon} as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
            JOIN ${this.phuongxa} as px ON t.id_phuongxa = px.id_phuongxa
            JOIN ${this.quanhuyen} as qh ON px.id_quanhuyen = qh.id_quanhuyen
            JOIN ${this.tinhthanhpho} as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
            JOIN ${this.cthoancanh} as cthc ON prs.id_person = cthc.id_person
            JOIN ${this.hoancanh} as hc ON hc.id_hoancanh = cthc.id_hoancanh 
            AND hc.id_loaihoancanh = 166) as info WHERE ${search} ORDER BY info.id_treem ASC`
            const [rows, fields] = await this.sqlConn.execute(sql);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async searchData_DM_CT_NCHCDB(search) {
        try {
            let sql = ` SELECT * FROM (SELECT prs.id_person as id_treem, 
                prs.hoten, 
                prs.ngaysinh, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                prs.dantoc, 
                prs.gioitinh,
                t.tenthon,t.id_thon,
                px.ten_phuongxa, px.id_phuongxa,
                qh.ten_quanhuyen, qh.id_quanhuyen,
                ct.ten_tinhthanhpho, ct.id_tinhthanhpho,
                hc.id_hoancanh, hc.ten_hoancanh, hc.cha

            FROM ${this.person} as prs
            JOIN ${this.chitietgiadinh} as ctgd ON ctgd.id_person=prs.id_person AND ctgd.id_chucdanh=7
            JOIN ${this.giadinh} as gd ON ctgd.id_giadinh = gd.id_giadinh
            JOIN ${this.thon} as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
            JOIN ${this.phuongxa} as px ON t.id_phuongxa = px.id_phuongxa
            JOIN ${this.quanhuyen} as qh ON px.id_quanhuyen = qh.id_quanhuyen
            JOIN ${this.tinhthanhpho} as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
            JOIN ${this.cthoancanh} as cthc ON prs.id_person = cthc.id_person
            JOIN ${this.hoancanh} as hc ON hc.id_hoancanh = cthc.id_hoancanh 
            AND hc.id_loaihoancanh = 166) as info WHERE ${search} ORDER BY info.id_treem ASC`

            const [rows, fields] = await this.sqlConn.execute(sql);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

}
module.exports = NguyCoHCDB;
