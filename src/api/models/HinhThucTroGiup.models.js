const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');

class HinhThucTroGiup {
    constructor(sqlConn) {
        this.person = 'person';
        this.giadinh = 'giadinh';
        this.chitietgiadinh = 'chitietgiadinh';
        this.pbtrogiup = 'phanbotrogiup';
        this.httrogiup = 'hinhthuctrogiup';
        this.thon = 'thon';
        this.phuongxa = 'phuongxa';
        this.quanhuyen = 'quanhuyen';
        this.tinhthanhpho = 'tinhthanhpho';
        this.sqlConn = sqlConn;

    }

    async getData_HinhThucTroGiup(data) {
        
        try {
            let sql =  `SELECT prs.id_person as id_treem, 
                        prs.hoten, 
                        prs.ngaysinh, 
                        gd.id_giadinh, 
                        (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                        (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                        (SELECT person.hoten FROM chitietgiadinh
                        JOIN person ON person.id_person = chitietgiadinh.id_person
                        WHERE chitietgiadinh.id_giadinh = gd.id_giadinh 
                        AND chitietgiadinh.id_person <> prs.cha 
                        AND chitietgiadinh.id_person <> prs.me
                        AND chitietgiadinh.trangthainuoiduong = 1
                        LIMIT 1
                        ) as nguoinuoiduong,
                        prs.dantoc, 
                        prs.gioitinh,
                        t.tenthon,
                        px.ten_phuongxa,
                        qh.ten_quanhuyen,
                        ct.ten_tinhthanhpho,
                        httg.id_hinhthuc

                        FROM ${this.person} as prs
                        JOIN ${this.chitietgiadinh} as ctgd ON ctgd.id_person=prs.id_person AND ctgd.id_chucdanh=7
                        JOIN ${this.giadinh} as gd ON ctgd.id_giadinh = gd.id_giadinh
                        JOIN ${this.thon} as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
                        JOIN ${this.phuongxa} as px ON t.id_phuongxa = px.id_phuongxa
                        JOIN ${this.quanhuyen} as qh ON px.id_quanhuyen = qh.id_quanhuyen
                        JOIN ${this.tinhthanhpho} as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
                        JOIN ${this.pbtrogiup} as pbtg ON prs.id_person = pbtg.id_person
                        JOIN ${this.httrogiup} as httg ON pbtg.id_hinhthuc = httg.id_hinhthuc 
                        WHERE ${data}
                        ORDER BY prs.id_person ASC`;

            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async getData_DM_HTTG() {
        try {
            let sql = "SELECT * FROM hinhthuctrogiup WHERE `chahinhthuc` = 0"

            const [rows, fields] = await this.sqlConn.execute(sql);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getData_DM_CT_HTTG() {
        try {
            let sql = "SELECT * FROM `hinhthuctrogiup` as ht JOIN `phanbotrogiup` as pb ON ht.id_hinhthuc = pb.id_hinhthuc ORDER BY pb.id_person ASC"

            const [rows, fields] = await this.sqlConn.execute(sql);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async searchData_HTTG(search) {
        
        try {
            let sql = ` SELECT * FROM (SELECT prs.id_person as id_treem, 
                prs.hoten, 
                prs.ngaysinh, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                prs.dantoc, 
                prs.gioitinh,
                t.tenthon,t.id_thon,
                px.ten_phuongxa, px.id_phuongxa,
                qh.ten_quanhuyen, qh.id_quanhuyen,
                ct.ten_tinhthanhpho, ct.id_tinhthanhpho,
                httg.id_hinhthuc

            FROM ${this.person} as prs
            JOIN ${this.chitietgiadinh} as ctgd ON ctgd.id_person=prs.id_person AND ctgd.id_chucdanh=7
            JOIN ${this.giadinh} as gd ON ctgd.id_giadinh = gd.id_giadinh
            JOIN ${this.thon} as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
            JOIN ${this.phuongxa} as px ON t.id_phuongxa = px.id_phuongxa
            JOIN ${this.quanhuyen} as qh ON px.id_quanhuyen = qh.id_quanhuyen
            JOIN ${this.tinhthanhpho} as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
            JOIN ${this.pbtrogiup} as pbtg ON prs.id_person = pbtg.id_person
            JOIN ${this.httrogiup} as httg ON pbtg.id_hinhthuc = httg.id_hinhthuc) as info 
            WHERE ${search} ORDER BY info.id_treem ASC`

            const [rows, fields] = await this.sqlConn.execute(sql);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async searchData_DM_CT_HTTG(search) {
        
        try {
            let sql = ` SELECT * FROM (SELECT prs.id_person as id_treem, 
                prs.hoten, 
                prs.ngaysinh, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                prs.dantoc, 
                prs.gioitinh,
                t.tenthon,t.id_thon,
                px.ten_phuongxa, px.id_phuongxa,
                qh.ten_quanhuyen, qh.id_quanhuyen,
                ct.ten_tinhthanhpho, ct.id_tinhthanhpho,
                httg.id_hinhthuc, httg.chahinhthuc, httg.tenhinhthuc

            FROM ${this.person} as prs
            JOIN ${this.chitietgiadinh} as ctgd ON ctgd.id_person=prs.id_person AND ctgd.id_chucdanh=7
            JOIN ${this.giadinh} as gd ON ctgd.id_giadinh = gd.id_giadinh
            JOIN ${this.thon} as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
            JOIN ${this.phuongxa} as px ON t.id_phuongxa = px.id_phuongxa
            JOIN ${this.quanhuyen} as qh ON px.id_quanhuyen = qh.id_quanhuyen
            JOIN ${this.tinhthanhpho} as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
            JOIN ${this.pbtrogiup} as pbtg ON prs.id_person = pbtg.id_person
            JOIN ${this.httrogiup} as httg ON pbtg.id_hinhthuc = httg.id_hinhthuc) as info 
            WHERE ${search} ORDER BY info.id_treem ASC`

            const [rows, fields] = await this.sqlConn.execute(sql);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isExists(id) {
        try {
            let sql = `SELECT * FROM hinhthuctrogiup WHERE id_hinhthuc=${id}`
            const [rows, fields] = await this.sqlConn.execute(sql);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    

}
module.exports = HinhThucTroGiup;


