
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');


class QuanLyThon {
    constructor(sqlConn) {
        this.sqlConn = sqlConn;
    }

    async getThon(id) {
        try {
            let sql = ` SELECT t.* FROM thon as t 
                        JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
                        JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
                        JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
                        WHERE px.id_phuongxa = ${id} AND daxoa = 0
                    `
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async addThon(data) {
        try {
            let sql = `INSERT INTO thon(tenthon, id_phuongxa) VALUES ("${data.tenthon}", ${data.id_phuongxa})`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async deleteThon(data) {
        try {
            let sql = `UPDATE thon SET daxoa = 1 WHERE id_phuongxa = ${data.id_phuongxa} AND id_thon = ${data.id}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async updateThon(data) {
        try {
            let sql = `UPDATE thon SET tenthon = '${data.tenthon}' WHERE id_phuongxa = ${data.id_phuongxa} AND id_thon = ${data.id}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async mergeThon(id, listThon) {
        try {
            const length_ = listThon.length;
            if(length_ > 0){
                listThon.forEach(async (value) => {
                    let Sql =  `SELECT giadinh.id_giadinh FROM giadinh JOIN thon ON giadinh.id_thon=thon.id_thon WHERE giadinh.id_thon=${value};`
                    const [rows, fields] = await this.sqlConn.execute(Sql);
                    let Sql2 = `UPDATE thon SET daxoa=1 WHERE id_thon=${value}`;
                    const [rows2, fields2] = await this.sqlConn.execute(Sql2);
                    rows.forEach(async ({id_giadinh}) => {
                        let Sql_ = `UPDATE giadinh SET id_thon=${id} WHERE id_giadinh=${id_giadinh}`
                        await this.sqlConn.execute(Sql_);
                    })
                })

                return 1;
            }else{
                return 0;
            }
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isCheckChildrenHaveExists(id_thon) {

        /* chuc danh === 7 is children */

        try {
            let sql = `SELECT * FROM thon
                        JOIN giadinh ON thon.id_thon = giadinh.id_thon
                        JOIN chitietgiadinh ON chitietgiadinh.id_giadinh= giadinh.id_giadinh
                        JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1)
                        WHERE thon.id_thon=${id_thon} AND chitietgiadinh.id_chucdanh=7;`            
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
}
module.exports = QuanLyThon;
