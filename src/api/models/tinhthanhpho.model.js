
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');


class TinhThanhPho {
    constructor(sqlConn) {
        this.table = 'tinhthanhpho';
        this.sqlConn = sqlConn;

    }

    // Function Show Lists Thanh Pho
    async showListTP() {
        try {
            let Sql = `SELECT * FROM ${this.table}`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    // Function Add Thon/Xa 
    async addTinhThanhPho(data) {
        try {
            let Sql = `INSERT INTO ${this.table}(ten_tinhthanhpho) VALUES(?)`;
            let ArraySql = [data];
            const [rows, fields] = await this.sqlConn.execute(Sql,ArraySql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

}

module.exports = TinhThanhPho;
