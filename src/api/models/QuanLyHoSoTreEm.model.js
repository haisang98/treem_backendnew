
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');


class QuanLyHoSoTreEm {
    constructor(sqlConn) {
        this.sqlConn = sqlConn;
    }

    async getData(Object) {
        try {

            let where = "WHERE "
            if(Object.id_phuongxa){
                where += "px.id_phuongxa = " + Object.id_phuongxa + " AND "
            }
            if(Object.id_quanhuyen){
                where += "qh.id_quanhuyen = " + Object.id_quanhuyen + " AND "
            }
            if(Object.id_tinhthanhpho){
                where += "ct.id_tinhthanhpho = " + Object.id_tinhthanhpho
            }

            let Sql = `SELECT person.id_person as id_treem,
            gd.id_giadinh,
            person.hoten,
            person.ngaysinh,
            (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
            (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
            (SELECT prs.hoten FROM chitietgiadinh as ctgd
            JOIN person as prs ON prs.id_person = ctgd.id_person
            WHERE ctgd.id_giadinh = gd.id_giadinh 
            AND ctgd.id_person <> person.cha 
            AND ctgd.id_person <> person.me
            AND ctgd.trangthainuoiduong = 1
            LIMIT 1
            ) as nguoinuoiduong,
            person.dantoc,
            person.gioitinh,
            ct.ten_tinhthanhpho,
            qh.ten_quanhuyen,
            px.ten_phuongxa,
            t.tenthon,
            gd.diachi
            FROM thon as t
            JOIN giadinh as gd ON t.id_thon = gd.id_thon AND t.daxoa = 0
            JOIN chitietgiadinh ON chitietgiadinh.id_giadinh = gd.id_giadinh AND chitietgiadinh.id_chucdanh=7
            JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1)
            JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
            JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
            JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho
            WHERE ${getData}
            ORDER BY person.id_person ASC;`;
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    // Funtion get Total children on Server
    async getTotal() {
        try {
            let Sql =  `SELECT COUNT(person.id_person) AS total FROM person 
                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
                        JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh 
                        JOIN thon ON giadinh.id_thon=thon.id_thon AND thon.daxoa = 0 AND (person.trangthai=0 OR person.trangthai=1)
                        JOIN phuongxa ON thon.id_phuongxa=phuongxa.id_phuongxa 
                        JOIN quanhuyen ON phuongxa.id_quanhuyen=quanhuyen.id_quanhuyen 
                        JOIN tinhthanhpho ON quanhuyen.id_tinhthanhpho=tinhthanhpho.id_tinhthanhpho;`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return rows[0].total;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async searchInfoChildren(data) {
        try {

            const {
                tinhthanhpho,
                quanhuyen,
                phuongxa,
                thon,
                hoten,
                nguoinuoi,
                id_giadinh,
                thungrac,
                dantoc,
                gioitinh
            } = data
            
            /* handle id_giadinh */
            const magiadinh = id_giadinh==undefined ?
             `` : (isNaN(id_giadinh) ? ` AND (giadinh.id_giadinh=-123456789)` : ` AND (giadinh.id_giadinh=${id_giadinh})`);

            
             let Sql =  `SELECT person.id_person as id_treem, giadinh.id_giadinh, person.hoten,
                        person.ngaysinh, 
                        (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                        (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                        (SELECT prs.hoten FROM chitietgiadinh as ctgd
                        JOIN person as prs ON prs.id_person = ctgd.id_person
                        WHERE ctgd.id_giadinh = giadinh.id_giadinh 
                        AND ctgd.id_person <> person.cha 
                        AND ctgd.id_person <> person.me
                        AND ctgd.trangthainuoiduong = 1
                        LIMIT 1
                        ) as nguoinuoiduong,
                        person.dantoc, person.gioitinh, 
                        thon.tenthon, phuongxa.ten_phuongxa, quanhuyen.ten_quanhuyen, 
                        tinhthanhpho.ten_tinhthanhpho
                        FROM person
                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
                        JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                        JOIN thon ON giadinh.id_thon=thon.id_thon AND thon.daxoa = 0
                        JOIN phuongxa ON thon.id_phuongxa=phuongxa.id_phuongxa 
                        JOIN quanhuyen ON phuongxa.id_quanhuyen=quanhuyen.id_quanhuyen 
                        JOIN tinhthanhpho ON quanhuyen.id_tinhthanhpho=tinhthanhpho.id_tinhthanhpho
                        WHERE ` + (tinhthanhpho==undefined?``:`(tinhthanhpho.id_tinhthanhpho='${tinhthanhpho}')`) 
                        + (quanhuyen==undefined?``:` AND (quanhuyen.id_quanhuyen='${quanhuyen}')`)
                        + (phuongxa==undefined?``:` AND (phuongxa.id_phuongxa='${phuongxa}')`) 
                        + (thon==undefined?``:` AND (thon.id_thon='${thon}')`)
                        + (hoten==undefined?``:` AND (person.hoten like '%${hoten}%')`) 
                        + (nguoinuoi==undefined?``:` AND (person.cha IN (SELECT person.id_person FROM person 
                                                                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person
                                                                        AND chitietgiadinh.id_chucdanh = 1
                                                                        WHERE hoten like '%${nguoinuoi}%') 
                                                    OR person.me IN (SELECT person.id_person FROM person 
                                                                    JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person
                                                                    AND chitietgiadinh.id_chucdanh = 2
                                                                    WHERE hoten like '%${nguoinuoi}%'))`)
                        + (magiadinh) 
                        + (thungrac==undefined?` AND (person.trangthai=0 OR person.trangthai=1)`: (thungrac==1 ? ` AND (person.trangthai=0 OR person.trangthai=1)` : ` AND (person.trangthai=2)`)) 
                        + (dantoc==undefined?``:` AND (person.dantoc='${dantoc}')`) 
                        + (gioitinh==undefined?``:` AND (person.gioitinh='${gioitinh}')`) + `;`


            //Handle String
            let indexWhere = Sql.indexOf("WHERE")+6
            let indexAnd = indexWhere+4
            let stringSlice = Sql.slice(indexWhere,indexAnd)
            if(stringSlice==" AND"){
                let start = Sql.slice(0,indexWhere)
                let end = Sql.slice(indexAnd+1)
                Sql = start.concat(end)
            }

            //Handle IF (All Field == undefined)
            let count = Object.keys(data).length;
            let countTemp = 0
            for(var k in data){
                if(data[k]==undefined){
                    countTemp += 1;
                }
            }

            if(count == countTemp){
                let indexWhere = Sql.indexOf("WHERE")
                Sql = Sql.slice(0,indexWhere) + `;`
            }
            console.log(Sql)
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getInfoPerson(id) {
        try {
            let Sql = `SELECT * FROM person WHERE id_person=${id}`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getInfoChildren(id, object) {
        try {

            let where = ""
            if(object.phuongxa){
                where += "phuongxa.id_phuongxa = " + object.phuongxa + " AND "
            }
            if(object.quanhuyen){
                where += "quanhuyen.id_quanhuyen = " + object.quanhuyen + " AND "
            }
            if(object.tinhthanhpho){
                where += "tinhthanhpho.id_tinhthanhpho = " + object.tinhthanhpho
            }

            let Sql =  `SELECT tinhthanhpho.ten_tinhthanhpho, tinhthanhpho.id_tinhthanhpho, quanhuyen.ten_quanhuyen, quanhuyen.id_quanhuyen, phuongxa.ten_phuongxa, phuongxa.id_phuongxa, 
                               thon.tenthon, thon.id_thon, giadinh.id_giadinh,
                               (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                               (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                               (SELECT prs.hoten FROM chitietgiadinh as ctgd
                               JOIN person as prs ON prs.id_person = ctgd.id_person
                               WHERE ctgd.id_giadinh = giadinh.id_giadinh
                               AND ctgd.id_chucdanh = 4 
                               AND ctgd.trangthainuoiduong = 1
                               LIMIT 1
                               ) as nguoinuoiduong,
                               (SELECT prs.id_person FROM chitietgiadinh as ctgd
                                JOIN person as prs ON prs.id_person = ctgd.id_person
                                WHERE ctgd.id_giadinh = giadinh.id_giadinh 
                                AND ctgd.id_chucdanh = 4
                                AND ctgd.trangthainuoiduong = 1
                                LIMIT 1
                                ) as idNguoiNuoi, 
                               (SELECT prs.sodienthoai FROM person as prs 
                                JOIN chitietgiadinh as ctgd ON ctgd.id_person=prs.id_person 
                                WHERE ctgd.id_giadinh = giadinh.id_giadinh AND ctgd.trangthainuoiduong = 1 AND prs.sodienthoai IS NOT NULL LIMIT 1
                               ) as sodienthoai,
                               giadinh.diachi, hoancanhgiadinh.id_hoancanhgiadinh as hoancanh, hoancanhgiadinh.ten_hoancanhgiadinh, 
                               person.id_person as id_treem, person.ngaysinh, person.cmnd, person.gioitinh, person.cha, person.me,
                               person.hoten, person.dantoc, person.trinhdohocvan FROM person
                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
                        JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                        JOIN hoancanhgiadinh ON hoancanhgiadinh.id_hoancanhgiadinh=giadinh.id_hoancanhgiadinh
                        JOIN thon ON giadinh.id_thon=thon.id_thon AND thon.daxoa=0 AND (person.trangthai=0 OR person.trangthai=1)
                        JOIN phuongxa ON thon.id_phuongxa=phuongxa.id_phuongxa
                        JOIN quanhuyen ON phuongxa.id_quanhuyen=quanhuyen.id_quanhuyen
                        JOIN tinhthanhpho ON quanhuyen.id_tinhthanhpho=tinhthanhpho.id_tinhthanhpho
                        WHERE person.id_person=${id} AND ${where};`
                        console.log(Sql)
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getChildrenHCDB() {
        try {
            let Sql = `SELECT * FROM hoancanh WHERE id_loaihoancanh=165`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async cloneGetChildrenHCDB() {
        try {
            let Sql = `SELECT id_hoancanh as value, ten_hoancanh as title, cha, id_loaihoancanh FROM hoancanh WHERE id_loaihoancanh=165`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async cloneGetChildrenNCHCDB() {
        try {
            let Sql = `SELECT id_hoancanh as value, ten_hoancanh as title, cha, id_loaihoancanh FROM hoancanh WHERE id_loaihoancanh=166`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getChildrenNCHCDB() {
        try {
            let Sql = `SELECT * FROM hoancanh WHERE id_loaihoancanh=166`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async cloneGetChildrenHCK() {
        try {
            let Sql = `SELECT id_hoancanh as value, ten_hoancanh as title, cha, id_loaihoancanh FROM hoancanh WHERE id_loaihoancanh=167`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getChildrenHCK() {
        try {
            let Sql = `SELECT * FROM hoancanh WHERE id_loaihoancanh=167`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getIdChildren(id) {
        try {
            let Sql = `SELECT hoancanh.id_hoancanh as id_, hoancanh.cha as parent, hoancanh.ten_hoancanh as name,
                              hoancanh.id_loaihoancanh as id_lhc
                       FROM person
                       JOIN chitietloaihoancanh ON chitietloaihoancanh.id_person=person.id_person
                       JOIN hoancanh ON hoancanh.id_hoancanh=chitietloaihoancanh.id_hoancanh
                       JOIN loaihoancanh ON loaihoancanh.id_loaihoancanh=hoancanh.id_loaihoancanh
                       WHERE person.id_person=${id};`         
                       let Sql2 = `SELECT hinhthuctrogiup.id_hinhthuc, hinhthuctrogiup.tenhinhthuc, hinhthuctrogiup.chahinhthuc
                       FROM person
                       JOIN chitietloaihoancanh ON chitietloaihoancanh.id_person=person.id_person
                       JOIN phanbotrogiup ON phanbotrogiup.id_person=person.id_person
                       JOIN hinhthuctrogiup ON hinhthuctrogiup.id_hinhthuc=phanbotrogiup.id_hinhthuc
                       WHERE person.id_person=${id};`  
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getIdChildrenHTTG(id) {
        try {        
                let Sql = `SELECT hinhthuctrogiup.id_hinhthuc, hinhthuctrogiup.tenhinhthuc, hinhthuctrogiup.chahinhthuc
                       FROM person
                       JOIN chitietloaihoancanh ON chitietloaihoancanh.id_person=person.id_person
                       JOIN phanbotrogiup ON phanbotrogiup.id_person=person.id_person
                       JOIN hinhthuctrogiup ON hinhthuctrogiup.id_hinhthuc=phanbotrogiup.id_hinhthuc
                       WHERE person.id_person=${id};`  
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getAllHTTG() {
        try {
            let Sql =  `SELECT * FROM hinhthuctrogiup`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async cloneGetAllHTTG() {
        try {
            let Sql =  `SELECT id_hinhthuc as value, tenhinhthuc as title, chahinhthuc FROM hinhthuctrogiup`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    //  Functions Add Tình trạng học tập và return idTTHT
    async ThemTinhTrangHocTap_WithFamily_OtherFamily(data) {
        try {
            // Handle If data is undefined
            if(data.lophoccaonhat==undefined)
                data.lophoccaonhat = ''
            if(data.tinhtranghoctap==undefined)
                data.tinhtranghoctap = ''

            let Sql =  `INSERT INTO tinhtranghoctap(lophoccaonhat,tinhtranghoctap) VALUES('${data.lophoccaonhat}','${data.tinhtranghoctap}');`
            let Select_Id = `select last_insert_id() as id;`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            const [id, fieldId] = await this.sqlConn.execute(Select_Id);
            // Convert array => object example: {'0' : ...}
            const resultId = { ...id }
            return resultId['0'].id
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    // Function Add Children And return idChildren cho chức năng nhập liệu trẻ em cùng gia đình
    async EnterInfoChildren_WithFamily(data) {
        try {

            //Handle IF data is Undefined
            data.ngaysinh = data.ngaysinh==undefined ? '' : data.ngaysinh
            data.ghichu = data.ghichu==undefined ? '' : data.ghichu
            data.trinhdohocvan = data.trinhdohocvan==undefined ? null : data.trinhdohocvan 

            let Sql =  `INSERT INTO person(hoten,ngaysinh,gioitinh,dantoc,trinhdohocvan,ghichu,cha,me) 
                        VALUES('${data.hoten}',
                               '${data.ngaysinh}',
                               '${data.gioitinh}',
                               '${data.dantoc}',
                               '${data.trinhdohocvan}',
                               '${data.ghichu}',
                                ${data.cha},
                                ${data.me}
                               );`

            // let sqlIdChildren = `select last_insert_id() as id;`;
            const [rows, fields] = await this.sqlConn.execute(Sql);
            // const [idChildren, fieldsChildren] = await this.sqlConn.execute(sqlIdChildren);
            const resultId = { ...rows }
            return resultId.insertId
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async AddChildrenIntoFamily__(idChildren, idFamily) {
        try {

            let Sql =  `INSERT INTO chitietgiadinh(id_person, id_giadinh, id_chucdanh, trangthainuoiduong) 
                        VALUES(${idChildren}, ${idFamily}, 7, 0);`
            
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    // Function Add Hình thức trợ giúp cho Cùng gia đình và Khác gia đình
    async AddHTTG_WithFamily_OtherFamily(data, idChildren) {
        try {
            let arrayIdHTTG = data.id_trogiup.length;
            if(arrayIdHTTG > 0){
                data.id_trogiup.forEach(async (value) => {
                    let Sql =  `INSERT INTO phanbotrogiup(id_person,id_hinhthuc) VALUES(${idChildren},${value});`
                    let [rows, fields] = await this.sqlConn.execute(Sql);
                })            
                return 1;
            }
            return 0;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
    // Function add Hoàn cảnh cho cả cùng gia đình và khác gia đình
    async AddHoanCanh_WithFamily_OtherFamily(data, idChildren) {
        try {
            let arrayIdHoanCanh = data.id_hoancanh.length;
            if(arrayIdHoanCanh > 0){
                data.id_hoancanh.forEach(async (value) => {
                    let Sql =  `INSERT INTO chitietloaihoancanh(id_person,id_hoancanh) VALUES(${idChildren},${value});`
                    let [rows, fields] = await this.sqlConn.execute(Sql);
                })            
                return 1;
            }
            return 0;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }



    // Functions Add gia đình và return về idFamily cho chức năng gia đình khác
    async AddFamily_OtherFamily(data) {
        try {

            data.nguoinuoiduong = data.nguoinuoiduong==undefined ? '' : data.nguoinuoiduong
            data.hotencha = data.hotencha==undefined ? '' : data.hotencha
            data.hotenme = data.hotenme==undefined ? '' : data.hotenme
            data.sodienthoai = data.sodienthoai==undefined ? '' : data.sodienthoai
            data.diachi = data.diachi==undefined ? '' : data.diachi
            data.hoancanh = data.hoancanh==undefined ? '' : data.hoancanh
            
            // let Sql =  `INSERT INTO giadinh(nguoinuoiduong,hotencha,hotenme,sodienthoai,diachi,hoancanh,id_thon) 
            //             VALUES('${data.nguoinuoiduong}',
            //                    '${data.hotencha}',
            //                    '${data.hotenme}',
            //                    '${data.sodienthoai}',
            //                    '${data.diachi}',
            //                    '${data.hoancanh}',
            //                    '${data.id_thon}');`
            let Sql = `INSERT INTO giadinh(diachi, id_hoancanhgiadinh, id_thon)
                      VALUES('${data.diachi}', ${data.hoancanh}, ${data.id_thon})`
            // let Sql_Id =`select last_insert_id() as id;`;

            let [rows, fields] = await this.sqlConn.execute(Sql);
            // let [getIdFamily,fields1] = await this.sqlConn.execute(Sql_Id);
            const resultId = { ...rows }
            return resultId.insertId

        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    /* Add Member !== children into Family */
    async AddMember_OtherFamily(data, id, idChildren) {
        try {
            
            let Sql = `INSERT INTO chitietgiadinh(id_giadinh, id_person, id_chucdanh, trangthainuoiduong)
            VALUES `;

            for(const [key, value] in Object.entries(data)){
                if(value && key === 'cha'){
                    Sql += `(${id}, ${value}, 1, 1),`
                }else if(value && key === 'me'){
                    Sql += `(${id}, ${value}, 2, 1),`
                }
            }

            Sql += `(${id}, ${idChildren}, 7, 0);`;
            
            // let Sql =  `INSERT INTO giadinh(nguoinuoiduong,hotencha,hotenme,sodienthoai,diachi,hoancanh,id_thon) 
            //             VALUES('${data.nguoinuoiduong}',
            //                    '${data.hotencha}',
            //                    '${data.hotenme}',
            //                    '${data.sodienthoai}',
            //                    '${data.diachi}',
            //                    '${data.hoancanh}',
            //                    '${data.id_thon}');`
            // let Sql_Id =`select last_insert_id() as id;`;

            let [rows, fields] = await this.sqlConn.execute(Sql);
            // let [getIdFamily,fields1] = await this.sqlConn.execute(Sql_Id);
            // const resultId = { ...getIdFamily }
            // return resultId['0'].id
            return [rows, fields];

        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    //Function nhập liệu trẻ em và trả về  idChildren cho chức năng nhập liệu trẻ em cho gia đình khác
    async EnterInfoChildren_OtherFamily(data) {
        try {
            data.ngaysinh = data.ngaysinh==undefined ? '' : data.ngaysinh
            data.ghichu = data.ghichu==undefined ? null : data.ghichu
            data.cha = data.cha==undefined ? null : (!data.cha ? null : data.cha)
            data.me = data.me==undefined ? null : (!data.me ? null : data.me)
            data.trinhdohocvan = data.trinhdohocvan==undefined ? null : data.trinhdohocvan

            let Sql =  `INSERT INTO person(hoten,ngaysinh,gioitinh,dantoc,trinhdohocvan,ghichu,cha,me) 
                        VALUES('${data.hoten}',
                               '${data.ngaysinh}',
                               '${data.gioitinh}',
                               '${data.dantoc}',
                               '${data.trinhdohocvan}',
                               '${data.ghichu}',
                                ${data.cha},
                                ${data.me});`
                                console.log(Sql);
            const [rows, fields] = await this.sqlConn.execute(Sql);
            const resultId = { ...rows }
            return resultId.insertId;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    // Function handle Delete Physical Children
    async physicalDeleteChildren(id_treem) {
        try {
            // let SqlSearchTTHT = `SELECT * FROM person WHERE id_person=${id_treem}`;
            // const [idTTHT, idHTTHTFields] = await this.sqlConn.execute(SqlSearchTTHT);

            let Sql1 =  `DELETE FROM chitietloaihoancanh WHERE id_person = ${id_treem}`
            await this.sqlConn.execute(Sql1);

            let Sql2 =  `DELETE FROM phanbotrogiup WHERE id_person = ${id_treem}`
            await this.sqlConn.execute(Sql2);

            let Sql3 =  `DELETE FROM person WHERE id_person = ${id_treem}`
            const [rows, fields] = await this.sqlConn.execute(Sql3);

            let Sql4 =  `DELETE FROM chitietgiadinh WHERE id_person = ${id_treem}`
            await this.sqlConn.execute(Sql4);

            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    //Function Handle Delete Logical Children
    async logicalDeleteChildren(id_treem) {
        try {
            let Sql =  `UPDATE person SET trangthai=2 WHERE id_person = ${id_treem}`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async logicalDeleteChildrenMulti(listId) {
        try {
            if(listId.length > 0){
                listId.forEach(async (id) => {
                    let Sql =  `UPDATE person SET trangthai=2 WHERE id_person = ${id}`
                    await this.sqlConn.execute(Sql);
                })

                return 1;
            }else{
                return 0;
            }
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async restoreChildrenMulti(listId) {
        try {
            if(listId.length > 0){
                listId.forEach(async (id) => {
                    let Sql =  `UPDATE person SET trangthai=1 WHERE id_person = ${id}`
                    await this.sqlConn.execute(Sql);
                })

                return 1;
            }else{
                return 0;
            }
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    // Functions Update Children 
    async updateData(data, id) {
        try {
            // let Sql =  `UPDATE treem, giadinh, tinhtranghoctap, thon
            //             SET     treem.hoten = "${data.hoten}",
            //                     treem.ngaysinh = "${data.ngaysinh}",
            //                     giadinh.hotencha = "${data.hotencha}",
            //                     giadinh.hotenme = "${data.hotenme}",
            //                     giadinh.nguoinuoiduong = "${data.nguoinuoiduong}",
            //                     treem.dantoc = "${data.dantoc}",
            //                     treem.gioitinh = "${data.gioitinh}",
            //                     giadinh.id_thon = "${data.id_thon}",
            //                     giadinh.sodienthoai = "${data.sdt}",
            //                     giadinh.hoancanh = "${data.hoancanh}",
            //                     giadinh.diachi = "${data.diachi}",
            //                     tinhtranghoctap.lophoccaonhat = "${data.lophoccaonhat}",
            //                     tinhtranghoctap.tinhtranghoctap = "${data.tinhtranghoctap}",
            //                     treem.ghichu = "${data.ghichu}"


            //             WHERE   treem.id_giadinh = giadinh.id_giadinh AND 
            //                     treem.id_tinhtranghoctap = tinhtranghoctap.id_tinhtranghoctap AND
            //                     treem.id_treem = ${id}
            //             `

            data.cha = data.cha==undefined ? null : (!data.cha ? null : data.cha)
            data.me = data.me==undefined ? null : (!data.me ? null : data.me)
            data.ghichu = data.ghichu==undefined ? null : (!data.ghichu ? null : data.ghichu)
            data.trinhdohocvan = data.trinhdohocvan==undefined ? null : (!data.trinhdohocvan ? null : data.trinhdohocvan)

            let Sql = `UPDATE person SET person.hoten = "${data.hoten}", person.ngaysinh = "${data.ngaysinh}",
                       person.cha = ${data.cha}, person.me = ${data.me}, person.gioitinh = "${data.gioitinh}", person.dantoc = "${data.dantoc}",
                       person.ghichu = "${data.ghichu}", person.trinhdohocvan = "${data.trinhdohocvan}", person.trangthai = 1
                       WHERE person.id_person=${id}`;
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async updateMemberFamily(data) {
        try {

            let SqlDel = `DELETE FROM chitietgiadinh WHERE id_giadinh=${data.id_giadinh} AND (id_chucdanh <> 7)`;

            const [del, delField] = await this.sqlConn.execute(SqlDel);

            data.cha = data.cha==undefined ? null : (!data.cha ? null : data.cha)
            data.me = data.me==undefined ? null : (!data.me ? null : data.me)
            data.nguoinuoiduong = data.nguoinuoiduong==undefined ? null : (!data.nguoinuoiduong ? null : data.nguoinuoiduong)
            console.log({data})
            let SqlAdd = `INSERT INTO chitietgiadinh(id_giadinh, id_person, id_chucdanh, trangthainuoiduong)
                          VALUES `;
            // for(const [key, value] in Object.entries(data)){
            //     if(key === 'cha'){
            //         SqlAdd += `(${data.id_giadinh},${value},1),`
            //     }else if(key === 'me'){
            //         SqlAdd += `(${data.id_giadinh},${value},2);`
            //     }
            // }
            if(data.cha && data.me){
                SqlAdd += `(${data.id_giadinh}, ${data.cha}, 1, 1),
                           (${data.id_giadinh}, ${data.me}, 2, 1)`
            }else if(data.cha && !data.me){
                SqlAdd += `(${data.id_giadinh}, ${data.cha}, 1, 1)`
            }else if(!data.cha && data.me){
                SqlAdd += `(${data.id_giadinh}, ${data.me}, 2, 1)`
            }

            if(data.nguoinuoiduong){
                SqlAdd += `,(${data.id_giadinh}, ${data.idNguoiNuoi}, 4, 1)`
            }

            SqlAdd += `;`;

            const [add, addField] = await this.sqlConn.execute(SqlAdd);

            return [add, addField];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async updateFamily(data) {
        try {

            data.diachi = data.diachi ? data.diachi : '';
            data.hoancanh = data.hoancanh ? data.hoancanh : 3;

            let Sql = `UPDATE giadinh SET diachi='${data.diachi}', id_hoancanhgiadinh=${data.hoancanh}, id_thon=${data.id_thon} WHERE id_giadinh=${data.id_giadinh};`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async updatePerson(data, id) {
        try {
            let Sql = `UPDATE person SET person.hoten="${data.hoten}", person.ngaysinh="${data.ngaysinh}",
                       person.gioitinh="${data.gioitinh}", person.dantoc="${data.dantoc}", person.sodienthoai="${data.sodienthoai}",
                       person.trinhdohocvan="${data.trinhdohocvan}", person.cmnd = '${data.cmnd}'
                       WHERE person.id_person=${id}`;
                       
            let Sql2 = ` SELECT * FROM (
                            SELECT person.id_person, 
                            gd.id_giadinh, 
                            gd.diachi,
                            person.hoten,
                            person.gioitinh,
                            person.ngaysinh,
                            person.sodienthoai,
                            person.trinhdohocvan,
                            person.dantoc,
                            person.cmnd,
                            phuongxa.id_phuongxa,
                            t.id_thon, t.tenthon

                            FROM thon as t
                            JOIN giadinh as gd ON t.id_thon = gd.id_thon AND t.daxoa = 0
                            JOIN phuongxa ON phuongxa.id_phuongxa=t.id_phuongxa
                            JOIN chitietgiadinh ON chitietgiadinh.id_giadinh = gd.id_giadinh
                            JOIN person ON person.id_person=chitietgiadinh.id_person AND (person.trangthai=0 OR person.trangthai=1))
                            as info WHERE info.id_person = ${id};`

            await this.sqlConn.execute(Sql);
            const [rows, fields] = await this.sqlConn.execute(Sql2);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async addPerson(data) {
        try {

            const {
                hoten, ngaysinh, gioitinh, dantoc, sodienthoai, trinhdohocvan, cmnd
            } = data;

            let Sql = `INSERT INTO person (hoten, ngaysinh, gioitinh, dantoc, sodienthoai, trinhdohocvan, cmnd)
            VALUES ('${hoten}','${ngaysinh}','${gioitinh}','${dantoc}','${sodienthoai}','${trinhdohocvan}', '${cmnd}')`;
            
            const [rows, fields] = await this.sqlConn.execute(Sql);

            let Sql2 = ` SELECT * FROM person WHERE id_person = ${rows.insertId};`

            const [rows2, fields2] = await this.sqlConn.execute(Sql2);
            return [rows2, fields2];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async updateHoanCanh(data, id) {
        try {
            if(data.id_hoancanh != undefined){
                let arrayIdHoanCanh = data.id_hoancanh.length;
                if(arrayIdHoanCanh > 0){
                    data.id_hoancanh.forEach(async (value) => {
                        let Sql =  `INSERT INTO chitietloaihoancanh(id_person,id_hoancanh) VALUES(${id},${value});`
                        let [rows, fields] = await this.sqlConn.execute(Sql);
                    })            
                    return 1;
                }
            }
            return 1;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async deleteHoanCanh(id) {
        try {
            let Sql =  `DELETE FROM chitietloaihoancanh WHERE id_person = ${id}`    
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
//////////////////////////////
    async updateTroGiup(data, id) {
        try {
            if(data.id_trogiup != undefined){
                let arrayIdTroGiup = data.id_trogiup.length;
                if(arrayIdTroGiup > 0){
                    data.id_trogiup.forEach(async (value) => {
                        let Sql =  `INSERT INTO phanbotrogiup(id_person, id_hinhthuc) VALUES (${id},${value});`
                        let [rows, fields] = await this.sqlConn.execute(Sql);
                    })            
                    return 1;
                }
            }
            return 1;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async deleteTroGiup(id) {
        try {
            let Sql =  `DELETE FROM phanbotrogiup WHERE id_person = ${id} `   
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async CheckChildrenBeLongTo(id, address) {
        try {
            let where = ""
            if(address.id_phuongxa){
                where += "phuongxa.id_phuongxa = " + address.id_phuongxa + " AND "
            }
            if(address.id_quanhuyen){
                where += "quanhuyen.id_quanhuyen = " + address.id_quanhuyen + " AND "
            }
            if(address.id_tinhthanhpho){
                where += "tinhthanhpho.id_tinhthanhpho = " + address.id_tinhthanhpho
            }

            let Sql =  `SELECT person.id_person as id_treem, giadinh.id_giadinh, person.hoten, 
                               person.ngaysinh, person.dantoc, person.gioitinh, thon.tenthon,
                               (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                               (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                               (SELECT prs.hoten FROM chitietgiadinh as ctgd
                               JOIN person as prs ON prs.id_person = ctgd.id_person
                               WHERE ctgd.id_giadinh = giadinh.id_giadinh 
                               AND ctgd.id_person <> person.cha 
                               AND ctgd.id_person <> person.me
                               AND ctgd.trangthainuoiduong = 1
                               LIMIT 1
                               ) as nguoinuoiduong,
                               phuongxa.ten_phuongxa, quanhuyen.ten_quanhuyen, tinhthanhpho.ten_tinhthanhpho 
            FROM person
            JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
            JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh 
            JOIN thon ON giadinh.id_thon=thon.id_thon AND thon.daxoa = 0 AND (person.trangthai=0 OR person.trangthai=1)
            JOIN phuongxa ON thon.id_phuongxa=phuongxa.id_phuongxa 
            JOIN quanhuyen ON phuongxa.id_quanhuyen=quanhuyen.id_quanhuyen 
            JOIN tinhthanhpho ON quanhuyen.id_tinhthanhpho=tinhthanhpho.id_tinhthanhpho
            WHERE person.id_person=${id} AND ${where};`
            console.log(Sql);
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async CheckChildrenInTrash(id, address) {
        try {
            let where = ""
            if(address.id_phuongxa){
                where += "phuongxa.id_phuongxa = " + address.id_phuongxa + " AND "
            }
            if(address.id_quanhuyen){
                where += "quanhuyen.id_quanhuyen = " + address.id_quanhuyen + " AND "
            }
            if(address.id_tinhthanhpho){
                where += "tinhthanhpho.id_tinhthanhpho = " + address.id_tinhthanhpho
            }

            let Sql =  `SELECT treem.id_treem, giadinh.id_giadinh, treem.hoten, treem.ngaysinh, giadinh.hotencha, giadinh.hotenme, giadinh.nguoinuoiduong, treem.dantoc, treem.gioitinh, thon.tenthon, phuongxa.ten_phuongxa, quanhuyen.ten_quanhuyen, tinhthanhpho.ten_tinhthanhpho FROM treem 
            JOIN giadinh ON treem.id_giadinh=giadinh.id_giadinh 
            JOIN thon ON giadinh.id_thon=thon.id_thon AND thon.daxoa = 0 AND treem.daxoa = 1
            JOIN phuongxa ON thon.id_phuongxa=phuongxa.id_phuongxa 
            JOIN quanhuyen ON phuongxa.id_quanhuyen=quanhuyen.id_quanhuyen 
            JOIN tinhthanhpho ON quanhuyen.id_tinhthanhpho=tinhthanhpho.id_tinhthanhpho
            WHERE treem.id_treem=${id} AND ${where};`  

            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
}


module.exports = QuanLyHoSoTreEm;