const Test = require('./test.model')
const TinhThanhPho = require('./tinhthanhpho.model')
const QuanLyHoSoTreEm = require('./QuanLyHoSoTreEm.model')
const NguyCoHCDB = require('./NguyCoHCDB.model')
const HoanCanhDacBiet = require('./HoanCanhDacBiet.model')
const HoanCanhKhac = require('./HoanCanhKhac.models')
const HinhThucTroGiup = require('./HinhThucTroGiup.models')
const Local = require('./Local.models')
const DanhMuc = require('./DanhMuc.models')
const User = require('./User.models')
const QuanLyThon = require('./QuanLyThon.models')
const DSHoGiaDinh = require('./DanhSachHoGiaDinh.models')
const Account = require('./Account.models')
const Excel = require('./Excel.models')

class Model {
    constructor({ sqlConn }) {
        this.test1 = new Test(sqlConn);
        this.tinhthanhpho = new TinhThanhPho(sqlConn);
        this.QuanLyHoSoTreEm = new QuanLyHoSoTreEm(sqlConn);
        this.NguyCoHCDB = new NguyCoHCDB(sqlConn);
        this.HoanCanhDacBiet = new HoanCanhDacBiet(sqlConn);
        this.HoanCanhKhac = new HoanCanhKhac(sqlConn);
        this.HinhThucTroGiup = new HinhThucTroGiup(sqlConn);
        this.Local = new Local(sqlConn);
        this.DanhMuc = new DanhMuc(sqlConn);
        this.User = new User(sqlConn);
        this.QuanLyThon = new QuanLyThon(sqlConn);
        this.DSHoGiaDinh = new DSHoGiaDinh(sqlConn);
        this.Account = new Account(sqlConn);
        this.Excel = new Excel(sqlConn);
    }
}

module.exports = Model;
