const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const _ = require('lodash');
const moment = require('moment');

class Excel {
    constructor(sqlConn) {
        this.sqlConn = sqlConn;
    }

    async getcolumnsHCDB() {
        try {
            let sql = `SELECT * FROM hoancanh where id_loaihoancanh = 165 AND cha=0;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getcolumnsNCHCDB() {
        try {
            let sql = `SELECT * FROM hoancanh where id_loaihoancanh = 166 AND cha=0;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getcolumnsHCK() {
        try {
            let sql = `SELECT * FROM hoancanh where id_loaihoancanh = 167 AND cha=0;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getcolumnsHTTG() {
        try {
            let sql = `SELECT * FROM hinhthuctrogiup where chahinhthuc=0;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isCheckFather(data) {
        try {
            let sql = `SELECT * from giadinh WHERE hotencha=${data}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            if(rows.length > 0){
                return true
            }else{
                return false
            }
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isCheckMother(data) {
        try {
            let sql = `SELECT * from giadinh WHERE hotenme=${data}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            if(rows.length > 0){
                return true
            }else{
                return false
            }
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isCheckFamily(data) {
        try {
            let sql = `SELECT * from giadinh WHERE hotencha=${data} AND hotenme=${data} AND nguoinuoiduong=${data}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields]
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    /* Id Hoan Canh === 165 */
    async getChildrenFollowVillage(id) {
        try {

            let sql = `SELECT giadinh.id_giadinh, person.hoten, person.id_person as id_treem, person.ngaysinh, person.dantoc, person.gioitinh,
                              (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                              (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                              (SELECT prs.hoten FROM chitietgiadinh as ctgd
                                    JOIN person as prs ON prs.id_person = ctgd.id_person
                                    WHERE ctgd.id_giadinh = giadinh.id_giadinh
                                    AND ctgd.id_person <> person.cha 
                                    AND ctgd.id_person <> person.me
                                    AND ctgd.trangthainuoiduong = 1
                                    LIMIT 1
                              ) as nguoinuoiduong,
                              giadinh.diachi, thon.id_thon, thon.tenthon, hoancanh.id_hoancanh 
                        FROM person
                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
                        JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                        JOIN thon ON thon.id_thon=giadinh.id_thon AND thon.daxoa=0 AND (person.trangthai=0 OR person.trangthai=1)
                        JOIN chitietloaihoancanh ON chitietloaihoancanh.id_person=person.id_person
                        JOIN hoancanh ON hoancanh.id_hoancanh=chitietloaihoancanh.id_hoancanh
                        WHERE thon.id_thon=${id} AND hoancanh.id_loaihoancanh=165
                        GROUP BY person.id_person;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    /* Id Hoan Canh === 166 */
    async getChildrenFollowVillageNCHCDB(id) {
        try {

            let sql = `SELECT giadinh.id_giadinh, person.hoten, person.id_person as id_treem, person.ngaysinh, person.dantoc, person.gioitinh,
                       (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                       (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                       (SELECT prs.hoten FROM chitietgiadinh as ctgd
                       JOIN person as prs ON prs.id_person = ctgd.id_person
                       WHERE ctgd.id_giadinh = giadinh.id_giadinh
                       AND ctgd.id_person <> person.cha 
                       AND ctgd.id_person <> person.me
                       AND ctgd.trangthainuoiduong = 1
                       LIMIT 1
                       ) as nguoinuoiduong,
                              giadinh.diachi, thon.id_thon, thon.tenthon, hoancanh.id_hoancanh 
                        FROM person
                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
                        JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                        JOIN thon ON thon.id_thon=giadinh.id_thon AND thon.daxoa=0 AND (person.trangthai=0 OR person.trangthai=1)
                        JOIN chitietloaihoancanh ON chitietloaihoancanh.id_person=person.id_person
                        JOIN hoancanh ON hoancanh.id_hoancanh=chitietloaihoancanh.id_hoancanh
                        WHERE thon.id_thon=${id} AND hoancanh.id_loaihoancanh=166
                        GROUP BY person.id_person;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    /* Id Hoan Canh === 167 */
    async getChildrenFollowVillageHCK(id) {
        try {

            let sql = `SELECT giadinh.id_giadinh, person.hoten, person.id_person as id_treem, person.ngaysinh, person.dantoc, person.gioitinh,
                       (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                       (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                       (SELECT prs.hoten FROM chitietgiadinh as ctgd
                       JOIN person as prs ON prs.id_person = ctgd.id_person
                       WHERE ctgd.id_giadinh = giadinh.id_giadinh
                       AND ctgd.id_person <> person.cha 
                       AND ctgd.id_person <> person.me
                       AND ctgd.trangthainuoiduong = 1
                       LIMIT 1
                       ) as nguoinuoiduong,
                              giadinh.diachi, thon.id_thon, thon.tenthon, hoancanh.id_hoancanh 
                        FROM person
                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
                        JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                        JOIN thon ON thon.id_thon=giadinh.id_thon AND thon.daxoa=0 AND (person.trangthai=0 OR person.trangthai=1)
                        JOIN chitietloaihoancanh ON chitietloaihoancanh.id_person=person.id_person
                        JOIN hoancanh ON hoancanh.id_hoancanh=chitietloaihoancanh.id_hoancanh
                        WHERE thon.id_thon=${id} AND hoancanh.id_loaihoancanh=167
                        GROUP BY person.id_person;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getChildrenFollowVillageHTTG(id) {
        try {

            let sql = `SELECT giadinh.id_giadinh, person.hoten, person.id_person as id_treem, person.ngaysinh, person.dantoc, person.gioitinh,
                              (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.cha) as hotencha,
                              (SELECT prs.hoten FROM person as prs WHERE prs.id_person=person.me) as hotenme,
                              (SELECT prs.hoten FROM chitietgiadinh as ctgd
                              JOIN person as prs ON prs.id_person = ctgd.id_person
                              WHERE ctgd.id_giadinh = giadinh.id_giadinh
                              AND ctgd.id_person <> person.cha 
                              AND ctgd.id_person <> person.me
                              AND ctgd.trangthainuoiduong = 1
                              LIMIT 1
                              ) as nguoinuoiduong,
                              giadinh.diachi, thon.id_thon, thon.tenthon, phanbotrogiup.id_hinhthuc 
                        FROM person
                        JOIN chitietgiadinh ON chitietgiadinh.id_person=person.id_person AND chitietgiadinh.id_chucdanh=7
                        JOIN giadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                        JOIN thon ON thon.id_thon=giadinh.id_thon AND thon.daxoa=0 AND (person.trangthai=0 OR person.trangthai=1)
                        JOIN phanbotrogiup ON phanbotrogiup.id_person=person.id_person
                        JOIN hinhthuctrogiup ON hinhthuctrogiup.id_hinhthuc=phanbotrogiup.id_hinhthuc
                        WHERE thon.id_thon=${id}
                        GROUP BY person.id_person;`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getListVillageFollowIDWard(id) {
        try {

            let sql = `SELECT * FROM thon WHERE id_phuongxa=${id}`;
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getListVillageFollowIDDistrict(id) {
        try {

            let sql = `SELECT * FROM phuongxa WHERE id_quanhuyen=${id}`;
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
    
    async findVillage(id) {
        try {

            let sql = `SELECT * FROM thon WHERE id_thon=${id};`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getFamilyFollowVillage(search) {

        try {

            let sql = ` SELECT * FROM (
                SELECT prs.id_person as id_treem, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                gd.diachi,
                prs.sodienthoai,
                hcgd.id_hoancanhgiadinh as hoancanh,
                prs.hoten,
                ct.id_tinhthanhpho,
                qh.id_quanhuyen,
                px.id_phuongxa,
                t.id_thon, t.tenthon

                FROM person as prs
                JOIN chitietgiadinh as ctgd ON prs.id_person=ctgd.id_person AND ctgd.id_chucdanh=7
                JOIN giadinh as gd ON ctgd.id_giadinh = gd.id_giadinh
                JOIN hoancanhgiadinh as hcgd ON hcgd.id_hoancanhgiadinh = gd.id_hoancanhgiadinh
                JOIN thon as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
                JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
                JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
                JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho) as info WHERE ${search} 
                GROUP BY info.id_giadinh ORDER BY info.id_giadinh ASC`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getChildrenFollowVillageKTDL(search) {

        try {

            let sql = ` SELECT * FROM (
                SELECT prs.id_person as id_treem, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                gd.diachi,
                prs.ngaysinh,
                prs.trinhdohocvan,
                prs.dantoc,
                prs.gioitinh,
                hcgd.id_hoancanhgiadinh as hoancanh,
                prs.hoten,
                ct.id_tinhthanhpho,
                qh.id_quanhuyen,
                px.id_phuongxa,
                t.id_thon, t.tenthon

                FROM person as prs
                JOIN chitietgiadinh as ctgd ON prs.id_person=ctgd.id_person AND ctgd.id_chucdanh=7
                JOIN giadinh as gd ON ctgd.id_giadinh = gd.id_giadinh
                JOIN hoancanhgiadinh as hcgd ON hcgd.id_hoancanhgiadinh = gd.id_hoancanhgiadinh
                JOIN thon as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
                JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
                JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
                JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho) as info WHERE ${search} 
                ORDER BY info.id_treem ASC`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }


    async getChildrenFollowVillageNormalKTDL(search) {

        try {

            let sql = ` SELECT * FROM (
                SELECT prs.id_person as id_treem, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                gd.diachi,
                prs.ngaysinh,
                prs.trinhdohocvan,
                prs.dantoc,
                prs.gioitinh,
                hcgd.id_hoancanhgiadinh as hoancanh,
                prs.hoten,
                ct.id_tinhthanhpho,
                qh.id_quanhuyen,
                px.id_phuongxa,
                t.id_thon, t.tenthon

                FROM person as prs
                JOIN chitietgiadinh as ctgd ON prs.id_person=ctgd.id_person AND ctgd.id_chucdanh=7
                JOIN giadinh as gd ON ctgd.id_giadinh = gd.id_giadinh
                JOIN hoancanhgiadinh as hcgd ON hcgd.id_hoancanhgiadinh = gd.id_hoancanhgiadinh
                JOIN thon as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
                JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
                JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
                JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho) as info WHERE ${search}
                AND info.id_treem NOT IN (SELECT ctlhc.id_person FROM chitietloaihoancanh as ctlhc)
                AND info.id_treem NOT IN (SELECT pbtg.id_person FROM phanbotrogiup as pbtg)
                ORDER BY info.id_treem ASC`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getChildrenFollowVillageObjectKTDL(search) {

        try {

            let sql = ` SELECT * FROM (
                SELECT prs.id_person as id_treem, 
                gd.id_giadinh, 
                (SELECT person.hoten FROM person WHERE person.id_person=prs.cha) as hotencha,
                (SELECT person.hoten FROM person WHERE person.id_person=prs.me) as hotenme,
                (SELECT person.hoten FROM chitietgiadinh
                JOIN person ON person.id_person = chitietgiadinh.id_person
                WHERE chitietgiadinh.id_giadinh = gd.id_giadinh
                AND chitietgiadinh.id_person <> prs.cha 
                AND chitietgiadinh.id_person <> prs.me
                AND chitietgiadinh.trangthainuoiduong = 1
                LIMIT 1
                ) as nguoinuoiduong, 
                prs.ngaysinh,
                prs.dantoc,
                prs.gioitinh,
                prs.hoten,
                ct.id_tinhthanhpho,
                qh.id_quanhuyen,
                px.id_phuongxa,
                t.id_thon, t.tenthon

                FROM person as prs
                JOIN chitietgiadinh as ctgd ON prs.id_person=ctgd.id_person AND ctgd.id_chucdanh=7
                JOIN giadinh as gd ON ctgd.id_giadinh = gd.id_giadinh
                JOIN thon as t ON t.id_thon = gd.id_thon AND t.daxoa = 0 AND (prs.trangthai=0 OR prs.trangthai=1)
                JOIN phuongxa as px ON t.id_phuongxa = px.id_phuongxa
                JOIN quanhuyen as qh ON px.id_quanhuyen = qh.id_quanhuyen
                JOIN tinhthanhpho as ct ON qh.id_tinhthanhpho = ct.id_tinhthanhpho) as info WHERE ${search}
                ORDER BY info.id_treem ASC`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return [rows, fields];
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
    
    async getWardWhenExistsVillage(id){
        try {

            let sql = `SELECT * FROM phuongxa WHERE id_phuongxa=${id}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return rows[0].ten_phuongxa;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async getDistrictWhenExistsVillage(id){
        try {

            let sql = `SELECT * FROM quanhuyen WHERE id_quanhuyen=${id}`
            const [rows, fields] = await this.sqlConn.execute(sql);
            return rows[0].ten_quanhuyen;
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async AddChildrenWithFileExcel(data, rows4, rows5) {
        try {
            const length = data.length;
            if(length > 0){
                
                for(let j = 0; j<length; j++){
                    let dateParts = data[j][3].split("/");
                    let dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                    let datetime =  dateObject.getFullYear() + "-" + (dateObject.getMonth()+1)  + "-" + dateObject.getDate()
                    let Sql2 = `INSERT INTO chitietloaihoancanh(id_person, id_hoancanh) VALUES `;
                    let Sql3 = `INSERT INTO phanbotrogiup(id_person, id_hinhthuc) VALUES `;
                    let Sql4 = `INSERT INTO chitietgiadinh(id_giadinh, id_person, id_chucdanh, trangthainuoiduong) VALUES `
                    let Sql = `INSERT INTO person(hoten,ngaysinh,gioitinh,dantoc,trinhdohocvan,ghichu,cha,me) 
                    VALUES ('${data[j][2]}','${datetime}','${data[j][5]}','${data[j][4]}','${data[j][6]}','${data[j][7]}',${data[j][36].cha},${data[j][37].me})`; 

                    const [rows, fields] = await this.sqlConn.execute(Sql);
                    const resultId = { ...rows }

                    // let lengthDT = data[j].length;
                    // for await (const [index,value] of data[j].entries()){
                    //     if(value === 1 && rows4[index] != "Hình thức trợ giúp"){
                    //         let SqlName = `SELECT * FROM hoancanh WHERE ten_hoancanh = '${rows5[i]}'`
                    //         const [name, nameFields] = await this.sqlConn.execute(SqlName);
                    //         Sql2 += `(${resultId.insertId}, ${name[0].ten_hoancanh})`
                    //     }else if(value === 1 && rows4[index] == "Hình thức trợ giúp"){
                    //         let SqlName = `SELECT * FROM hinhthuctrogiup WHERE tenhinhthuc = '${rows5[i]}'`
                    //         const [name, nameFields] = await this.sqlConn.execute(SqlName);
                    //         Sql3 += `(${resultId.insertId}, ${name[0].tenhinhthuc})`
                    //     }
                    // }

                    async function AddChiTieu(data, sqlConn){
                        let arrIdHoanCanh = [];
                        let arrIdTroGiup = [];

                        for (const [index,value] of data.entries()){
                            if(value == 1  && rows4[index] != "Hình thức trợ giúp"){
                                let SqlName = `SELECT * FROM hoancanh WHERE ten_hoancanh = '${rows5[index]}'`
                                const [name, nameFields] = await sqlConn.execute(SqlName);
                                arrIdHoanCanh.push(name[0].id_hoancanh);
                            }else if(value == 1 && rows4[index] == "Hình thức trợ giúp"){
                                let SqlName = `SELECT * FROM hinhthuctrogiup WHERE tenhinhthuc = '${rows5[index]}'`
                                const [name, nameFields] = await sqlConn.execute(SqlName);
                                arrIdTroGiup.push(name[0].id_hinhthuc)
                            }
                        }

                        return [arrIdHoanCanh, arrIdTroGiup];
                    }

                    const [arrHoanCanh, arrTroGiup] = await AddChiTieu(data[j], this.sqlConn);
                    const arrHoanCanhLength = arrHoanCanh.length;
                    const arrTroGiupLength = arrTroGiup.length;
                    if(arrHoanCanhLength > 0){
                        for(const value of arrHoanCanh){
                            Sql2 += `(${resultId.insertId}, ${value}),`;
                        }
                        Sql2 = Sql2.slice(0,-1);
                        const [addHoancanh, addHoancanhFields] = await this.sqlConn.execute(Sql2);
                    }
                    if(arrTroGiupLength > 0){
                        for(const value of arrTroGiup){
                            Sql3 += `(${resultId.insertId}, ${value}),`;
                        }
                        Sql3 = Sql3.slice(0,-1);
                        const [addTrogiup, addTrogiupFields] = await this.sqlConn.execute(Sql3)
                    }

                    Sql4 += `(${data[j][1]}, ${resultId.insertId}, 7, 0)`;
                    const [addChildWithFamily, addChildWithFamilyFields] = await this.sqlConn.execute(Sql4);

                }
            }

            // let sqlIdChildren = `select last_insert_id() as id;`;
            // const [rows, fields] = await this.sqlConn.execute(Sql);
            // const [idChildren, fieldsChildren] = await this.sqlConn.execute(sqlIdChildren);
            // const resultId = { ...rows }
            // return resultId.insertId
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async AddChildrenOtherFamilyFileExcel(data, rows4, rows5) {
        try {
            const length = data.length;
            if(length > 0){
                
                for(let j = 0; j<length; j++){

                    data[j][7] = data[j][7] ? data[j][7] : '';
                    data[j][12] = data[j][12] ? data[j][12] : null;
                    data[j][13] = data[j][13] ? data[j][13] : null;

                    let dateParts = data[j][9].split("/");
                    let dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                    let datetime =  dateObject.getFullYear() + "-" + (dateObject.getMonth()+1)  + "-" + dateObject.getDate()

                    let Sql2 = `INSERT INTO chitietloaihoancanh(id_person, id_hoancanh) VALUES `;
                    let Sql3 = `INSERT INTO phanbotrogiup(id_person, id_hinhthuc) VALUES `;
                    let Sql4 = `INSERT INTO chitietgiadinh(id_giadinh, id_person, id_chucdanh, trangthainuoiduong) VALUES `;
                    let Sql5 = `INSERT INTO giadinh(id_thon, id_hoancanhgiadinh, diachi) 
                    VALUES ('${data[j][44].IDThon}', ${data[j][42].hoancanhgiadinh}, '${data[j][7]}')`;
                    let Sql = `INSERT INTO person(hoten,ngaysinh,gioitinh,dantoc,trinhdohocvan,ghichu,cha,me) 
                    VALUES ('${data[j][8]}','${datetime}','${data[j][11]}','${data[j][10]}','${data[j][12]}','${data[j][13]}',${data[j][43].member.cha},${data[j][43].member.me})`; 

                    const [rows5_, fields5] = await this.sqlConn.execute(Sql5);
                    const IDFamily = { ...rows5_ }

                    const [rows, fields] = await this.sqlConn.execute(Sql);
                    const IDChildren = { ...rows }

                    // let lengthDT = data[j].length;
                    // for await (const [index,value] of data[j].entries()){
                    //     if(value === 1 && rows4[index] != "Hình thức trợ giúp"){
                    //         let SqlName = `SELECT * FROM hoancanh WHERE ten_hoancanh = '${rows5[i]}'`
                    //         const [name, nameFields] = await this.sqlConn.execute(SqlName);
                    //         Sql2 += `(${resultId.insertId}, ${name[0].ten_hoancanh})`
                    //     }else if(value === 1 && rows4[index] == "Hình thức trợ giúp"){
                    //         let SqlName = `SELECT * FROM hinhthuctrogiup WHERE tenhinhthuc = '${rows5[i]}'`
                    //         const [name, nameFields] = await this.sqlConn.execute(SqlName);
                    //         Sql3 += `(${resultId.insertId}, ${name[0].tenhinhthuc})`
                    //     }
                    // }

                    async function AddChiTieu(data, sqlConn){
                        let arrIdHoanCanh = [];
                        let arrIdTroGiup = [];

                        for (const [index,value] of data.entries()){
                            if(value == 1  && rows4[index] != "Hình thức trợ giúp"){
                                let SqlName = `SELECT * FROM hoancanh WHERE ten_hoancanh = '${rows5[index]}'`
                                const [name, nameFields] = await sqlConn.execute(SqlName);
                                arrIdHoanCanh.push(name[0].id_hoancanh);
                            }else if(value == 1 && rows4[index] == "Hình thức trợ giúp"){
                                let SqlName = `SELECT * FROM hinhthuctrogiup WHERE tenhinhthuc = '${rows5[index]}'`
                                const [name, nameFields] = await sqlConn.execute(SqlName);
                                arrIdTroGiup.push(name[0].id_hinhthuc)
                            }
                        }

                        return [arrIdHoanCanh, arrIdTroGiup];
                    }

                    const [arrHoanCanh, arrTroGiup] = await AddChiTieu(data[j], this.sqlConn);
                    const arrHoanCanhLength = arrHoanCanh.length;
                    const arrTroGiupLength = arrTroGiup.length;
                    if(arrHoanCanhLength > 0){
                        for(const value of arrHoanCanh){
                            Sql2 += `(${IDChildren.insertId}, ${value}),`;
                        }
                        Sql2 = Sql2.slice(0,-1);
                        const [addHoancanh, addHoancanhFields] = await this.sqlConn.execute(Sql2)
                    }
                    if(arrTroGiupLength > 0){
                        for(const value of arrTroGiup){
                            Sql3 += `(${IDChildren.insertId}, ${value}),`;
                        }
                        Sql3 = Sql3.slice(0,-1);
                        const [addTrogiup, addTrogiupFields] = await this.sqlConn.execute(Sql3)
                    }

                    Sql4 += `(${IDFamily.insertId}, ${IDChildren.insertId}, 7, 0),`;

                    if(data[j][43].member.cha){
                        Sql4 += `(${IDFamily.insertId},${data[j][43].member.cha}, 1, 1),`;
                    }

                    if(data[j][43].member.me){
                        Sql4 += `(${IDFamily.insertId},${data[j][43].member.me}, 2, 1),`;
                    }

                    if(data[j][43].member.nuoi){
                        Sql4 += `(${IDFamily.insertId},${data[j][43].member.nuoi}, 4, 1),`;
                    }

                    Sql4 = Sql4.slice(0,-1);

                    const [addChildWithFamily, addChildWithFamilyFields] = await this.sqlConn.execute(Sql4);

                }
            }

            // let sqlIdChildren = `select last_insert_id() as id;`;
            // const [rows, fields] = await this.sqlConn.execute(Sql);
            // const [idChildren, fieldsChildren] = await this.sqlConn.execute(sqlIdChildren);
            // const resultId = { ...rows }
            // return resultId.insertId
        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async addPerson(data) {
        try {

            const length = data.length;

            if(length > 0){
                for(let j = 0; j<length; j++){

                    data[j][2] = data[j][2] ? data[j][2] : null;
                    data[j][6] = data[j][6] ? data[j][6] : null;
                    data[j][7] = data[j][7] ? data[j][7] : null;

                    let dateParts = data[j][3].split("/");
                    let dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
                    let datetime =  dateObject.getFullYear() + "-" + (dateObject.getMonth()+1)  + "-" + dateObject.getDate()

                    let Sql = `INSERT INTO person (hoten, ngaysinh, gioitinh, dantoc, sodienthoai, trinhdohocvan, cmnd)
                    VALUES ('${data[j][1]}','${datetime}','${data[j][5]}','${data[j][4]}','${data[j][6]}','${data[j][7]}', '${data[j][2]}')`;

                    const [rows, fields] = await this.sqlConn.execute(Sql);

                }
            }

        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async CanAddFamily(thon, hoancanhgiadinh, object) {
        try {

            if(object.phuongxa){
                const Sql3 = `SELECT * FROM thon WHERE id_phuongxa = ${object.phuongxa}`;
                const Sql = `SELECT * FROM thon WHERE tenthon = '${thon}' AND id_phuongxa = ${object.phuongxa}`;
                const Sql2 = `SELECT * FROM hoancanhgiadinh WHERE id_hoancanhgiadinh = ${hoancanhgiadinh}`;
                const [rows3, fields3] = await this.sqlConn.execute(Sql3);
                const [rows2, fields2] = await this.sqlConn.execute(Sql2);
                const [rows1, fields1] = await this.sqlConn.execute(Sql);

                if(rows1.length > 0 && rows2.length > 0){
                    const idx = rows3.findIndex(({ id_thon }) => id_thon===rows1[0].id_thon);
                    if(idx!==-1){
                        return [true, rows1[0].id_thon];
                    }
                    return [false,null];
                }
    
            }
            return [false, null];

        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isCheckPerson(cmndcha, cmndme, cmndnguoinuoiduong) {
        try {

            let cha = [];
            let me = [];
            let nguoinuoiduong = [];

            if(cmndcha){
                let Sql1 = `SELECT * FROM person WHERE cmnd = '${cmndcha}'`;
                const [rows, fields] = await this.sqlConn.execute(Sql1);
                if(rows.length > 0){
                    const Sql4 = `SELECT * FROM chitietgiadinh WHERE id_person = ${rows[0].id_person} AND id_chucdanh = 1`;
                    const [rows1, fields1] = await this.sqlConn.execute(Sql4);
                    if(rows1.length <= 0){
                        cha = [...rows];
                    }
                }
            }

            if(cmndme){
                let Sql2 = `SELECT * FROM person WHERE cmnd = '${cmndme}'`;
                const [rows, fields] = await this.sqlConn.execute(Sql2);
                if(rows.length > 0){
                    const Sql4 = `SELECT * FROM chitietgiadinh WHERE id_person = ${rows[0].id_person} AND id_chucdanh = 2`;
                    const [rows1, fields1] = await this.sqlConn.execute(Sql4);
                    if(rows1.length <= 0){
                        me = [...rows];
                    }
                }
            }

            if(cmndnguoinuoiduong){
                let Sql3 = `SELECT * FROM person WHERE cmnd = '${cmndnguoinuoiduong}'`;
                const [rows, fields] = await this.sqlConn.execute(Sql3);
                if(rows.length > 0){
                    const Sql4 = `SELECT * FROM chitietgiadinh WHERE id_person = ${rows[0].id_person} AND id_chucdanh <> 1 AND id_chucdanh <> 2 AND id_chucdanh <> 7`;
                    const [rows1, fields1] = await this.sqlConn.execute(Sql4);
                    nguoinuoiduong = [...rows];
                }
            }

            if(cha.length > 0 || me.length > 0 || nguoinuoiduong.length > 0){
                const dataNew = {
                    cha : cha.length > 0 ? cha[0].id_person : null,
                    me : me.length > 0 ? me[0].id_person : null,
                    nuoi : nguoinuoiduong.length > 0 ? nguoinuoiduong[0].id_person : null,
                }
                return [true, dataNew];
            }

            return [false,{}];

        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async checkCmnd(data) {
        try {

            const Sql = `SELECT * FROM person WHERE cmnd = '${data}'`
            const [rows, fields] = await this.sqlConn.execute(Sql);
            return [rows, fields];

        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }

    async isCheckIDFamilyHaveExists(id, location) {
        try {

            const {
                tinhthanhpho,
                quanhuyen,
                phuongxa
            } = location

            async function getListDistrict(rows, sqlConn){
                let data = [];
                const length = rows.length;
                for(const i = 0; i<length; i++){
                    let Sql3 = `SELECT * FROM thon WHERE id_phuongxa = ${rows[i].id_phuongxa}`;
                    const [rows1, fields1] = await sqlConn.execute(Sql3);
                    data = [...data, ...rows];
                }
                return Promise.all(data);
            }

            if(phuongxa){
                let Sql1 = `SELECT * FROM thon WHERE id_phuongxa = ${phuongxa}`;
                let Sql2 = `SELECT * FROM giadinh
                       JOIN chitietgiadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                       JOIN person ON person.id_person=chitietgiadinh.id_person
                       WHERE chitietgiadinh.id_chucdanh = 7 AND giadinh.id_giadinh = ${id} LIMIT 1`;
                const [rows1, fields1] = await this.sqlConn.execute(Sql1);
                const [rows2, fields2] = await this.sqlConn.execute(Sql2);
                if(rows2.length <= 0){
                    return [[], []];
                }
                const idx = rows1.findIndex(({ id_thon }) => id_thon===rows2[0].id_thon);
                if(idx!==-1){
                    return [rows2, fields2];
                }
                return [[],[]];
            }else if(quanhuyen){
                let Sql1 = `SELECT * FROM phuongxa WHERE id_quanhuyen = ${quanhuyen}`;
                let Sql2 = `SELECT * FROM giadinh
                       JOIN chitietgiadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                       JOIN person ON person.id_person=chitietgiadinh.id_person
                       WHERE chitietgiadinh.id_chucdanh = 7 AND giadinh.id_giadinh = ${id} LIMIT 1`;
                const [rows1, fields1] = await this.sqlConn.execute(Sql1);
                const list = await getListDistrict(rows1, this.sqlConn);
                const [rows2, fields2] = await this.sqlConn.execute(Sql2);
                if(rows2.length <= 0){
                    return [[], []];
                }
                const idx = list.findIndex(({ id_thon }) => id_thon===rows2[0].id_thon);
                if(idx!==-1){
                    return [rows2, fields2];
                }
                return [[],[]];
            }else{
                let Sql = `SELECT * FROM giadinh
                JOIN chitietgiadinh ON chitietgiadinh.id_giadinh=giadinh.id_giadinh
                JOIN person ON person.id_person=chitietgiadinh.id_person
                WHERE chitietgiadinh.id_chucdanh = 7 AND giadinh.id_giadinh = ${id} LIMIT 1`;

                const [rows, fields] = await this.sqlConn.execute(Sql);
                return [rows,fields];
            }

        } catch (error) {
            throw new APIError({
                message: 'failed on getting data',
                status: httpStatus.INTERNAL_SERVER_ERROR,
                stack: error.stack,
                isPublic: false,
                errors: error.errors,
            })
        }
    }
}
module.exports = Excel;

