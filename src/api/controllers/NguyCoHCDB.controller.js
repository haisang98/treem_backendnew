const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
var empty = require('is-empty');


module.exports.getData_NguyCoHCDB_Controller= async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            NguyCoHCDB,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const Object = {
            id_tinhthanhpho: thanhpho ? thanhpho : null,
            id_quanhuyen: huyen ? huyen : null,
            id_phuongxa: xa ? xa : null
        };  

        var data = ""
        if(Object.id_phuongxa){
            data += "px.id_phuongxa = " + Object.id_phuongxa + " AND "
        }
        if(Object.id_quanhuyen){
            data += "qh.id_quanhuyen = " + Object.id_quanhuyen + " AND "
        }
        if(Object.id_tinhthanhpho){
            data += "ct.id_tinhthanhpho = " + Object.id_tinhthanhpho
        }

        const [rows, fields] = await NguyCoHCDB.getData_NguyCoHCDB(data);
        const DM_NCHCDB = await NguyCoHCDB.getData_DM_NCHCDB();
        const DM_CT_NCHCDB = await NguyCoHCDB.getData_DM_CT_NCHCDB();

        var nguycodacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false"];

            if(DM_CT_NCHCDB[0][i].cha == 0){

                var dcha1 = DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
               if(dcha1 > 9){
                   dcha = dcha1;
               }else{
                    dcha =  DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false"];

                var dcha1 = DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =  DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
            nguycodacbiet.push(rows[i]);
        }

        var listObjectNonDuplicate = [];
        listObjectNonDuplicate.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < listObjectNonDuplicate.length; j ++){
                if(rows[i].id_treem == listObjectNonDuplicate[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(listObjectNonDuplicate[j].nguycoHCDB[k].value == "true"){
                            rows[i].nguycoHCDB[k].value = "true";
                        }
                    }
                    listObjectNonDuplicate.splice(j, 1);
                }
            }
            listObjectNonDuplicate.push(rows[i]);
        }

        total = listObjectNonDuplicate.length;

        let page = req.query.page;
        let pagesize = req.query.pagesize;

        if(empty(page)){
            page = 1;
        }

        if(empty(pagesize)){
            pagesize = 10;
        }

        var start = (page - 1) * pagesize;
        var end = pagesize*page - 1;
        var pagination = [];
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(listObjectNonDuplicate[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(listObjectNonDuplicate[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pagesize),
                pageNumber: Number(page),
                total: total,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.searchData_NCHCDB_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            NguyCoHCDB,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa,
            thon,
            hoancanh,
            magiadinh,
            tentreem,
            timestart,
            timefinish
        } = req.query

        const Object = {
            id_tinhthanhpho: thanhpho ? thanhpho : null,
            id_quanhuyen: huyen ? huyen : null,
            id_phuongxa: xa ? xa : null,
            id_thon: thon ? thon : null,
            id_hoancanh: hoancanh ? hoancanh : null,
            id_giadinh: magiadinh ? (isNaN(magiadinh) ? -123456789 : magiadinh) : null,
            hoten: tentreem ? tentreem : null,
            ts: timestart ? timestart : null,
            tf: timefinish ? timefinish : null
        };  

        // 
        timeS = Object.ts;
        timeF = Object.tf;

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()

        
        function timeD(a, b){
            var temp = "";
            if(a != null && b != null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
            }else if(a != null && b == null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        var search = "";
        for(const k in Object){
            if(Object[k]  != null && k != 'ts' && k != 'tf'){
                if(empty(search)){
                    if(k == 'hoten'){
                        search = k + " LIKE '%" + Object[k] + "%'";
                    }else{
                        search = k + " = " + Object[k];
                    }  
                }else{
                    if(k == 'hoten'){
                        search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                    } else{
                        search += " AND " + k + " = " + Object[k] ;
                    }  
                }
            }
        } 

        if(empty(search)){
            search = timeD(timeS, timeF);
        }else{
            if(timeD(timeS, timeF) == ""){
                search += "";
            }else{
                search += " AND " + timeD(timeS, timeF);
            }
        }

        if(empty(search)){
            search = " 1 ";
        }

        //---------------------------------------------------------------------------
        // 
        
        const [rows, fields] = await NguyCoHCDB.searchData_NCHCDB(search);
        const getData_DM_NCHCDB = await NguyCoHCDB.getData_DM_NCHCDB();
        const searchData_DM_CT_NCHCDB = await NguyCoHCDB.searchData_DM_CT_NCHCDB(search);

        var nguycodacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false"];

            if(searchData_DM_CT_NCHCDB[0][i].cha == 0){

                var dcha1 = searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
               if(dcha1 > 9){
                   dcha = dcha1;
               }else{
                    dcha =  searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: getData_DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false"];

                var dcha1 = searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =  searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: getData_DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
            nguycodacbiet.push(rows[i]);
        }

        var listObjectNonDuplicate = [];
        listObjectNonDuplicate.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < listObjectNonDuplicate.length; j ++){
                if(rows[i].id_treem == listObjectNonDuplicate[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(listObjectNonDuplicate[j].nguycoHCDB[k].value == "true"){
                            rows[i].nguycoHCDB[k].value = "true";
                        }
                    }
                    listObjectNonDuplicate.splice(j, 1);
                }
            }
            listObjectNonDuplicate.push(rows[i]);
        }

        if(!listObjectNonDuplicate[0]){
            listObjectNonDuplicate = [...listObjectNonDuplicate.slice(1)];
        }
        total = listObjectNonDuplicate.length;

        let page = req.query.page;
        let pagesize = req.query.pagesize;

        if(empty(page)){
            page = 1;
        }

        if(empty(pagesize)){
            pagesize = 10;
        }

        var start = (page - 1) * pagesize;
        var end = pagesize*page - 1;
        var pagination = [];
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(listObjectNonDuplicate[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(listObjectNonDuplicate[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pagesize),
                pageNumber: Number(page),
                total: total,
                search: req.query,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}