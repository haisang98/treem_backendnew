const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
const empty = require('is-empty');


module.exports.getData_HoGiaDinh_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            DSHoGiaDinh,
        } = new Model({ sqlConn });

        const data = {
            id_tinhthanhpho: req.query.thanhpho,
            id_quanhuyen: req.query.huyen,
            id_phuongxa: req.query.xa
        }


        var getData = "";
        if(data.id_phuongxa == undefined){
            if(data.id_quanhuyen == undefined){
                getData = "ct.id_tinhthanhpho = " + data.id_tinhthanhpho
            }else{
                getData = "ct.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "qh.id_quanhuyen  = " + data.id_quanhuyen
            }
        }else{
            getData = "ct.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "qh.id_quanhuyen = " + data.id_quanhuyen + " AND " + "px.id_phuongxa  = " + data.id_phuongxa
        }
        

        const [rows, fields] = await DSHoGiaDinh.getData_HoGiaDinh(getData);

        let page = req.query.page;
        var pagesize = req.query.pagesize; //-------------------------------------------------

        if(empty(page)){
            page = 1;
        }
        
        if(empty(pagesize)){
            pagesize = 20;
        }

        var start = (page - 1) * pagesize;
        var end = pagesize*page - 1;
        var pagination = [];
        var total = rows.length;
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(rows[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(rows[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pagesize),
                pageNumber:  Number(page),
                total: total,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}


module.exports.search_HoGiaDinh_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            DSHoGiaDinh,
        } = new Model({ sqlConn });

        const {
            thon,
            magiadinh,
            tenchame,
            tentreem,
        } = req.query
        var req_f = req.query;


        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            tenchame: tenchame ? tenchame : null,
            id_giadinh: magiadinh ? magiadinh : null,
            hoten: tentreem ? tentreem : null,
        };


        var search = "";
        for(const k in Object){
            if(Object[k]  != null){
                if(empty(search)){
                    if(k === 'hoten'){
                        search = k + " LIKE '%" + Object[k] + "%'";
                    }else if(k === 'tenchame'){
                        search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                    }else{
                        search = k + " = " + Object[k];
                    }    
                }else{
                    if(k === 'hoten'){
                        search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                    }else if(k === 'tenchame'){
                        search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                    }else{
                        search += " AND " + k + " = " + Object[k] ;
                    }  
                }
            }
        } 
        

        const [rows, fields] = await DSHoGiaDinh.search_HoGiaDinh(search);

        let page = req.query.page;
        var pagesize = req.query.pagesize; //-------------------------------------------------

        if(empty(page)){
            page = 1;
        }
        
        if(empty(pagesize)){
            pagesize = 10;
        }

        var start = (page - 1) * pagesize;
        var end = pagesize*page - 1;
        var pagination = [];
        var total = rows.length;
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(rows[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(rows[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pagesize),
                pageNumber:  Number(page),
                total: total,
                search: req_f,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.searchFatherController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            DSHoGiaDinh,
        } = new Model({ sqlConn });

        const {
            thon,
            xa,
            id_person,
            hoten,
            page,
            pagesize,
        } = req.query

        const Object = {
            // id_thon: thon ? thon : null,
            // id_phuongxa: xa ? xa : null,
            id_person: id_person ? id_person : null,
            hoten: hoten ? hoten : null,
        };
console.log({Object})
console.log(typeof Object.id_person)
        const [rows, fields] = await DSHoGiaDinh.searchFather(Object);

        let page_ = page;
        let pageSize_ = pagesize;
        if(empty(page_)){
            page_ = 1;
        }
        
        if(empty(pageSize_)){
            pageSize_ = 10;
        }

        var start = (page_ - 1) * pageSize_;
        var end = pageSize_*page_ - 1;
        var pagination = [];
        var total = rows.length;
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(rows[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(rows[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pageSize_),
                pageNumber:  Number(page_),
                total: total,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.searchMotherController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            DSHoGiaDinh,
        } = new Model({ sqlConn });

        const {
            thon,
            xa,
            id_person,
            hoten,
            page,
            pagesize,
        } = req.query

        const Object = {
            // id_thon: thon ? thon : null,
            // id_phuongxa: xa ? xa : null,
            id_person: id_person ? id_person : null,
            hoten: hoten ? hoten : null,
        };

        const [rows, fields] = await DSHoGiaDinh.searchMother(Object);

        let page_ = page;
        let pageSize_ = pagesize;
        if(empty(page_)){
            page_ = 1;
        }
        
        if(empty(pageSize_)){
            pageSize_ = 10;
        }

        var start = (page_ - 1) * pageSize_;
        var end = pageSize_*page_ - 1;
        var pagination = [];
        var total = rows.length;
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(rows[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(rows[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pageSize_),
                pageNumber:  Number(page_),
                total: total,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.searchNguoiNuoiController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            DSHoGiaDinh,
        } = new Model({ sqlConn });

        const {
            thon,
            xa,
            id_person,
            hoten,
            page,
            pagesize,
        } = req.query

        const Object = {
            // id_thon: thon ? thon : null,
            // id_phuongxa: xa ? xa : null,
            id_person: id_person ? id_person : null,
            hoten: hoten ? hoten : null,
        };

        const [rows1, fields1] = await DSHoGiaDinh.searchFather(Object);
        const [rows2, fields2] = await DSHoGiaDinh.searchMother(Object);
        const rows = [...rows1, ...rows2].sort((a, b) => {
            let idA = a.id_person;
            let idB = b.id_person;

            if(idA < idB){
                return -1;
            }

            if(idA > idB){
                return 1;
            }

            return 0;
        });

        let page_ = page;
        let pageSize_ = pagesize;
        if(empty(page_)){
            page_ = 1;
        }
        
        if(empty(pageSize_)){
            pageSize_ = 10;
        }

        var start = (page_ - 1) * pageSize_;
        var end = pageSize_*page_ - 1;
        var pagination = [];
        var total = rows.length;
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(rows[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(rows[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pageSize_),
                pageNumber:  Number(page_),
                total: total,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}
