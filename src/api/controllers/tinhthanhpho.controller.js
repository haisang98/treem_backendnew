const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');


module.exports.showListTP = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            tinhthanhpho,
        } = new Model({ sqlConn });
        const [rows, fields] = await tinhthanhpho.showListTP();

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.addTinhThanhPho = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            tinhthanhpho
        } = new Model({ sqlConn });

        let tenTinhThanhPho = req.body.tenTinhThanhPho;
        
        const [rows, fields] = await tinhthanhpho.addTinhThanhPho(tenTinhThanhPho);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: rows,
                fields: fields
            })
            .end();

    } catch (error) {
        next(error);
    }
}

