const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
const empty = require('is-empty');
const Excel__ = require('exceljs');
const readXlsxFile = require('read-excel-file/node');
const { 
    columnsInfoChildren,
    columnsInfoPerson,
    columnsInfoChildrenOtherFamily,
    dataFakeOtherFamily,
    dataFakePerson, 
    dataFake, 
    columnsDownload, 
    columnsDownloadFamily, 
    columnsDownloadChildrenKTDL, 
    columnsDownloadChildrenFollowObjectKTDL 
} = require('../helpers/columnsExcel');
const path = require('path');
const { 
    onHandleHCDB, onHandleNCHCDB, onHandleHCK, onHandleHTTG,
    onHandleObjectHCDB, onHandleObjectNCHCDB, onHandleObjectHCK,
    onHandleObjectHTTG,
} = require('../helpers/handleFeatureExcel');
const {
    getAllHoanCanh, eachRow1Object, detailHTTG, dequi, eachRowObject,
} = require('../helpers/functionGetDetailChildren');
const { child } = require('winston');
const { dirname } = require('path');
const { Promise } = require('bluebird');
const moment = require('moment');

module.exports.getDocHDSD = async (req, res, next) => {
    res.download(path.join(__dirname, '../uploads/HDSD.doc'));
}

module.exports.getExcelSample = async (req, res, next) => {
    try {

        const {
            sqlConn,
        } = req.app.locals;

        const {
            Excel,
        } = new Model({ sqlConn });

        const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();
        const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();
        const [HCK, HCKFields] = await Excel.getcolumnsHCK();
        const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

        /* Sort */
        HCDB.sort((a, b) => a>b?-1:1)
        NCHCDB.sort((a, b) => a>b?-1:1)
        HCK.sort((a, b) => a>b?-1:1)
        HTTG.sort((a, b) => a>b?-1:1)

        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        /* Excel Add Children With Family */
        const workbook = new Excel__.Workbook();
        const worksheet = workbook.addWorksheet('DSTreEm');

        const columnsHCDB = HCDB.map(e => ({   
            width: 15, 
            style: { alignment: { vertical:"middle", horizontal:"center" }  }
        }))
        
        const length_ = columnsInfoChildren.length+1;
        const columns_ = [...columnsInfoChildren]

        /* Fake Data  */
        const dataFake_hcdb = Object.assign({}, HCDB);
        let i = 0;
        for(const key in dataFake_hcdb){
            delete Object.assign(dataFake_hcdb, { [`hcdb_${i}`]: 0 })[key]
            i++;
        }
        const dataFake_nchcdb = Object.assign({}, NCHCDB);
        let j = 0;
        for(const key in dataFake_nchcdb){
            delete Object.assign(dataFake_nchcdb, { [`nchcdb_${j}`]: 0 })[key]
            j++;
        }
        const dataFake_hck = Object.assign({}, HCK);
        let k = 0;
        for(const key in dataFake_hck){
            delete Object.assign(dataFake_hck, { [`hck_${k}`]: 0 })[key]
            k++;
        }
        const dataFake_httg = Object.assign({}, HTTG);
        let f = 0;
        for(const key in dataFake_httg){
            delete Object.assign(dataFake_httg, { [`httg_${f}`]: 0 })[key]
            f++;
        }

        /* Push Column */
        for(let i=length_, j=0; i<length_+HCDB.length; i++, j++){
            columns_.push({
                header: HCDB[j].ten_hoancanh,
                key: `hcdb_${j}`,
                width: 20, 
            })
        }
        for(let i=columnsInfoChildren.length+HCDB.length+2, j=0; i<columnsInfoChildren.length+HCDB.length+2+NCHCDB.length; i++, j++){
            columns_.push({
                header: NCHCDB[j].ten_hoancanh,
                key: `nchcdb_${j}`,
                width: 20, 
            })
        }

        for(let i=columnsInfoChildren.length+HCDB.length+3+NCHCDB.length, j=0; i<columnsInfoChildren.length+HCDB.length+NCHCDB.length+3+HCK.length; i++, j++){
            columns_.push({
                header: NCHCDB[j].ten_hoancanh,
                key: `hck_${j}`,
                width: 20, 
            })
        }

        for(let i=columnsInfoChildren.length+HCDB.length+4+NCHCDB.length+HCK.length, j=0; i<columnsInfoChildren.length+HCDB.length+NCHCDB.length+4+HCK.length+HTTG.length; i++, j++){
            columns_.push({
                header: HTTG[j].tenhinhthuc,
                key: `httg_${j}`,
                width: 20, 
            })
        }
        worksheet.columns = [...columns_]

        worksheet.duplicateRow(1,4,true);

        worksheet.getRow(1).values = ["Khu phố 1 - Phuờng Hiệp Tân - Quận Tân Phú - Thành phố Hồ Chí Minh"]
        worksheet.getRow(1).height = 30
        worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
        worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
        worksheet.mergeCells(1,1,1,columns_.length);

        worksheet.getRow(2).height = 30
        worksheet.getRow(2).values = []
        worksheet.getRow(2).alignment = { horizontal:"left", vertical:"middle" }
        worksheet.mergeCells(2,1,2,columns_.length);

        worksheet.getRow(3).values = ["Tổng số trẻ em: 01"]
        worksheet.getRow(3).height = 30
        worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
        worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
        worksheet.mergeCells(3,1,3,columns_.length);


        worksheet.getRow(4).height = 30
        worksheet.getRow(5).height = 90
        worksheet.getRow(5).eachCell(cell => {
            cell.alignment = { wrapText: true, horizontal: "center", vertical:"middle" }
        })
        columnsInfoChildren.forEach((value, index) => {
            worksheet.mergeCells(4,index+1,5,index+1)
        })

        worksheet.getRow(4).getCell(columnsInfoChildren.length+1).value = "Hoàn cảnh đặc biệt"
        worksheet.getRow(4).getCell(columnsInfoChildren.length+1).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }
        worksheet.getRow(4).getCell(columnsInfoChildren.length+1+HCDB.length).value = "Nguy cơ rơi vào hoàn cảnh đặc biệt"
        worksheet.getRow(4).getCell(columnsInfoChildren.length+1+HCDB.length).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }
        worksheet.getRow(4).getCell(columnsInfoChildren.length+1+HCDB.length+NCHCDB.length).value = "Hoàn cảnh khác"
        worksheet.getRow(4).getCell(columnsInfoChildren.length+1+HCDB.length+NCHCDB.length).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }
        worksheet.getRow(4).getCell(columnsInfoChildren.length+1+HCDB.length+NCHCDB.length+HCK.length).value = "Hình thức trợ giúp"
        worksheet.getRow(4).getCell(columnsInfoChildren.length+1+HCDB.length+NCHCDB.length+HCK.length).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }

        worksheet.mergeCells(4,columnsInfoChildren.length+1,4,columnsInfoChildren.length+HCDB.length);
        worksheet.mergeCells(4,columnsInfoChildren.length+HCDB.length+1,4,columnsInfoChildren.length+HCDB.length+NCHCDB.length);
        worksheet.mergeCells(4,columnsInfoChildren.length+HCDB.length+1+NCHCDB.length,4,columnsInfoChildren.length+HCDB.length+NCHCDB.length+HCK.length);
        worksheet.mergeCells(4,columnsInfoChildren.length+HCDB.length+1+NCHCDB.length+HCK.length,4,columnsInfoChildren.length+HCDB.length+NCHCDB.length+HCK.length+HTTG.length);

        // Add row using key mapping to columns
        worksheet.addRow({...dataFake,...dataFake_hcdb,...dataFake_nchcdb,...dataFake_hck,...dataFake_httg})

        /* Add Person */
        const worksheet2 = workbook.addWorksheet('DSPerson');

        const columnsPerson = [...columnsInfoPerson]

        worksheet2.columns = [...columnsPerson]

        worksheet2.duplicateRow(1,3,true);

        worksheet2.getRow(1).values = ["Khu phố 1 - Phuờng Hiệp Tân - Quận Tân Phú - Thành phố Hồ Chí Minh"]
        worksheet2.getRow(1).height = 30
        worksheet2.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
        worksheet2.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
        worksheet2.mergeCells(1,1,1,columnsPerson.length);

        worksheet2.getRow(2).height = 30
        worksheet2.getRow(2).values = []
        worksheet2.getRow(2).alignment = { horizontal:"left", vertical:"middle" }
        worksheet2.mergeCells(2,1,2,columnsPerson.length);

        worksheet2.getRow(3).values = ["Tổng số trẻ em: 01"]
        worksheet2.getRow(3).height = 30
        worksheet2.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
        worksheet2.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
        worksheet2.mergeCells(3,1,3,columnsPerson.length);

        worksheet2.addRow({...dataFakePerson})

        /* Excel Add Children Other Family */
        const worksheet3 = workbook.addWorksheet('DSTreEmKhacGiaDinh');

        const lengthOther = columnsInfoChildrenOtherFamily.length+1;
        const columnsOther = [...columnsInfoChildrenOtherFamily];

        /* Push Column */
        for(let i=lengthOther, j=0; i<lengthOther+HCDB.length; i++, j++){
            columnsOther.push({
                header: HCDB[j].ten_hoancanh,
                key: `hcdb_${j}`,
                width: 20, 
            })
        }
        for(let i=columnsInfoChildrenOtherFamily.length+HCDB.length+2, j=0; i<columnsInfoChildrenOtherFamily.length+HCDB.length+2+NCHCDB.length; i++, j++){
            columnsOther.push({
                header: NCHCDB[j].ten_hoancanh,
                key: `nchcdb_${j}`,
                width: 20, 
            })
        }

        for(let i=columnsInfoChildrenOtherFamily.length+HCDB.length+3+NCHCDB.length, j=0; i<columnsInfoChildrenOtherFamily.length+HCDB.length+NCHCDB.length+3+HCK.length; i++, j++){
            columnsOther.push({
                header: NCHCDB[j].ten_hoancanh,
                key: `hck_${j}`,
                width: 20, 
            })
        }

        for(let i=columnsInfoChildrenOtherFamily.length+HCDB.length+4+NCHCDB.length+HCK.length, j=0; i<columnsInfoChildrenOtherFamily.length+HCDB.length+NCHCDB.length+4+HCK.length+HTTG.length; i++, j++){
            columnsOther.push({
                header: HTTG[j].tenhinhthuc,
                key: `httg_${j}`,
                width: 20, 
            })
        }

        worksheet3.columns = [...columnsOther]

        worksheet3.duplicateRow(1,4,true);

        worksheet3.getRow(1).values = ["Khu phố 1 - Phuờng Hiệp Tân - Quận Tân Phú - Thành phố Hồ Chí Minh"]
        worksheet3.getRow(1).height = 30
        worksheet3.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
        worksheet3.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
        worksheet3.mergeCells(1,1,1,columns_.length);

        worksheet3.getRow(2).height = 30
        worksheet3.getRow(2).values = []
        worksheet3.getRow(2).alignment = { horizontal:"left", vertical:"middle" }
        worksheet3.mergeCells(2,1,2,columns_.length);

        worksheet3.getRow(3).values = ["Tổng số trẻ em: 01"]
        worksheet3.getRow(3).height = 30
        worksheet3.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
        worksheet3.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
        worksheet3.mergeCells(3,1,3,columns_.length);


        worksheet3.getRow(4).height = 30
        worksheet3.getRow(5).height = 90
        worksheet3.getRow(5).eachCell(cell => {
            cell.alignment = { wrapText: true, horizontal: "center", vertical:"middle" }
        })
        columnsInfoChildrenOtherFamily.forEach((value, index) => {
            worksheet3.mergeCells(4,index+1,5,index+1)
        })

        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1).value = "Hoàn cảnh đặc biệt"
        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }
        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1+HCDB.length).value = "Nguy cơ rơi vào hoàn cảnh đặc biệt"
        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1+HCDB.length).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }
        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1+HCDB.length+NCHCDB.length).value = "Hoàn cảnh khác"
        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1+HCDB.length+NCHCDB.length).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }
        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1+HCDB.length+NCHCDB.length+HCK.length).value = "Hình thức trợ giúp"
        worksheet3.getRow(4).getCell(columnsInfoChildrenOtherFamily.length+1+HCDB.length+NCHCDB.length+HCK.length).style = { alignment: { horizontal:"center", vertical:"middle" }, font: { bold:true } }

        worksheet3.mergeCells(4,columnsInfoChildrenOtherFamily.length+1,4,columnsInfoChildrenOtherFamily.length+HCDB.length);
        worksheet3.mergeCells(4,columnsInfoChildrenOtherFamily.length+HCDB.length+1,4,columnsInfoChildrenOtherFamily.length+HCDB.length+NCHCDB.length);
        worksheet3.mergeCells(4,columnsInfoChildrenOtherFamily.length+HCDB.length+1+NCHCDB.length,4,columnsInfoChildrenOtherFamily.length+HCDB.length+NCHCDB.length+HCK.length);
        worksheet3.mergeCells(4,columnsInfoChildrenOtherFamily.length+HCDB.length+1+NCHCDB.length+HCK.length,4,columnsInfoChildrenOtherFamily.length+HCDB.length+NCHCDB.length+HCK.length+HTTG.length);

        // Add row using key mapping to columns
        worksheet3.addRow({...dataFakeOtherFamily,...dataFake_hcdb,...dataFake_nchcdb,...dataFake_hck,...dataFake_httg})

        /* -------------------------------- */

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DSTreEm.xls");
//application/vnd.openxmlformats-officedocument.wordprocessingml.document
        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });

    } catch (error) {
        next(error);
    }
}

module.exports.uploadFile = async (req, res, next) => {
    try {
        
        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }


        if(req.file == undefined) {
            return res.status(400)
                      .json({
                          code: 400,
                          message: "Please upload ab excel file!"
                      })
        }

        let path__ = path.join(__dirname, '../uploads', req.file.filename);

        const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();
        const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();
        const [HCK, HCKFields] = await Excel.getcolumnsHCK();
        const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

        /* Sort */
        HCDB.sort((a, b) => a>b?-1:1)
        NCHCDB.sort((a, b) => a>b?-1:1)
        HCK.sort((a, b) => a>b?-1:1)
        HTTG.sort((a, b) => a>b?-1:1)

        const workbook = new Excel__.Workbook();
        workbook.xlsx.readFile(path__).then(async () => {
            try{
            const sh = workbook.getWorksheet("DSTreEm");
                
                // async function handleAsync(){
                    let data = [];
                    let dataChiTieu = [];
                    let arrayHCDB = [];
                    let arrayNCHCDB = [];
                    let arrayHCK = [];
                    let arrayHTTG = [];
                    sh.eachRow({ includeEmpty: true }, function(row, rowNumber){
                        if(rowNumber===4){
                            row.eachCell((cell, cellNumber) => {
                                if(JSON.stringify(cell.value) == "Hoàn cảnh đặc biệt"){
                                    arrayHCDB.push(cellNumber)
                                }else if(JSON.stringify(cell.value) == "Nguy cơ rơi vào hoàn cảnh đặc biệt"){
                                    arrayNCHCDB.push(cellNumber)
                                }else if(JSON.stringify(cell.value) == "Hoàn cảnh khác"){
                                    arrayHCK.push(cellNumber)
                                }else if(JSON.stringify(cell.value) == "Hình thức trợ giúp"){
                                    arrayHTTG.push(cellNumber)
                                }
                            })
                        }
                        if(rowNumber===5){
                            dataChiTieu.push([...row.values])
                        }
                        if(rowNumber>=6){
                            data.push(row.values);
                        }
                        
                    })

                    async function handleData(){
                        let dataLength = data.length;
                        let dataFinal = [];
                        for(let i = 0; i<dataLength; i++){
                            let id = data[i][1];
                            let hoten = data[i][2];
                            let ngaysinh = data[i][3];
                            let gioitinh = data[i][4];
                            let dantoc = data[i][5];

                            if(id && id > 0 && hoten && ngaysinh && gioitinh && dantoc){
                                const [dataFamily, dataFamilyFields] = await Excel.isCheckIDFamilyHaveExists(id, object);
                                if(dataFamily.length > 0){
                                    const rowValues = [...data[i], { cha: dataFamily[0].cha }, { me: dataFamily[0].me} ];
                                    dataFinal.push(rowValues)
                                }
                            }
                        }
                        return Promise.all(dataFinal);
                    }

                    const res = await handleData();
                    const rows4SH = sh.getRow(4).values;
                    const rows5SH = sh.getRow(5).values;
                    await Excel.AddChildrenWithFileExcel(res, rows4SH, rows5SH);

                    /* DS Person */
                    const sh2 = workbook.getWorksheet("DSPerson");
                
                // async function handleAsync(){
                    let dataPerson = [];
                    sh2.eachRow({ includeEmpty: true }, function(row, rowNumber){
                        if(rowNumber===4){
                            row.eachCell((cell, cellNumber) => {
                                console.log(`Cell ${cellNumber} : ${cell}`)
                            })
                        }
                        if(rowNumber>=5){
                            dataPerson.push(row.values);
                        }
                        
                    })

                    async function handleDataPerson(){
                        let dataLength = dataPerson.length;
                        let dataFinal = [];
                        for(let i = 0; i<dataLength; i++){
                            let hoten = dataPerson[i][1];
                            let cmnd = dataPerson[i][2];
                            let ngaysinh = dataPerson[i][3];
                            let dantoc = dataPerson[i][4];
                            let gioitinh = dataPerson[i][5];

                            if(cmnd && hoten && ngaysinh && gioitinh && dantoc){
                                const [checkCmnd, checkCmndFields] = await Excel.checkCmnd(cmnd);
                                if(checkCmnd.length <= 0){
                                    const rowValues = [...dataPerson[i]];
                                    dataFinal.push(rowValues)
                                }
                            }
                        }
                        return Promise.all(dataFinal);
                    }

                    const dataPersonFinal = await handleDataPerson();
                    await Excel.addPerson(dataPersonFinal)

                    /* DS Children Other Family */
                    const sh3 = workbook.getWorksheet("DSTreEmKhacGiaDinh");
                
                // async function handleAsync(){
                    let dataOther = [];
                    sh3.eachRow({ includeEmpty: true }, function(row, rowNumber){
                        if(rowNumber===4){
                            row.eachCell((cell, cellNumber) => {
                                if(JSON.stringify(cell.value) == "Hoàn cảnh đặc biệt"){
                                    arrayHCDB.push(cellNumber)
                                }else if(JSON.stringify(cell.value) == "Nguy cơ rơi vào hoàn cảnh đặc biệt"){
                                    arrayNCHCDB.push(cellNumber)
                                }else if(JSON.stringify(cell.value) == "Hoàn cảnh khác"){
                                    arrayHCK.push(cellNumber)
                                }else if(JSON.stringify(cell.value) == "Hình thức trợ giúp"){
                                    arrayHTTG.push(cellNumber)
                                }
                            })
                        }
                        if(rowNumber===5){
                            // console.log(row.values)
                        }
                        if(rowNumber>=6){
                            dataOther.push(row.values);
                        }
                        
                    })
console.log({dataOther})
                    async function handleDataOtherFamily(){
                        let dataLength = dataOther.length;
                        let dataFinal = [];
                        for(let i = 0; i<dataLength; i++){
                            let step = 0;
                            let countStepComplete = 3;
                            let thon = dataOther[i][1];
                            let hoancanhgiadinh = (((dataOther[i][5] && dataOther[i][6]) || (dataOther[i][5] == 0 && dataOther[i][6] == 0) || (dataOther[i][5] == 1 && dataOther[i][6] == 0) || (dataOther[i][5] == 0 && dataOther[i][6] == 1)) ? ((dataOther[i][5] == 1 && dataOther[i][6] == 1) ? null : ((dataOther[i][5] == 0 && dataOther[i][6] == 0) ? 3 : (dataOther[i][5] == 1 ? 1 : (dataOther[i][6] == 1 ? 2 : null)))) : null);
                            let cmndcha = dataOther[i][2];
                            let cmndme = dataOther[i][3];
                            let cmndnguoinuoiduong = dataOther[i][4];
                            let hoten = dataOther[i][8];
                            let ngaysinh = dataOther[i][9];
                            let dantoc = dataOther[i][10];
                            let gioitinh = dataOther[i][11];
                            let __dataCheckPerson__ = null;
                            let __IDThon__ = null;

                            if(thon && hoancanhgiadinh){
                                console.log(thon, hoancanhgiadinh)
                                const [checkFamily, IDThon] = await Excel.CanAddFamily(thon, hoancanhgiadinh, object);
                                console.log(checkFamily, IDThon)
                                if(checkFamily){
                                    __IDThon__ = IDThon;
                                    step++;
                                }
                                
                            }

                            if(cmndcha || cmndme || cmndnguoinuoiduong){
                                console.log(cmndcha, cmndme, cmndnguoinuoiduong)
                                const [checkPerson, dataCheckPerson] = await Excel.isCheckPerson(cmndcha, cmndme, cmndnguoinuoiduong);
                                if(checkPerson){
                                    __dataCheckPerson__ = {...dataCheckPerson}
                                    step++;
                                }
                                
                            }

                            if(hoten && ngaysinh && gioitinh && dantoc){
                                console.log("CAC TRUONG KHACS")
                                step++;
                            }

                            if(step === countStepComplete){
                                const rowValues = [...dataOther[i], { hoancanhgiadinh : hoancanhgiadinh }, { member: __dataCheckPerson__ }, { IDThon: __IDThon__ }]
                                dataFinal.push(rowValues);
                            }
                        }
                        return Promise.all(dataFinal);
                    }

                    const dataOtherFamily = await handleDataOtherFamily();
                    console.log({dataOtherFamily})
                    const rows4SHOtherFamily = sh3.getRow(4).values;
                    const rows5SHOtherFamily = sh3.getRow(5).values;
                    await Excel.AddChildrenOtherFamilyFileExcel(dataOtherFamily, rows4SHOtherFamily, rows5SHOtherFamily);

                }catch(err){
                    console.log(err);
                }
            })

            return res.status(200).json({
                code: httpStatus.OK,
                message: 'Thực hiện thêm thành công',
            }).end();

    } catch (error) {
        next(error)
    }
}

module.exports.downloadFileExcel = async (req, res, next) => {
    try {
        
        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })


        /* req */
        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, hoancanh,
            magiadinh, tentreem, timestart, timefinish,
        } = req.query

        // const {
        //     thon, huyen_, xa_
        // } = req.params

        const data = {
            phuongxa : xa ? xa : null,
            quanhuyen : huyen ? huyen : null,
            tinhthanhpho : thanhpho ? thanhpho : null,
            thon: thon ? thon : null,
            tentreem: tentreem ? tentreem : null,
            hoancanh: hoancanh ? hoancanh : null,
            magiadinh: magiadinh ? magiadinh : null,
            timestart: timestart ? timestart : null,
            timefinish: timefinish ? timefinish : null,
        }

        /* req.params.thon */
        const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();
        const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();
        const [HCK, HCKFields] = await Excel.getcolumnsHCK();
        const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

        /* Sort */
        HCDB.sort((a, b) => a>b?-1:1)
        NCHCDB.sort((a, b) => a>b?-1:1)
        HCK.sort((a, b) => a>b?-1:1)
        HTTG.sort((a, b) => a>b?-1:1)
        
        const dataFormHelpers = await onHandleHCDB(req);

        /* Excel */
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){
                const [village, villageFields] = await Excel.findVillage(thon);
                const [childrens, childrensFields] = await Excel.getChildrenFollowVillage(thon);

                const worksheet = workbook.addWorksheet(village[0].tenthon);

                const columnsHCDB = HCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                const columns = [...columnsDownload,...columnsHCDB]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataFormHelpers.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhdacbiet.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hcdb_${n}`] = dataFormHelpers[t].hoancanhdacbiet[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                worksheet.addRows(dataEnd);
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillage(ward[i].id_thon);
                    data = [...data, ...childrens];
                }

                const worksheet = workbook.addWorksheet(tenxa);

                const columnsHCDB = HCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhdacbiet.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hcdb_${n}`] = dataFormHelpers[t].hoancanhdacbiet[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsHCDB]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);
            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillage(ward[i].id_thon);
                    data = [...data, ...childrens];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);

                const columnsHCDB = HCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhdacbiet.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hcdb_${n}`] = dataFormHelpers[t].hoancanhdacbiet[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsHCDB]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DanhSachTreEm.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });

    } catch (error) {
        next(error)
    }
}

module.exports.downloadFileExcelNCHCDB = async (req, res, next) => {
    try {
        
        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })


        /* req */
        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, hoancanh,
            magiadinh, tentreem, timestart, timefinish,
        } = req.query

        // const {
        //     thon, huyen_, xa_
        // } = req.params

        const data = {
            phuongxa : xa ? xa : null,
            quanhuyen : huyen ? huyen : null,
            tinhthanhpho : thanhpho ? thanhpho : null,
            thon: thon ? thon : null,
            tentreem: tentreem ? tentreem : null,
            hoancanh: hoancanh ? hoancanh : null,
            magiadinh: magiadinh ? magiadinh : null,
            timestart: timestart ? timestart : null,
            timefinish: timefinish ? timefinish : null,
        }

        /* req.params.thon */
        const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();
        const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();
        const [HCK, HCKFields] = await Excel.getcolumnsHCK();
        const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

        /* Sort */
        HCDB.sort((a, b) => a>b?-1:1)
        NCHCDB.sort((a, b) => a>b?-1:1)
        HCK.sort((a, b) => a>b?-1:1)
        HTTG.sort((a, b) => a>b?-1:1)
        
        const dataFormHelpers = await onHandleNCHCDB(req);

        /* Excel */
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){
                const [village, villageFields] = await Excel.findVillage(thon);
                const [childrens, childrensFields] = await Excel.getChildrenFollowVillageNCHCDB(thon);

                const worksheet = workbook.addWorksheet(village[0].tenthon);

                const columnsNCHCDB = NCHCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `nchcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                const columns = [...columnsDownload,...columnsNCHCDB]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataFormHelpers.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].nguycoHCDB.length;
                            for(let n = 0; n<length__; n++){
                                hc[`nchcdb_${n}`] = dataFormHelpers[t].nguycoHCDB[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                worksheet.addRows(dataEnd);
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageNCHCDB(ward[i].id_thon);
                    data = [...data, ...childrens];
                }

                const worksheet = workbook.addWorksheet(tenxa);

                const columnsNCHCDB = NCHCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `nchcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].nguycoHCDB.length;
                            for(let n = 0; n<length__; n++){
                                hc[`nchcdb_${n}`] = dataFormHelpers[t].nguycoHCDB[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsNCHCDB]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);
            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageNCHCDB(ward[i].id_thon);
                    data = [...data, ...childrens];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);

                const columnsNCHCDB = NCHCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `nchcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].nguycoHCDB.length;
                            for(let n = 0; n<length__; n++){
                                hc[`nchcdb_${n}`] = dataFormHelpers[t].nguycoHCDB[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsNCHCDB]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DanhSachTreEm.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });
    } catch (error) {
        next(error)
    }
}

module.exports.downloadFileExcelHCK = async (req, res, next) => {
    try {
        
        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })


        /* req */
        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, hoancanh,
            magiadinh, tentreem, timestart, timefinish,
        } = req.query

        // const {
        //     thon, huyen_, xa_
        // } = req.params

        const data = {
            phuongxa : xa ? xa : null,
            quanhuyen : huyen ? huyen : null,
            tinhthanhpho : thanhpho ? thanhpho : null,
            thon: thon ? thon : null,
            tentreem: tentreem ? tentreem : null,
            hoancanh: hoancanh ? hoancanh : null,
            magiadinh: magiadinh ? magiadinh : null,
            timestart: timestart ? timestart : null,
            timefinish: timefinish ? timefinish : null,
        }

        /* req.params.thon */
        const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();
        const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();
        const [HCK, HCKFields] = await Excel.getcolumnsHCK();
        const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

        /* Sort */
        HCDB.sort((a, b) => a>b?-1:1)
        NCHCDB.sort((a, b) => a>b?-1:1)
        HCK.sort((a, b) => a>b?-1:1)
        HTTG.sort((a, b) => a>b?-1:1)
        
        const dataFormHelpers = await onHandleHCK(req);

        /* Excel */
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){
                const [village, villageFields] = await Excel.findVillage(thon);
                const [childrens, childrensFields] = await Excel.getChildrenFollowVillageHCK(thon);

                const worksheet = workbook.addWorksheet(village[0].tenthon);

                const columnsHCK = HCK.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hck_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                const columns = [...columnsDownload,...columnsHCK]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataFormHelpers.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhkhac.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hck_${n}`] = dataFormHelpers[t].hoancanhkhac[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                worksheet.addRows(dataEnd);
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageHCK(ward[i].id_thon);
                    data = [...data, ...childrens];
                }

                const worksheet = workbook.addWorksheet(tenxa);

                const columnsHCK = HCK.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hck_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhkhac.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hck_${n}`] = dataFormHelpers[t].hoancanhkhac[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsHCK]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);
            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageHCK(ward[i].id_thon);
                    data = [...data, ...childrens];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);

                const columnsHCK = HCK.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hck_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhkhac.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hck_${n}`] = dataFormHelpers[t].hoancanhkhac[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsHCK]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DanhSachTreEm.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });

    } catch (error) {
        next(error)
    }
}

module.exports.downloadFileExcelHTTG = async (req, res, next) => {
    try {
        
        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })


        /* req */
        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, hoancanh,
            magiadinh, tentreem, timestart, timefinish,
        } = req.query

        // const {
        //     thon, huyen_, xa_
        // } = req.params

        const data = {
            phuongxa : xa ? xa : null,
            quanhuyen : huyen ? huyen : null,
            tinhthanhpho : thanhpho ? thanhpho : null,
            thon: thon ? thon : null,
            tentreem: tentreem ? tentreem : null,
            hoancanh: hoancanh ? hoancanh : null,
            magiadinh: magiadinh ? magiadinh : null,
            timestart: timestart ? timestart : null,
            timefinish: timefinish ? timefinish : null,
        }

        /* req.params.thon */
        const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();
        const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();
        const [HCK, HCKFields] = await Excel.getcolumnsHCK();
        const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

        /* Sort */
        HCDB.sort((a, b) => a>b?-1:1)
        NCHCDB.sort((a, b) => a>b?-1:1)
        HCK.sort((a, b) => a>b?-1:1)
        HTTG.sort((a, b) => a>b?-1:1)
        
        const dataFormHelpers = await onHandleHTTG(req);

        /* Excel */
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){
                const [village, villageFields] = await Excel.findVillage(thon);
                const [childrens, childrensFields] = await Excel.getChildrenFollowVillageHTTG(thon);

                const worksheet = workbook.addWorksheet(village[0].tenthon);

                const columnsHTTG = HTTG.map((e, index) => ({
                    header: e.tenhinhthuc,
                    key: `httg_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                const columns = [...columnsDownload,...columnsHTTG]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataFormHelpers.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hinhthuctrogiup.length;
                            for(let n = 0; n<length__; n++){
                                hc[`httg_${n}`] = dataFormHelpers[t].hinhthuctrogiup[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                worksheet.addRows(dataEnd);
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageHTTG(ward[i].id_thon);
                    data = [...data, ...childrens];
                }

                const worksheet = workbook.addWorksheet(tenxa);

                const columnsHTTG = HTTG.map((e, index) => ({
                    header: e.tenhinhthuc,
                    key: `httg_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hinhthuctrogiup.length;
                            for(let n = 0; n<length__; n++){
                                hc[`httg_${n}`] = dataFormHelpers[t].hinhthuctrogiup[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsHTTG]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);
            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageHTTG(ward[i].id_thon);
                    data = [...data, ...childrens];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);

                const columnsHTTG = HTTG.map((e, index) => ({
                    header: e.tenhinhthuc,
                    key: `httg_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))
                
                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<data.length; l++){
                        if(data[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hinhthuctrogiup.length;
                            for(let n = 0; n<length__; n++){
                                hc[`httg_${n}`] = dataFormHelpers[t].hinhthuctrogiup[n].value == true ? 1 : 0
                            }
                            dataEnd.push({
                                ...data[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownload,...columnsHTTG]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DanhSachTreEm.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });

    } catch (error) {
        next(error)
    }
}



module.exports.downloadFileExcelFamily = async (req, res, next) => {
    try {
        
        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon,
            magiadinh, tentreem, tenchame
        } = req.query

        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            tenchame: tenchame ? tenchame : null,
            id_giadinh: magiadinh ? magiadinh : null,
            hoten: tentreem ? tentreem : null,
        };


        function whereSearch(Object){
            let search = "";
            for(const k in Object){
                if(Object[k]  != null){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }
            return search;
        }

        // const data = {
        //     phuongxa : xa ? xa : null,
        //     quanhuyen : huyen ? huyen : null,
        //     tinhthanhpho : thanhpho ? thanhpho : null,
        //     thon: thon ? thon : null,
        //     tentreem: tentreem ? tentreem : null,
        //     magiadinh: magiadinh ? magiadinh : null,
        //     nguoinuoi: nguoinuoi ? nguoinuoi : null,
        // }

        /* Excel */
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){
                const [village, villageFields] = await Excel.findVillage(thon);
                const [familys, familysFields] = await Excel.getFamilyFollowVillage(whereSearch(Object));

                const worksheet = workbook.addWorksheet(village[0].tenthon);
                
                const columns = [...columnsDownloadFamily]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH HỘ GIA ĐÌNH"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số hộ gia đình: ${familys.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                let dataEnd = [];
                    for(l = 0; l<familys.length; l++){
                        if(familys[l].hoancanh === 3){
                            dataEnd.push({
                                ...familys[l],
                                hongheo: 0,
                                hocanngheo: 0
                            })
                        }else if(familys[l].hoancanh === 1){
                            dataEnd.push({
                                ...familys[l],
                                hongheo: 1,
                                hocanngheo: 0
                            })
                        }else if(familys[l].hoancanh === 2){
                            dataEnd.push({
                                ...familys[l],
                                hongheo: 0,
                                hocanngheo: 1
                            })
                        }else{
                            /* TODO SOMETHING */
                        }
                    }

                worksheet.addRows(dataEnd);
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [familys, familysFields] = await Excel.getFamilyFollowVillage(whereSearch(ObjectFollowWard));
                    data = [...data, ...familys];
                }

                const worksheet = workbook.addWorksheet(tenxa);
                

                let dataEnd = [];
                    for(l = 0; l<data.length; l++){
                        if(data[l].hoancanh === 3){
                            dataEnd.push({
                                ...data[l],
                                hongheo: 0,
                                hocanngheo: 0
                            })
                        }else if(data[l].hoancanh === 1){
                            dataEnd.push({
                                ...data[l],
                                hongheo: 1,
                                hocanngheo: 0
                            })
                        }else if(data[l].hoancanh === 2){
                            dataEnd.push({
                                ...data[l],
                                hongheo: 0,
                                hocanngheo: 1
                            })
                        }else{
                            /* TODO SOMETHING */
                        }
                    }

                const columns = [...columnsDownloadFamily]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH HỘ GIA ĐÌNH"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số hộ gia đình: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);
            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [familys, familysFields] = await Excel.getFamilyFollowVillage(whereSearch(ObjectFollowWard));
                    data = [...data, ...familys];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);
                
                let dataEnd = [];
                    for(l = 0; l<data.length; l++){
                        if(data[l].hoancanh === 3){
                            dataEnd.push({
                                ...data[l],
                                hongheo: 0,
                                hocanngheo: 0
                            })
                        }else if(data[l].hoancanh === 1){
                            dataEnd.push({
                                ...data[l],
                                hongheo: 1,
                                hocanngheo: 0
                            })
                        }else if(data[l].hoancanh === 2){
                            dataEnd.push({
                                ...data[l],
                                hongheo: 0,
                                hocanngheo: 1
                            })
                        }else{
                            /* TODO SOMETHING */
                        }
                    }

                const columns = [...columnsDownloadFamily]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH HỘ GIA ĐÌNH"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số hộ gia đình: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(dataEnd);
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DSHoGiaDinh.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });
    } catch (error) {
        next(error)
    }
}


/* ---- */
module.exports.downloadFileExcelChildrenKTDL = async (req, res, next) => {
    try {
        
        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon,
            dotuoitu, dotuoiden
        } = req.query

        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            dotuoitu: dotuoitu ? dotuoitu : null,
            dotuoiden: dotuoiden ? dotuoiden : null,
        };

        // timeS = Object.ts;
        // timeF = Object.tf;

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()

        // function timeD(a, b){
        //     let temp = "";
        //     if(a != null && b != null){
        //         temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
        //     }else if(a != null && b == null){
        //         temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
        //     }else if(a == null && b != null){
        //         temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
        //     }else if(a == null && b == null){
        //         temp = "";
        //     }
        //     return temp;
        // }

        function CalculatorAge(fromAge, toAge){
            const currentDate = new Date();
            const from_ = currentDate.getFullYear() - fromAge;
            const to_ = currentDate.getFullYear() - toAge;
            const tuoiTu = new Date(`${from_}-01-01`);
            const tuoiDen = new Date(`${to_}-01-01`);
            const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
            const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
            return [tT,tD];
        }

        function aboutAge(a, b){
            let temp = "";
            if(a != null && b != null){
                const [from, to] = CalculatorAge(a,b);
                temp = "(ngaysinh BETWEEN '" + from + "' AND '" + to + "')";
            }else if(a != null && b == null){
                /* TH fromAge Exists AND toAge undefined */
                const from_ = currentDate.getFullYear() - a;
                const tuoiTu = new Date(`${from_}-01-01`);
                const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
                temp = "(ngaysinh BETWEEN '" + tT + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                /* TH fromAge undefined AND toAge Exists */
                const to_ = currentDate.getFullYear() - a;
                const tuoiDen = new Date(`${to_}-01-01`);
                const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + tD + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        // if(empty(search)){
        //     search = aboutAge(agesFrom, comeOfAge);
        // }else{
        //     if(aboutAge(agesFrom, comeOfAge) == ""){
        //         search += "";
        //     }else{
        //         search += " AND " + aboutAge(agesFrom, comeOfAge);
        //     }
        // }

        // if(empty(search)){
        //     search = " 1 ";
        // }

        function whereSearch(Object){
            let agesFrom = Object.dotuoitu;
            let comeOfAge = Object.dotuoiden;
            let search = "";
            for(const k in Object){
                if(Object[k]  != null && k != 'dotuoitu' && k != 'dotuoiden'){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }

            if(empty(search)){
                search = aboutAge(agesFrom, comeOfAge);
            }else{
                if(aboutAge(agesFrom, comeOfAge) == ""){
                    search += "";
                }else{
                    search += " AND " + aboutAge(agesFrom, comeOfAge);
                }
            }

            if(empty(search)){
                search = " 1 ";
            }


            return search;
        }

        // const data = {
        //     phuongxa : xa ? xa : null,
        //     quanhuyen : huyen ? huyen : null,
        //     tinhthanhpho : thanhpho ? thanhpho : null,
        //     thon: thon ? thon : null,
        //     tentreem: tentreem ? tentreem : null,
        //     magiadinh: magiadinh ? magiadinh : null,
        //     nguoinuoi: nguoinuoi ? nguoinuoi : null,
        // }

        /* Excel */
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){
                const [village, villageFields] = await Excel.findVillage(thon);
                const [childrens, childrensFields] = await Excel.getChildrenFollowVillageKTDL(whereSearch(Object));

                const worksheet = workbook.addWorksheet(village[0].tenthon);
                
                const columns = [...columnsDownloadChildrenKTDL]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${childrens.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                // let dataEnd = [];
                //     for(l = 0; l<childrens.length; l++){
                //         if(childrens[l].hoancanh === 0){
                //             dataEnd.push({
                //                 ...familys[l],
                //                 hongheo: 0,
                //                 hocanngheo: 0
                //             })
                //         }else if(familys[l].hoancanh === 1){
                //             dataEnd.push({
                //                 ...familys[l],
                //                 hongheo: 1,
                //                 hocanngheo: 0
                //             })
                //         }else if(familys[l].hoancanh === 2){
                //             dataEnd.push({
                //                 ...familys[l],
                //                 hongheo: 0,
                //                 hocanngheo: 1
                //             })
                //         }else{
                //             /* TODO SOMETHING */
                //         }
                //     }

                worksheet.addRows(childrens);
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageKTDL(whereSearch(ObjectFollowWard));
                    data = [...data, ...childrens];
                }

                const worksheet = workbook.addWorksheet(tenxa);
                

                // let dataEnd = [];
                //     for(l = 0; l<data.length; l++){
                //         if(data[l].hoancanh === 0){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 1){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 1,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 2){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 1
                //             })
                //         }else{
                //             /* TODO SOMETHING */
                //         }
                //     }

                const columns = [...columnsDownloadChildrenKTDL]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${data.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(data);
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);
            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageKTDL(whereSearch(ObjectFollowWard));
                    data = [...data, ...childrens];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);
                
                // let dataEnd = [];
                //     for(l = 0; l<data.length; l++){
                //         if(data[l].hoancanh === 0){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 1){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 1,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 2){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 1
                //             })
                //         }else{
                //             /* TODO SOMETHING */
                //         }
                //     }

                const columns = [...columnsDownloadChildrenKTDL]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${data.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(data);
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DSHoGiaDinh.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });
    } catch (error) {
        next(error)
    }
}

module.exports.downloadFileExcelChildrenNormalKTDL = async (req, res, next) => {
    try {
        
        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn
        } = req.app.locals

        const {
            Excel
        } = new Model({ sqlConn })

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon,
            dotuoitu, dotuoiden
        } = req.query

        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            dotuoitu: dotuoitu ? dotuoitu : null,
            dotuoiden: dotuoiden ? dotuoiden : null,
        };

        // timeS = Object.ts;
        // timeF = Object.tf;

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()

        // function timeD(a, b){
        //     let temp = "";
        //     if(a != null && b != null){
        //         temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
        //     }else if(a != null && b == null){
        //         temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
        //     }else if(a == null && b != null){
        //         temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
        //     }else if(a == null && b == null){
        //         temp = "";
        //     }
        //     return temp;
        // }

        function CalculatorAge(fromAge, toAge){
            const currentDate = new Date();
            const from_ = currentDate.getFullYear() - fromAge;
            const to_ = currentDate.getFullYear() - toAge;
            const tuoiTu = new Date(`${from_}-01-01`);
            const tuoiDen = new Date(`${to_}-01-01`);
            const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
            const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
            return [tT,tD];
        }

        function aboutAge(a, b){
            let temp = "";
            if(a != null && b != null){
                const [from, to] = CalculatorAge(a,b);
                temp = "(ngaysinh BETWEEN '" + from + "' AND '" + to + "')";
            }else if(a != null && b == null){
                /* TH fromAge Exists AND toAge undefined */
                const from_ = currentDate.getFullYear() - a;
                const tuoiTu = new Date(`${from_}-01-01`);
                const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
                temp = "(ngaysinh BETWEEN '" + tT + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                /* TH fromAge undefined AND toAge Exists */
                const to_ = currentDate.getFullYear() - a;
                const tuoiDen = new Date(`${to_}-01-01`);
                const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + tD + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        // if(empty(search)){
        //     search = aboutAge(agesFrom, comeOfAge);
        // }else{
        //     if(aboutAge(agesFrom, comeOfAge) == ""){
        //         search += "";
        //     }else{
        //         search += " AND " + aboutAge(agesFrom, comeOfAge);
        //     }
        // }

        // if(empty(search)){
        //     search = " 1 ";
        // }

        function whereSearch(Object){
            let agesFrom = Object.dotuoitu;
            let comeOfAge = Object.dotuoiden;
            let search = "";
            for(const k in Object){
                if(Object[k]  != null && k != 'dotuoitu' && k != 'dotuoiden'){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }

            if(empty(search)){
                search = aboutAge(agesFrom, comeOfAge);
            }else{
                if(aboutAge(agesFrom, comeOfAge) == ""){
                    search += "";
                }else{
                    search += " AND " + aboutAge(agesFrom, comeOfAge);
                }
            }

            if(empty(search)){
                search = " 1 ";
            }


            return search;
        }

        // const data = {
        //     phuongxa : xa ? xa : null,
        //     quanhuyen : huyen ? huyen : null,
        //     tinhthanhpho : thanhpho ? thanhpho : null,
        //     thon: thon ? thon : null,
        //     tentreem: tentreem ? tentreem : null,
        //     magiadinh: magiadinh ? magiadinh : null,
        //     nguoinuoi: nguoinuoi ? nguoinuoi : null,
        // }

        /* Excel */
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){
                const [village, villageFields] = await Excel.findVillage(thon);
                const [childrens, childrensFields] = await Excel.getChildrenFollowVillageNormalKTDL(whereSearch(Object));

                const worksheet = workbook.addWorksheet(village[0].tenthon);
                
                const columns = [...columnsDownloadChildrenKTDL]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${childrens.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                // let dataEnd = [];
                //     for(l = 0; l<childrens.length; l++){
                //         if(childrens[l].hoancanh === 0){
                //             dataEnd.push({
                //                 ...familys[l],
                //                 hongheo: 0,
                //                 hocanngheo: 0
                //             })
                //         }else if(familys[l].hoancanh === 1){
                //             dataEnd.push({
                //                 ...familys[l],
                //                 hongheo: 1,
                //                 hocanngheo: 0
                //             })
                //         }else if(familys[l].hoancanh === 2){
                //             dataEnd.push({
                //                 ...familys[l],
                //                 hongheo: 0,
                //                 hocanngheo: 1
                //             })
                //         }else{
                //             /* TODO SOMETHING */
                //         }
                //     }

                worksheet.addRows(childrens);
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageNormalKTDL(whereSearch(ObjectFollowWard));
                    data = [...data, ...childrens];
                }

                const worksheet = workbook.addWorksheet(tenxa);
                

                // let dataEnd = [];
                //     for(l = 0; l<data.length; l++){
                //         if(data[l].hoancanh === 0){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 1){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 1,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 2){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 1
                //             })
                //         }else{
                //             /* TODO SOMETHING */
                //         }
                //     }

                const columns = [...columnsDownloadChildrenKTDL]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${data.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(data);
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);
            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let data = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [childrens, childrensFields] = await Excel.getChildrenFollowVillageNormalKTDL(whereSearch(ObjectFollowWard));
                    data = [...data, ...childrens];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);
                
                // let dataEnd = [];
                //     for(l = 0; l<data.length; l++){
                //         if(data[l].hoancanh === 0){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 1){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 1,
                //                 hocanngheo: 0
                //             })
                //         }else if(data[l].hoancanh === 2){
                //             dataEnd.push({
                //                 ...data[l],
                //                 hongheo: 0,
                //                 hocanngheo: 1
                //             })
                //         }else{
                //             /* TODO SOMETHING */
                //         }
                //     }

                const columns = [...columnsDownloadChildrenKTDL]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${data.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);

                worksheet.addRows(data);
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DSTreEmBinhThuong.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });
    } catch (error) {
        next(error)
    }
}

module.exports.downloadFileExcelChildrenFollowObjectKTDL = async (req, res, next) => {
    try {

        /* Init */
        const StylesXform = require("exceljs/lib/xlsx/xform/style/styles-xform.js");
        const origStylesXformInit = StylesXform.prototype.init;
        StylesXform.prototype.init = function() {
            origStylesXformInit.apply(this, arguments);
            this._addFont({size: 9, color: {theme: 1}, name: 'Times New Roman', family: 2, scheme: 'minor'});
        };

        const {
            sqlConn,
        } = req.app.locals;

        const {
            HoanCanhDacBiet, NguyCoHCDB, HoanCanhKhac, HinhThucTroGiup, QuanLyHoSoTreEm, Excel
        } = new Model({ sqlConn });

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, dotuoitu, dotuoiden,
            hoancanh,
        } = req.query
        
        const Object = {
            id_tinhthanhpho: thanhpho ? thanhpho : null,
            id_quanhuyen: huyen ? huyen : null,
            id_phuongxa: xa ? xa : null,
            id_thon: thon ? thon : null,
            dotuoitu: dotuoitu ? dotuoitu : null,
            dotuoiden: dotuoiden ? dotuoiden : null,
        }; 
        
        //---------------------------------------------------------------------------

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()

        function CalculatorAge(fromAge, toAge){
            const currentDate = new Date();
            const from_ = currentDate.getFullYear() - fromAge;
            const to_ = currentDate.getFullYear() - toAge;
            const tuoiTu = new Date(`${from_}-01-01`);
            const tuoiDen = new Date(`${to_}-01-01`);
            const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
            const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
            return [tT,tD];
        }

        function aboutAge(a, b){
            let temp = "";
            if(a != null && b != null){
                const [from, to] = CalculatorAge(a,b);
                temp = "(ngaysinh BETWEEN '" + from + "' AND '" + to + "')";
            }else if(a != null && b == null){
                /* TH fromAge Exists AND toAge undefined */
                const from_ = currentDate.getFullYear() - a;
                const tuoiTu = new Date(`${from_}-01-01`);
                const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
                temp = "(ngaysinh BETWEEN '" + tT + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                /* TH fromAge undefined AND toAge Exists */
                const to_ = currentDate.getFullYear() - a;
                const tuoiDen = new Date(`${to_}-01-01`);
                const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + tD + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        function whereSearch(Object){
            let agesFrom = Object.dotuoitu;
            let comeOfAge = Object.dotuoiden;
            let search = "";
            for(const k in Object){
                if(Object[k]  != null && k != 'dotuoitu' && k != 'dotuoiden'){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }

            if(empty(search)){
                search = aboutAge(agesFrom, comeOfAge);
            }else{
                if(aboutAge(agesFrom, comeOfAge) == ""){
                    search += "";
                }else{
                    search += " AND " + aboutAge(agesFrom, comeOfAge);
                }
            }

            if(empty(search)){
                search = " 1 ";
            }


            return search;
        }

        const [rows0, fields0] = await HoanCanhDacBiet.isExists(hoancanh);
        const [rows3, fields3] = await HinhThucTroGiup.isExists(hoancanh);
        
        const workbook = new Excel__.Workbook();

        if(xa && huyen && thanhpho){
            if(thon){

                const [village, villageFields] = await Excel.findVillage(thon);
                const [childrens, childrensFields] = await Excel.getChildrenFollowVillageObjectKTDL(whereSearch(Object));

                let tenxa_, ten_quanhuyen_

                if(!tenxa){
                    tenxa_ = await Excel.getWardWhenExistsVillage(xa)
                }

                if(!tenhuyen){
                    ten_quanhuyen_ = await Excel.getDistrictWhenExistsVillage(huyen)
                }

                const check = rows0.length > 0 ? rows0[0].id_lhc : (rows3.length > 0 ? 168 : null);

                const worksheet = workbook.addWorksheet(village[0].tenthon);

                if(hoancanh==1){

                    const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();

                    HCDB.sort((a, b) => a>b?-1:1)

                    const dataFormHelpers = await onHandleObjectHCDB(req);

                    const columnsHCDB = HCDB.map((e, index) => ({
                        header: e.ten_hoancanh,
                        key: `hcdb_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))

                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].hoancanhdacbiet.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`hcdb_${n}`] = JSON.parse(dataFormHelpers[t].hoancanhdacbiet[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }

                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHCDB]

                    worksheet.columns = [...columns]

                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHỈ TIÊU"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);

                    worksheet.addRows(dataEnd);

                }else if(hoancanh==2){
                    const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();

                    NCHCDB.sort((a, b) => a>b?-1:1)

                    const dataFormHelpers = await onHandleObjectNCHCDB(req);

                    const columnsNCHCDB = NCHCDB.map((e, index) => ({
                        header: e.ten_hoancanh,
                        key: `nchcdb_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))

                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].nguycoHCDB.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`nchcdb_${n}`] = JSON.parse(dataFormHelpers[t].nguycoHCDB[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }

                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsNCHCDB]

                    worksheet.columns = [...columns]

                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${childrens.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);

                    worksheet.addRows(dataEnd);

                }else if(hoancanh==3){
                    const [HCK, HCKFields] = await Excel.getcolumnsHCK();

                    HCK.sort((a, b) => a>b?-1:1)

                    const dataFormHelpers = await onHandleObjectHCK(req);

                    const columnsHCK = HCK.map((e, index) => ({
                        header: e.ten_hoancanh,
                        key: `hck_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))

                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].hoancanhkhac.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`hck_${n}`] = JSON.parse(dataFormHelpers[t].hoancanhkhac[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }

                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHCK]

                    worksheet.columns = [...columns]

                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${childrens.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);

                    worksheet.addRows(dataEnd);

                }else if(hoancanh==4){
                    const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

                    HTTG.sort((a, b) => a>b?-1:1)

                    const dataFormHelpers = await onHandleObjectHTTG(req);

                    const columnsHTTG = HTTG.map((e, index) => ({
                        header: e.tenhinhthuc,
                        key: `httg_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))

                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].hinhthuctrogiup.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`httg_${n}`] = JSON.parse(dataFormHelpers[t].hinhthuctrogiup[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }

                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHTTG]

                    worksheet.columns = [...columns]

                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${childrens.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);

                    worksheet.addRows(dataEnd);

                }else{

                    async function funcSwitch(){
                        switch (check) {
                            case 165:
                                const [rowsHCDB, fieldsHCDB] = await QuanLyHoSoTreEm.getChildrenHCDB();
                                async function loop1(){
                                    let promises = [];
                                    let length = childrens.length;
                                    for(let i = 0; i<length; i++){
                                        const [rows1HCDB, fields1HCDB] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                        const idx1 = rows1HCDB.findIndex(({id_}) => id_===rows0[0].id_)
                                        if(idx1!==-1){
                                            let HCDB = getAllHoanCanh(rowsHCDB);
                                            eachRow1Object(HCDB,rows0)
                                            let slice_ = HCDB.filter(({ value }) => value===true)
                                            slice_.forEach((e,i) => {
                                                delete slice_[i].children
                                            })
                                            promises.push({
                                                ...childrens[i],
                                                doituong: [...slice_]
                                            })
                                        }
                                    }
    
                                    return Promise.all(promises);
                                }
    
                                return loop1().then(res => res).catch(err => console.log(err))
    
                                break;
                            case 166:
                                const [rowsNCHCDB, fieldsNCHCDB] = await QuanLyHoSoTreEm.getChildrenNCHCDB();
                                async function loop2(){
                                    let promises = [];
                                    let length = childrens.length;
                                    for(let i = 0; i<length; i++){
                                        const [rows1NCHCDB, fields1NCHCDB] = await QuanLyHoSoTreEm.getIdChildren(childrens.id_treem)
                                        const idx2 = rows1NCHCDB.findIndex(({id_}) => id_===rows0[0].id_)
                                        if(idx2!==-1){
                                            let NCRVHCDB = getAllHoanCanh(rowsNCHCDB);
                                            eachRow1Object(NCRVHCDB,rows0)
                                            let slice_ = NCRVHCDB.filter(({ value }) => value===true)
                                            slice_.forEach((e,i) => {
                                                delete slice_[i].children
                                            })
                                            promises.push({
                                                ...childrens[i],
                                                doituong: [...slice_]
                                            })
                                        }
                                    }
    
                                    return Promise.all(promises);
                                }
                                return loop2().then(res => res).catch(err => console.log(err))
    
                                break;       
                            case 167:
                                const [rowsHCK, fieldsHCK] = await QuanLyHoSoTreEm.getChildrenHCK();
                                async function loop3(){
                                    let promises = [];
                                    let length = childrens.length;
                                    for(let i = 0; i<length; i++){
                                        const [rows1HCK, fields1HCK] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                        const idx3 = rows1HCK.findIndex(({id_}) => id_===rows0[0].id_)
                                        if(idx3!==-1){
                                            let HCK = getAllHoanCanh(rowsHCK);
                                            eachRow1Object(HCK,rows0)
                                            let slice_ = HCK.filter(({ value }) => value===true)
                                            slice_.forEach((e,i) => {
                                                delete slice_[i].children
                                            })
                                            promises.push({
                                                ...childrens[i],
                                                doituong: [...slice_]
                                            })
                                        }
                                    }
    
                                    return Promise.all(promises);
                                }
                                return loop3().then(res => res).catch(err => console.log(err))
                                break;       
                            case 168:
                                const [rowsHTTG, fieldsHTTG] = await QuanLyHoSoTreEm.getAllHTTG();
                                async function loop(){
                                    let promises = [];
                                        let length = childrens.length;
                                        for(let i = 0; i<length; i++){
                                            let sum = 0;
                                            let result = [];
                                            let n = 0;
                                            const [rows1HTTG, fields1HTTG] = await QuanLyHoSoTreEm.getIdChildrenHTTG(childrens[i].id_treem)
                                            const idx4 = rows1HTTG.findIndex(({id_hinhthuc}) => id_hinhthuc===rows3[0].id_hinhthuc)
                                            
                                            if(idx4!==-1){
                                                let data = dequi(rowsHTTG,sum,result,n);
                                                let HinhThucTroGiup = detailHTTG(data);
                                                eachRowObject(HinhThucTroGiup,rows3)
                                                let slice_ = HinhThucTroGiup.filter(({ value }) => value===true)
                                                slice_.forEach((e,i) => {
                                                    delete slice_[i].children
                                                })

                                                promises.push({
                                                    ...childrens[i],
                                                    doituong: [...slice_]
                                                })
                                            }
                                        }
                                    return Promise.all(promises)
                                }
                                return loop().then(res => res).catch(err => console.log({err}))
                                break;       
                            default:
                                break;
                        }
                    }
    
                    let dataChildrens = await funcSwitch();
                    
                    let columnsChiTieu = [];

                    if(dataChildrens.length > 0){
                        columnsChiTieu = dataChildrens[0].doituong.map((e, index) => ({
                            header: (check==168) ? e.tenhinhthuc : e.ten_hoancanh,
                            key: `hoancanh_${index}`,   
                            width: 17, 
                            style: { alignment: { vertical:"middle", horizontal:"center" }  }
                        }))
                    }
    
                    let dataEnd = [];
                    for(t = 0; t<dataChildrens.length; t++){
                        let hc = {};
                        let length__ = dataChildrens[t].doituong.length;
                        for(n = 0; n<length__; n++){
                            hc[`hoancanh_${n}`] = dataChildrens[t].doituong[n].value == true ? 1 : 0
                        }
    
                        dataEnd.push({
                            ...dataChildrens[t],
                            ...hc
                        })
                    }
    
                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsChiTieu]
                    
                    worksheet.columns = [...columns]
    
                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${village[0].tenthon} - ${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHỈ TIÊU"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);
                    
                    worksheet.addRows(dataEnd);
                }
                
            }else{
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(xa);
                let childrens = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [data, dataFields] = await Excel.getChildrenFollowVillageObjectKTDL(whereSearch(ObjectFollowWard));
                    childrens = [...childrens, ...data];
                }

                let tenxa_, ten_quanhuyen_
                if(!tenxa){
                    tenxa_ = await Excel.getWardWhenExistsVillage(xa)
                }

                if(!tenhuyen){
                    ten_quanhuyen_ = await Excel.getDistrictWhenExistsVillage(huyen)
                }

                const check = rows0.length > 0 ? rows0[0].id_lhc : (rows3.length > 0 ? 168 : null);

                const worksheet = workbook.addWorksheet(tenxa ? tenxa : tenxa_);

            if(hoancanh==1){

                const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();

                HCDB.sort((a, b) => a>b?-1:1)

                const dataFormHelpers = await onHandleObjectHCDB(req);
                console.log(dataFormHelpers[0].hoancanhdacbiet)

                const columnsHCDB = HCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhdacbiet.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hcdb_${n}`] = JSON.parse(dataFormHelpers[t].hoancanhdacbiet[n].value) === true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHCDB]

                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHỈ TIÊU"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);                

                worksheet.addRows(dataEnd);

            }else if(hoancanh==2){
                const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();

                NCHCDB.sort((a, b) => a>b?-1:1)

                const dataFormHelpers = await onHandleObjectNCHCDB(req);

                const columnsNCHCDB = NCHCDB.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `nchcdb_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].nguycoHCDB.length;
                            for(let n = 0; n<length__; n++){
                                hc[`nchcdb_${n}`] = JSON.parse(dataFormHelpers[t].nguycoHCDB[n].value) === true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsNCHCDB]

                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHI TIÊU"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);                                

                worksheet.addRows(dataEnd);

            }else if(hoancanh==3){
                const [HCK, HCKFields] = await Excel.getcolumnsHCK();

                HCK.sort((a, b) => a>b?-1:1)

                const dataFormHelpers = await onHandleObjectHCK(req);

                const columnsHCK = HCK.map((e, index) => ({
                    header: e.ten_hoancanh,
                    key: `hck_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hoancanhkhac.length;
                            for(let n = 0; n<length__; n++){
                                hc[`hck_${n}`] = JSON.parse(dataFormHelpers[t].hoancanhkhac[n].value) === true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHCK]

                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHI TIÊU"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);                                

                worksheet.addRows(dataEnd);

            }else if(hoancanh==4){
                const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();

                HTTG.sort((a, b) => a>b?-1:1)

                const dataFormHelpers = await onHandleObjectHTTG(req);

                const columnsHTTG = HTTG.map((e, index) => ({
                    header: e.tenhinhthuc,
                    key: `httg_${index}`,   
                    width: 17, 
                    style: { alignment: { vertical:"middle", horizontal:"center" }  }
                }))

                let dataEnd = [];
                for(t = 0; t<dataFormHelpers.length; t++){
                    for(l = 0; l<childrens.length; l++){
                        if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                            let hc = {};
                            let length__ = dataFormHelpers[t].hinhthuctrogiup.length;
                            for(let n = 0; n<length__; n++){
                                hc[`httg_${n}`] = JSON.parse(dataFormHelpers[t].hinhthuctrogiup[n].value) === true ? 1 : 0
                            }
                            dataEnd.push({
                                ...childrens[l],
                                ...hc
                            })
                        }
                    }
                }

                const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHTTG]

                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa ? tenxa : tenxa_} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHI TIÊU"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);                                

                worksheet.addRows(dataEnd);

            }else{

                async function funcSwitch(){
                    switch (check) {
                        case 165:
                            const [rowsHCDB, fieldsHCDB] = await QuanLyHoSoTreEm.getChildrenHCDB();
                            async function loop1(){
                                let promises = [];
                                let length = childrens.length;
                                for(let i = 0; i<length; i++){
                                    const [rows1HCDB, fields1HCDB] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                    const idx1 = rows1HCDB.findIndex(({id_}) => id_===rows0[0].id_)
                                    if(idx1!==-1){
                                        let HCDB = getAllHoanCanh(rowsHCDB);
                                        eachRow1Object(HCDB,rows0)
                                        let slice_ = HCDB.filter(({ value }) => value===true)
                                        slice_.forEach((e,i) => {
                                            delete slice_[i].children
                                        })
                                        promises.push({
                                            ...childrens[i],
                                            doituong: [...slice_]
                                        })
                                    }
                                }

                                return Promise.all(promises);
                            }

                            return loop1().then(res => res).catch(err => console.log(err))

                            break;
                        case 166:
                            const [rowsNCHCDB, fieldsNCHCDB] = await QuanLyHoSoTreEm.getChildrenNCHCDB();
                            async function loop2(){
                                let promises = [];
                                let length = childrens.length;
                                for(let i = 0; i<length; i++){
                                    const [rows1NCHCDB, fields1NCHCDB] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                    const idx2 = rows1NCHCDB.findIndex(({id_}) => id_===rows0[0].id_)
                                    if(idx2!==-1){
                                        let NCRVHCDB = getAllHoanCanh(rowsNCHCDB);
                                        eachRow1Object(NCRVHCDB,rows0)
                                        let slice_ = NCRVHCDB.filter(({ value }) => value===true)
                                        slice_.forEach((e,i) => {
                                            delete slice_[i].children
                                        })
                                        promises.push({
                                            ...childrens[i],
                                            doituong: [...slice_]
                                        })
                                    }
                                }

                                return Promise.all(promises);
                            }
                            return loop2().then(res => res).catch(err => console.log(err))

                            break;       
                        case 167:
                            const [rowsHCK, fieldsHCK] = await QuanLyHoSoTreEm.getChildrenHCK();
                            async function loop3(){
                                let promises = [];
                                let length = childrens.length;
                                for(let i = 0; i<length; i++){
                                    const [rows1HCK, fields1HCK] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                    const idx3 = rows1HCK.findIndex(({id_}) => id_===rows0[0].id_)
                                    if(idx3!==-1){
                                        let HCK = getAllHoanCanh(rowsHCK);
                                        eachRow1Object(HCK,rows0)
                                        let slice_ = HCK.filter(({ value }) => value===true)
                                        slice_.forEach((e,i) => {
                                            delete slice_[i].children
                                        })
                                        promises.push({
                                            ...childrens[i],
                                            doituong: [...slice_]
                                        })
                                    }
                                }

                                return Promise.all(promises);
                            }
                            return loop3().then(res => res).catch(err => console.log(err))
                            break;       
                        case 168:
                            const [rowsHTTG, fieldsHTTG] = await QuanLyHoSoTreEm.getAllHTTG();
                            async function loop(){
                                let promises = [];
                                    let length = childrens.length;
                                    for(let i = 0; i<length; i++){
                                        let sum = 0;
                                        let result = [];
                                        let n = 0;
                                        const [rows1HTTG, fields1HTTG] = await QuanLyHoSoTreEm.getIdChildrenHTTG(childrens[i].id_treem)
                                        const idx4 = rows1HTTG.findIndex(({id_hinhthuc}) => id_hinhthuc===rows3[0].id_hinhthuc)
                                        
                                        if(idx4!==-1){
                                            let data = dequi(rowsHTTG,sum,result,n);
                                            let HinhThucTroGiup = detailHTTG(data);
                                            eachRowObject(HinhThucTroGiup,rows3)
                                            let slice_ = HinhThucTroGiup.filter(({ value }) => value===true)
                                            slice_.forEach((e,i) => {
                                                delete slice_[i].children
                                            })

                                            promises.push({
                                                ...childrens[i],
                                                doituong: [...slice_]
                                            })
                                        }
                                    }
                                return Promise.all(promises)
                            }
                            return loop().then(res => res).catch(err => console.log({err}))
                            break;       
                        default:
                            break;
                    }
                }

                let dataChildrens = await funcSwitch();
                
                let columnsChiTieu = [];
console.log("HAI SANG LA TUI : ", dataChildrens.length);
                if(dataChildrens.length > 0){
                    columnsChiTieu = dataChildrens[0].doituong.map((e, index) => ({
                        header: (check==168) ? e.tenhinhthuc : e.ten_hoancanh,
                        key: `hoancanh_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))
                }

                let dataEnd = [];
                for(t = 0; t<dataChildrens.length; t++){
                    let hc = {};
                    let length__ = dataChildrens[t].doituong.length;
                    for(n = 0; n<length__; n++){
                        hc[`hoancanh_${n}`] = dataChildrens[t].doituong[n].value == true ? 1 : 0
                    }

                    dataEnd.push({
                        ...dataChildrens[t],
                        ...hc
                    })
                }

                const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsChiTieu]
                
                worksheet.columns = [...columns]

                worksheet.duplicateRow(1,3,true);

                worksheet.getRow(1).values = [`${tenxa ? tenxa : tenxa_} - ${tenhuyen} - ${tenthanhpho}`]
                worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(1,1,1,columns.length);

                worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM"]
                worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                worksheet.mergeCells(2,1,2,columns.length);

                worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                worksheet.mergeCells(3,1,3,columns.length);
                
                worksheet.addRows(dataEnd);
            }
            }
        }else if(thanhpho && huyen && !xa){
            const [district, districtFields] = await Excel.getListVillageFollowIDDistrict(huyen);

            let ten_quanhuyen_
            if(!tenhuyen){
                ten_quanhuyen_ = await Excel.getDistrictWhenExistsVillage(huyen);
            }

            const check = rows0.length > 0 ? rows0[0].id_lhc : (rows3.length > 0 ? 168 : null);

            for(let j = 0; j<district.length; j++){
                const [ward, wardFields] = await Excel.getListVillageFollowIDWard(district[j].id_phuongxa);
                let childrens = [];
                for(let i = 0; i<ward.length; i++){
                    let ObjectFollowWard = {
                        ...Object,
                        id_thon: ward[i].id_thon
                    }
                    const [data, dataFields] = await Excel.getChildrenFollowVillageObjectKTDL(whereSearch(ObjectFollowWard));
                    childrens = [...childrens, ...data];
                }
                const worksheet = workbook.addWorksheet(district[j].ten_phuongxa);

                if(hoancanh==1){

                    const [HCDB, HCDBFields] = await Excel.getcolumnsHCDB();
    
                    HCDB.sort((a, b) => a>b?-1:1)
    
                    const dataFormHelpers = await onHandleObjectHCDB(req);
    
                    const columnsHCDB = HCDB.map((e, index) => ({
                        header: e.ten_hoancanh,
                        key: `hcdb_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))

                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].hoancanhdacbiet.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`hcdb_${n}`] = JSON.parse(dataFormHelpers[t].hoancanhdacbiet[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }
    
                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHCDB]
    
                    worksheet.columns = [...columns]
    
                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHI TIÊU"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);
    
                    worksheet.addRows(dataEnd);
    
                }else if(hoancanh==2){
                    const [NCHCDB, NCHCDBFields] = await Excel.getcolumnsNCHCDB();
    
                    NCHCDB.sort((a, b) => a>b?-1:1)
    
                    const dataFormHelpers = await onHandleObjectNCHCDB(req);
    
                    const columnsNCHCDB = NCHCDB.map((e, index) => ({
                        header: e.ten_hoancanh,
                        key: `nchcdb_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))
    
                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].nguycoHCDB.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`nchcdb_${n}`] = JSON.parse(dataFormHelpers[t].nguycoHCDB[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }
    
                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsNCHCDB]
    
                    worksheet.columns = [...columns]
    
                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHI TIÊU"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);                                
    
                    worksheet.addRows(dataEnd);
    
                }else if(hoancanh==3){
                    const [HCK, HCKFields] = await Excel.getcolumnsHCK();
    
                    HCK.sort((a, b) => a>b?-1:1)
    
                    const dataFormHelpers = await onHandleObjectHCK(req);
    
                    const columnsHCK = HCK.map((e, index) => ({
                        header: e.ten_hoancanh,
                        key: `hck_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))

                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].hoancanhkhac.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`hck_${n}`] = JSON.parse(dataFormHelpers[t].hoancanhkhac[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }
    
                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHCK]
    
                    worksheet.columns = [...columns]
    
                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHỈ TIÊU"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);                                        
    
                    worksheet.addRows(dataEnd);
    
                }else if(hoancanh==4){
                    const [HTTG, HTTGFields] = await Excel.getcolumnsHTTG();
    
                    HTTG.sort((a, b) => a>b?-1:1)
    
                    const dataFormHelpers = await onHandleObjectHTTG(req);
    
                    const columnsHTTG = HTTG.map((e, index) => ({
                        header: e.tenhinhthuc,
                        key: `httg_${index}`,   
                        width: 17, 
                        style: { alignment: { vertical:"middle", horizontal:"center" }  }
                    }))

                    let dataEnd = [];
                    for(t = 0; t<dataFormHelpers.length; t++){
                        for(l = 0; l<childrens.length; l++){
                            if(childrens[l].id_treem == dataFormHelpers[t].id_treem){
                                let hc = {};
                                let length__ = dataFormHelpers[t].hinhthuctrogiup.length;
                                for(let n = 0; n<length__; n++){
                                    hc[`httg_${n}`] = JSON.parse(dataFormHelpers[t].hinhthuctrogiup[n].value) === true ? 1 : 0
                                }
                                dataEnd.push({
                                    ...childrens[l],
                                    ...hc
                                })
                            }
                        }
                    }
    
                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsHTTG]
    
                    worksheet.columns = [...columns]
    
                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHỈ TIÊU"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);                    
    
                    worksheet.addRows(dataEnd);
    
                }else{

                    async function funcSwitch(){
                        switch (check) {
                            case 165:
                                const [rowsHCDB, fieldsHCDB] = await QuanLyHoSoTreEm.getChildrenHCDB();
                                async function loop1(){
                                    let promises = [];
                                    let length = childrens.length;
                                    for(let i = 0; i<length; i++){
                                        const [rows1HCDB, fields1HCDB] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                        const idx1 = rows1HCDB.findIndex(({id_}) => id_===rows0[0].id_)
                                        if(idx1!==-1){
                                            let HCDB = getAllHoanCanh(rowsHCDB);
                                            eachRow1Object(HCDB,rows0)
                                            let slice_ = HCDB.filter(({ value }) => value===true)
                                            slice_.forEach((e,i) => {
                                                delete slice_[i].children
                                            })
                                            promises.push({
                                                ...childrens[i],
                                                doituong: [...slice_]
                                            })
                                        }
                                    }
  
                                    return Promise.all(promises);
                                }
    
                                return loop1().then(res => res).catch(err => console.log(err))
    
                                break;
                            case 166:
                                const [rowsNCHCDB, fieldsNCHCDB] = await QuanLyHoSoTreEm.getChildrenNCHCDB();
                                async function loop2(){
                                    let promises = [];
                                    let length = childrens.length;
                                    for(let i = 0; i<length; i++){
                                        const [rows1NCHCDB, fields1NCHCDB] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                        const idx2 = rows1NCHCDB.findIndex(({id_}) => id_===rows0[0].id_)
                                        if(idx2!==-1){
                                            let NCRVHCDB = getAllHoanCanh(rowsNCHCDB);
                                            eachRow1Object(NCRVHCDB,rows0)
                                            let slice_ = NCRVHCDB.filter(({ value }) => value===true)
                                            slice_.forEach((e,i) => {
                                                delete slice_[i].children
                                            })
                                            promises.push({
                                                ...childrens[i],
                                                doituong: [...slice_]
                                            })
                                        }
                                    }
    
                                    return Promise.all(promises);
                                }
                                return loop2().then(res => res).catch(err => console.log(err))
    
                                break;       
                            case 167:
                                const [rowsHCK, fieldsHCK] = await QuanLyHoSoTreEm.getChildrenHCK();
                                async function loop3(){
                                    let promises = [];
                                    let length = childrens.length;
                                    for(let i = 0; i<length; i++){
                                        const [rows1HCK, fields1HCK] = await QuanLyHoSoTreEm.getIdChildren(childrens[i].id_treem)
                                        const idx3 = rows1HCK.findIndex(({id_}) => id_===rows0[0].id_)
                                        if(idx3!==-1){
                                            let HCK = getAllHoanCanh(rowsHCK);
                                            eachRow1Object(HCK,rows0)
                                            let slice_ = HCK.filter(({ value }) => value===true)
                                            slice_.forEach((e,i) => {
                                                delete slice_[i].children
                                            })
                                            promises.push({
                                                ...childrens[i],
                                                doituong: [...slice_]
                                            })
                                        }
                                    }
    
                                    return Promise.all(promises);
                                }
                                return loop3().then(res => res).catch(err => console.log(err))
                                break;       
                            case 168:
                                const [rowsHTTG, fieldsHTTG] = await QuanLyHoSoTreEm.getAllHTTG();
                                async function loop(){
                                    let promises = [];
                                        let length = childrens.length;
                                        for(let i = 0; i<length; i++){
                                            let sum = 0;
                                            let result = [];
                                            let n = 0;
                                            const [rows1HTTG, fields1HTTG] = await QuanLyHoSoTreEm.getIdChildrenHTTG(childrens[i].id_treem)
                                            const idx4 = rows1HTTG.findIndex(({id_hinhthuc}) => id_hinhthuc===rows3[0].id_hinhthuc)
                                            
                                            if(idx4!==-1){
                                                let data = dequi(rowsHTTG,sum,result,n);
                                                let HinhThucTroGiup = detailHTTG(data);
                                                eachRowObject(HinhThucTroGiup,rows3)
                                                let slice_ = HinhThucTroGiup.filter(({ value }) => value===true)
                                                slice_.forEach((e,i) => {
                                                    delete slice_[i].children
                                                })
    
                                                promises.push({
                                                    ...childrens[i],
                                                    doituong: [...slice_]
                                                })
                                            }
                                        }
                                    return Promise.all(promises)
                                }
                                return loop().then(res => res).catch(err => console.log({err}))
                                break;       
                            default:
                                break;
                        }
                    }
    
                    let dataChildrens = await funcSwitch();

                    let columnsChiTieu = [];

                    if(dataChildrens.length > 0){
                        console.log(dataChildrens[0].doituong[0])
                        columnsChiTieu = dataChildrens[0].doituong.map((e, index) => ({
                            header: (check==168) ? e.tenhinhthuc : e.ten_hoancanh,
                            key: `hoancanh_${index}`,   
                            width: 17, 
                            style: { alignment: { vertical:"middle", horizontal:"center" }  }
                        }))
                    }
    
                    let dataEnd = [];
                    for(t = 0; t<dataChildrens.length; t++){
                        let hc = {};
                        let length__ = dataChildrens[t].doituong.length;
                        for(n = 0; n<length__; n++){
                            hc[`hoancanh_${n}`] = dataChildrens[t].doituong[n].value == true ? 1 : 0
                        }
    
                        dataEnd.push({
                            ...dataChildrens[t],
                            ...hc
                        })
                    }
    
                    const columns = [...columnsDownloadChildrenFollowObjectKTDL, ...columnsChiTieu]
                    
                    worksheet.columns = [...columns]
    
                    worksheet.duplicateRow(1,3,true);
    
                    worksheet.getRow(1).values = [`${district[j].ten_phuongxa} - ${tenhuyen ? tenhuyen : ten_quanhuyen_} - ${tenthanhpho}`]
                    worksheet.getRow(1).font = { bold: true, name:"Times New Roman", size:9}
                    worksheet.getRow(1).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(1,1,1,columns.length);
    
                    worksheet.getRow(2).values = ["DANH SÁCH TRẺ EM THEO CHỈ TIÊU"]
                    worksheet.getRow(2).alignment = { horizontal:"center", vertical:"middle" }
                    worksheet.getRow(2).font = { bold: true, name:"Times New Roman", size:10 }
                    worksheet.mergeCells(2,1,2,columns.length);
    
                    worksheet.getRow(3).values = [`Tổng số trẻ em: ${dataEnd.length}`]
                    worksheet.getRow(3).font = { bold: true, name:"Times New Roman", size:9 }
                    worksheet.getRow(3).alignment = { horizontal:"left", vertical:"middle" }
                    worksheet.mergeCells(3,1,3,columns.length);
                    
                    worksheet.addRows(dataEnd);
                }
            }
        }

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "DanhSachTreEmTheoChiTieu.xls");

        return workbook
        .xlsx
        .write(res)
        .then(() => {
            console.log("saved");
            res.status(200).end();
        })
        .catch((err) => {
            console.log("err", err);
        });

    } catch (error) {
        next(error);
    }
}