const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
const empty = require('is-empty');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const saltRounds = 10;


module.exports.getAccountController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            Account,
        } = new Model({ sqlConn });

        const data = {
            id_tinhthanhpho: req.params.id_tinhthanhpho,
            id_quanhuyen: req.query.huyen ? req.query.huyen : "NULL",
            id_phuongxa: req.query.xa ? req.query.xa : "NULL",
            tentaikhoan: req.query.tentaikhoan ? req.query.tentaikhoan : "NULL",
            tenhienthi: req.query.tenhienthi ? req.query.tenhienthi : "NULL",
        }

        var getData = "";
        if(data.id_phuongxa == "NULL"){
            if(data.id_quanhuyen == "NULL"){
                getData = "dv.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "dv.id_quanhuyen IS NULL" + " AND " + "dv.id_phuongxa IS NULL"
            }else if(data.id_quanhuyen == "NOT NULL"){
                getData = "dv.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "dv.id_quanhuyen IS NOT NULL" + " AND " + "dv.id_phuongxa IS NULL"
            }else{
                getData = "dv.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "dv.id_quanhuyen  = " + data.id_quanhuyen + " AND " + "dv.id_phuongxa IS NULL"
            }
        }else if("NOT NULL"){
            getData = "dv.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "dv.id_quanhuyen = " + data.id_quanhuyen + " AND " + "dv.id_phuongxa IS NOT NULL "
        }else{
            getData = "dv.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "dv.id_quanhuyen = " + data.id_quanhuyen + " AND " + "dv.id_phuongxa = " + data.id_phuongxa
        }

        var getData2 = "";
        if(data.tentaikhoan != "NULL" && data.tenhienthi != "NULL"){
            getData2 = "tk.tenhienthi LIKE '%" + data.tenhienthi + "%'" + " AND " + "tk.tentaikhoan LIKE '%" + data.tentaikhoan + "%'";
        }else if(data.tentaikhoan == "NULL" && data.tenhienthi != "NULL"){
            getData2 = "tk.tenhienthi LIKE '%" + data.tenhienthi + "%'";
        }else if(data.tentaikhoan != "NULL" && data.tenhienthi == "NULL"){
            getData2 = "tk.tentaikhoan LIKE '%" + data.tentaikhoan + "%'";
        }else{
            getData2 = "";
        }
        
        var getData3 = "";
        if(getData2 === ""){
            getData3 = getData;
        }else{
            getData3 = getData + " AND " + getData2;
        }

        console.log(getData3);

        const [rows, fields] = await Account.getListAccount(getData3);

        let page = req.query.page;
        var pagesize = req.query.pagesize; //-------------------------------------------------

        if(empty(page)){
            page = 1;
        }
        
        if(empty(pagesize)){
            pagesize = 20;
        }

        var start = (page - 1) * pagesize;
        var end = pagesize*page - 1;
        var pagination = [];
        var total = rows.length;
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(rows[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(rows[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pagesize),
                pageNumber:  Number(page),
                total: total,
                search: data,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}


module.exports.updateAccountController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            Account,
        } = new Model({ sqlConn });

        

        const data = {
            id_tinhthanhpho: req.params.id_tinhthanhpho,
            id_quanhuyen: req.body.huyen ? req.body.huyen : "NULL",
            id_phuongxa: req.body.xa ? req.body.xa : "NULL",
            id_taikhoan: req.params.id,
            tentaikhoan: req.body.tentaikhoan,
            tenhienthi: req.body.tenhienthi,
            email: req.body.email,
            dakhoa: req.body.dakhoa,
        }


        var getData = "";
        if(data.id_phuongxa == "NULL"){
            if(data.id_quanhuyen == "NULL"){
                getData = "donvi.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "donvi.id_quanhuyen IS NULL" + " AND " + "donvi.id_phuongxa IS NULL"
            }else{
                getData = "donvi.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "donvi.id_quanhuyen  = " + data.id_quanhuyen + " AND " + "donvi.id_phuongxa IS NULL"
            }
        }else{
            getData = "donvi.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "donvi.id_quanhuyen = " + data.id_quanhuyen + " AND " + "donvi.id_phuongxa  = " + data.id_phuongxa
        }
        

        const [rows, fields] = await Account.updateAccount(getData, data);


        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Update successful',
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.updatePasswordAccountController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            Account,
        } = new Model({ sqlConn });

        

        const data = {
            id_taikhoan: req.params.id,
            newPassword: req.body.newPassword,
            confirmPassword: req.body.confirmPassword
        }

        let pass = data.newPassword;

        function validateChangePassword(data){ 
            const JoiSchema = Joi.object({ 
                newPassword: Joi.string().required(),
                confirmPassword: Joi.any().valid(Joi.ref('newPassword')).required()
            }).options({ abortEarly: false }); 
            return JoiSchema.validate(data) 
        }

        const validation = validateChangePassword({
            newPassword: data.newPassword,
            confirmPassword: data.confirmPassword
        });
        if(validation.error){
            return res.status(httpStatus.BAD_REQUEST)
            .json({
                code: httpStatus.BAD_REQUEST,
                message: 'Error Validation',
                // totalError: validation.error.details.length,
                result: validation.error.details,
            })
            .end();
        }

        bcrypt.hash(pass, saltRounds, async function (err, hash){
            data.newPassword = hash;
            const [rows, fields] = await Account.updatePasswordAccount(data);

            return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Đổi mật khẩu thành công',
                result: rows,
            })
            .end();
        })


    } catch (error) {
        next(error);
    }
}