const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
const { generator,
        Verify,
        renewAccessToken
} = require('../helpers/HandleToken.helper');
const jwt = require('jsonwebtoken');
const { response } = require('express');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const saltRounds = 10;


module.exports.LoginController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            User
        } = new Model({sqlConn})

        const {
            username,
            password
        } = req.body

        const [usernameRows, usernameFields] = await User.CheckUsername(username) 

        if(usernameRows.length <= 0){
            return res.status(401).json({
                code : 401,
                message : 'User not exsist'
            })
        }

        const [isLock, isLockFields] = await User.isLockUser(username)
        if(isLock.length <= 0){
            return res.status(401).json({
                code : 401,
                message : 'The account is locked'
            })
        }

        bcrypt.compare(password, usernameRows[0].matkhau, async function(err, result) {
            if(!result){
                return res.status(401).json({
                    code : 401,
                    message : 'Password Invalid'
                })
            }

            const [{id_tinh, thanhpho}, {id_quan, quanhuyen}, {id_xa, phuongxa}] = await User.GetLocationUser(usernameRows[0]["id_taikhoan"]);
            // const payload = {username:authRows[0]['tentaikhoan']}
            const payload = {username:usernameRows[0]["tentaikhoan"]}
            // Convert Expired Access Token from String to number
            const Access_Token_Life = Number(process.env.ACCESS_TOKEN_LIFETIME)
            const [accessToken] = await generator(payload, process.env.ACCESS_TOKEN,process.env.REFRESH_TOKEN,Access_Token_Life,process.env.REFRESH_TOKEN_LIFETIME);

            // await User.CreateAndUpdateRefreshToken(payload.tentaikhoan, refreshToken);

            return res.status(httpStatus.OK)
                .json({
                    code: httpStatus.OK,
                    message: 'Login Success',
                    username : usernameRows[0]["tenhienthi"],
                    location : {
                        id_tinh,
                        thanhpho,
                        id_quan,
                        quanhuyen,
                        id_xa,
                        phuongxa
                    },
                    token: accessToken,
                })
                .end();
        });

    } catch (error) {
        next(error);
    }
}

// Present func not use
module.exports.RefreshTokenController = async (req, res, next) => {

    try {
        const {
            sqlConn
        } = req.app.locals
    
        const {
            User
        } = new Model({sqlConn})
    
        const {
            refreshToken
        } = req.body

        const [rows, fields] = await User.CheckToken(refreshToken)
    
        if(refreshToken && rows.length > 0){
            const decoded = await Verify(refreshToken,process.env.REFRESH_TOKEN)

            const data = {
                tentaikhoan:decoded.tentaikhoan,
                tenhienthi:decoded.tenhienthi,
                email:decoded.email,
                id_phongban:decoded.id_phongban
            }

            // Convert Expired Access Token from String to number
            const Access_Token_Life = Number(process.env.ACCESS_TOKEN_LIFETIME)

            const Renew = await renewAccessToken(data,process.env.ACCESS_TOKEN,Access_Token_Life)

            localStorage.setItem('Access-Token',Renew)

            return res.status(httpStatus.OK)
                      .json({
                          code : httpStatus.OK,
                          message: 'Refresh access token success'
                      })
        }
    } catch (error) {
        next(error)
    }
}

module.exports.LogoutController = async (req, res, next) => {

    try {
        const {
            sqlConn
        } = req.app.locals
    
        const {
            User
        } = new Model({sqlConn})
    
        const {
            accessToken
        } = req.body

        if(accessToken){
            const decoded = jwt.verify(accessToken,process.env.ACCESS_TOKEN)
            const [rows, fields] = await User.AddAccessTokenIntoBlacklist(accessToken,decoded.exp)
        }else{
            return res.json({
                message : 'Not exsist Token'
            })
        }

        

        return res.status(httpStatus.OK)
                    .json({
                        code : httpStatus.OK,
                        message: 'Logout success'
                    })
    } catch (error) {
        next(error)
    }
}

module.exports.RegisterController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            User,
        } = new Model({ sqlConn });

        const Joi = require('joi');

        const data = {
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            name: req.body.name,
            id_phongban: req.body.id_phongban
        }

        

        function validateUser(abc){ 
            const JoiSchema = Joi.object({ 
            username: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().min(7).alphanum().required(),
            name: Joi.string().required(),
            id_phongban: Joi.number().required()
            }).options({ abortEarly: false }); 
            return JoiSchema.validate(abc) 
        }     

        const response = validateUser(data) 
  
        if(response.error) {
            return res.status(httpStatus.BAD_REQUEST)
            .json({
                code: httpStatus.BAD_REQUEST,
                message: 'Error Validation',
                result: response.error.details,
            })
            .end();
        }else{
            
            var [rows, fields] = await User.registerUser(data);
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.ChangePasswordController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            User,
        } = new Model({ sqlConn });

        const {
            newPassword,
            oldPassword,
            confirmPassword
        } = req.body

        const {
            id_taikhoan,
            tentaikhoan,
            tenhienthi,
        } = req.decoded

        const {
            token_
        } = req

        /* 
            TODO Validaiton...
        */
        function validateChangePassword(data){ 
            const JoiSchema = Joi.object({ 
                newPassword: Joi.string().required(),
                oldPassword: Joi.string().required(),
                confirmPassword: Joi.any().valid(Joi.ref('newPassword')).required()
            }).options({ abortEarly: false }); 
            return JoiSchema.validate(data) 
        }

        const validation = validateChangePassword({
            newPassword: newPassword,
            oldPassword: oldPassword,
            confirmPassword: confirmPassword
        });
        if(validation.error){
            return res.status(httpStatus.BAD_REQUEST)
            .json({
                code: httpStatus.BAD_REQUEST,
                message: 'Error Validation',
                // totalError: validation.error.details.length,
                result: validation.error.details,
            })
            .end();
        }

        const [rowsCheckPassword, fieldsCheckPassword] = await User.CheckUsername(tentaikhoan);

        bcrypt.compare(oldPassword, rowsCheckPassword[0].matkhau, async function(err, result) {
            if(!result){
                return res.status(401).json({
                    code : 401,
                    message : "Mật khẩu cũ không chính xác",
                })
                .end()
            }

            if(token_){
                const decoded__ = jwt.verify(token_,process.env.ACCESS_TOKEN)
                const [rows, fields] = await User.AddAccessTokenIntoBlacklist(token_,decoded__.exp)
            }

            bcrypt.hash(newPassword, saltRounds, async function (err, hash){
                const [rows, fields] = await User.changePassword(id_taikhoan, hash);

                const [{id_tinh, thanhpho}, {id_quan, quanhuyen}, {id_xa, phuongxa}] = await User.GetLocationUser(id_taikhoan);
                const payload = {username:tentaikhoan}
                // Convert Expired Access Token from String to number
                const Access_Token_Life = Number(process.env.ACCESS_TOKEN_LIFETIME)
                const [accessToken] = await generator(payload, process.env.ACCESS_TOKEN,process.env.REFRESH_TOKEN,Access_Token_Life,process.env.REFRESH_TOKEN_LIFETIME);

                return res.status(httpStatus.OK)
                .json({
                    code: httpStatus.OK,
                    message: 'Change password success',
                    username : tenhienthi,
                    location : {
                        id_tinh,
                        thanhpho,
                        id_quan,
                        quanhuyen,
                        id_xa,
                        phuongxa
                    },
                    token: accessToken,
                })
                .end();
            })
        })

        // if(rowsCheckPassword.length <= 0){
        //     return res.status(401).json({
        //         code : 401,
        //         message : "Mật khẩu cũ không chính xác",
        //     })
        //     .end()
        // }

        // if(token_){
        //     const decoded__ = jwt.verify(token_,process.env.ACCESS_TOKEN)
        //     const [rows, fields] = await User.AddAccessTokenIntoBlacklist(token_,decoded__.exp)
        // }

        // const [rows, fields] = await User.changePassword(id_taikhoan, newPassword)

        // const [{id_tinh, thanhpho}, {id_quan, quanhuyen}, {id_xa, phuongxa}] = await User.GetLocationUser(id_taikhoan);
        // const payload = {username:tentaikhoan}
        // // Convert Expired Access Token from String to number
        // const Access_Token_Life = Number(process.env.ACCESS_TOKEN_LIFETIME)
        // const [accessToken] = await generator(payload, process.env.ACCESS_TOKEN,process.env.REFRESH_TOKEN,Access_Token_Life,process.env.REFRESH_TOKEN_LIFETIME);

        // return res.status(httpStatus.OK)
        // .json({
        //     code: httpStatus.OK,
        //     message: 'Change password success',
        //     username : tenhienthi,
        //     location : {
        //         id_tinh,
        //         thanhpho,
        //         id_quan,
        //         quanhuyen,
        //         id_xa,
        //         phuongxa
        //     },
        //     token: accessToken,
        // })
        // .end();

        // return res.status(httpStatus.OK)
        // .json({
        //     code: httpStatus.OK,
        //     message: 'Đổi mật khẩu thành công',
        //     result: rows,
        // })
        // .end();
    } catch (error) {
        next(error);
    }
}