const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');


module.exports.getThonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyThon,
        } = new Model({ sqlConn });

        let id = req.params.id_phuongxa

        const [rows, fields] = await QuanLyThon.getThon(id);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                total: rows.length,
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}


module.exports.addThonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyThon,
        } = new Model({ sqlConn });

        const data = {
            tenthon:req.body.tenthon,
            id_phuongxa:req.params.id_phuongxa
        }

        const [rows, fields] = await QuanLyThon.addThon(data);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Thêm thôn thành công',
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}


module.exports.deleteThonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyThon,
        } = new Model({ sqlConn });

        const data = {
            id:req.params.id,
            id_phuongxa:req.params.id_phuongxa
        }

        const [rows, fields] = await QuanLyThon.deleteThon(data);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Xóa thôn thành công',
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.updateThonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyThon,
        } = new Model({ sqlConn });

        const data = {
            tenthon:req.body.tenthon,
            id:req.params.id,
            id_phuongxa:req.params.id_phuongxa
        }

        const [rows, fields] = await QuanLyThon.updateThon(data);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Cập nhật Thôn Thành Công',
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.mergeThonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyThon,
        } = new Model({ sqlConn });

        const data = {
            dsthon:req.body.dsthon,
            id:req.params.id,
            id_phuongxa:req.params.id_phuongxa,
        }

        const status = await QuanLyThon.mergeThon(data.id, data.dsthon);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Gộp Thôn Thành Công',
            })
            .end();

    } catch (error) {
        next(error);
    }
}