const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');

function listRoot(DM_Data, rootParent, condition){
    for(var i = 0; i <DM_Data.length; i++){
        if(DM_Data[i].ID_CHA == condition){
            rootParent.push(DM_Data[i]);
        }
    }
    return rootParent;
}

function dequi(DM_Data, sum, increment, danhmuc){
    var children = []
    if(sum != DM_Data.length){
        if(checkList(DM_Data)){
            addChildrenHTTG(DM_Data, danhmuc[increment], children, danhmuc);
        }else{
            addChildren(DM_Data, danhmuc[increment], children, danhmuc);
        }
        children = []
        increment += 1
        sum += danhmuc[increment].length;
        dequi(DM_Data, sum, increment, danhmuc);
    }
}

function addChildren(DM_Data, listParent, listChildren, result){
    for(var i = 0; i <DM_Data.length; i++){
        for(var j = 0; j < listParent.length; j++){
            if(DM_Data[i].ID_CHA == listParent[j].MA_CHI_TIET_HOAN_CANH){
                listChildren.push(DM_Data[i]);
            }
        }
    } 
    return result.push(listChildren);
}

function addParent(parent1, parent2){
    for(var i = 0; i < parent1.length; i++){
        for(var j = 0; j < parent2.length; j++){
            if(parent1[i].MA_CHI_TIET_HOAN_CANH == parent2[j].ID_CHA){
                parent1[i].CON.push(parent2[j]);
            }
        }
    }
}

function addChildrenHTTG(DM_Data, listParent, listChildren, result){
    for(var i = 0; i <DM_Data.length; i++){
        for(var j = 0; j < listParent.length; j++){
            if(DM_Data[i].ID_CHA == listParent[j].MA_CHI_TIET_HINH_THUC){
                listChildren.push(DM_Data[i]);
            }
        }
    } 
    return result.push(listChildren);
}

function addParentHTTG(parent1, parent2){
    for(var i = 0; i < parent1.length; i++){
        for(var j = 0; j < parent2.length; j++){
            if(parent1[i].MA_CHI_TIET_HINH_THUC == parent2[j].ID_CHA){
                parent1[i].CON.push(parent2[j]);
            }
        }
    }
}

function finalList(danhmuc, DM_Data){
    for(var i = 0; i < danhmuc.length; i++){
        for(var j = 0; j < danhmuc[i].length; j++){
            danhmuc[i][j].CON = [];
        }
    }
    for(var i = danhmuc.length-1; i > 0; i--){
        if(checkList(DM_Data)){
            addParentHTTG(danhmuc[i-1], danhmuc[i]);
        }else{
            addParent(danhmuc[i-1], danhmuc[i]);
        }
    }
}

function checkList(namelist){
    if(namelist[0].hasOwnProperty('MA_CHI_TIET_HINH_THUC')){
        return true;
    }else{
        return false;
    }
}

module.exports.getData_DanhMuc_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            DanhMuc,
        } = new Model({ sqlConn });

        const DM_HCDB = await DanhMuc.getData_DM_HCDB();
        var rootParent_HCDB = [];
        listRoot(DM_HCDB, rootParent_HCDB, 0);
        var danhmuc_HCDB = [];
        danhmuc_HCDB.push(rootParent_HCDB);
        var sum_HCDB = danhmuc_HCDB[0].length;
        var increment_HCDB = 0
        dequi(DM_HCDB, sum_HCDB, increment_HCDB, danhmuc_HCDB);
        finalList(danhmuc_HCDB, DM_HCDB);
        var DANH_MUC_HOAN_CANH_DAC_BIET = [];
        DANH_MUC_HOAN_CANH_DAC_BIET.push(danhmuc_HCDB[0]);

        const DM_NCHCDB = await DanhMuc.getData_DM_NCHCDB();
        var rootParent_NCHCDB = [];
        listRoot(DM_NCHCDB, rootParent_NCHCDB, 0);
        var danhmuc_NCHCDB = [];
        danhmuc_NCHCDB.push(rootParent_NCHCDB);
        var sum_NCHCDB = danhmuc_NCHCDB[0].length;
        var increment_NCHCDB = 0
        dequi(DM_NCHCDB, sum_NCHCDB, increment_NCHCDB, danhmuc_NCHCDB);
        finalList(danhmuc_NCHCDB, DM_HCDB);
        var DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET = [];
        DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET.push(danhmuc_NCHCDB[0]);

        const DM_HCK = await DanhMuc.getData_DM_HCK();
        var rootParent_HCK = [];
        listRoot(DM_HCK, rootParent_HCK, 0);
        var danhmuc_HCK = [];
        danhmuc_HCK.push(rootParent_HCK);
        var sum_HCK = danhmuc_HCK[0].length;
        var increment_HCK = 0
        dequi(DM_HCK, sum_HCK, increment_HCK, danhmuc_HCK);
        finalList(danhmuc_HCK, DM_HCDB);
        var DANH_MUC_HOAN_CANH_KHAC = [];
        DANH_MUC_HOAN_CANH_KHAC.push(danhmuc_HCK[0]);

        const DM_HTTG = await DanhMuc.getData_DM_HTTG();
        var rootParent_HTTG = [];
        listRoot(DM_HTTG, rootParent_HTTG, 0);
        var danhmuc_HTTG = [];
        danhmuc_HTTG.push(rootParent_HTTG);
        var sum_HTTG = danhmuc_HTTG[0].length;
        var increment_HTTG = 0
        dequi(DM_HTTG, sum_HTTG, increment_HTTG, danhmuc_HTTG);
        finalList(danhmuc_HTTG, DM_HTTG);
        var DANH_MUC_HINH_THUC_TRO_GIUP = [];
        DANH_MUC_HINH_THUC_TRO_GIUP.push(danhmuc_HTTG[0]);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: {
                        DANH_MUC_HOAN_CANH_DAC_BIET,
                        DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET,
                        DANH_MUC_HOAN_CANH_KHAC,
                        DANH_MUC_HINH_THUC_TRO_GIUP
                    },
            })
            .end();

    } catch (error) {
        next(error);
    }
}