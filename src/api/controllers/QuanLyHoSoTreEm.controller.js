const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
const {
        getAllHoanCanh, 
        dequi, 
        detailHoanCanh, 
        eachRow, 
        detailHTTG, 
        eachRow1
} = require('../helpers/functionGetDetailChildren');
const {paginationSearch} = require('../helpers/pagination.helper')
const Joi = require('joi');
const { parse } = require('qs');

module.exports.getDataController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });


        const {
            thanhpho,
            huyen,
            xa,
            tenthanhpho,
            tenhuyen,
            tenxa,
            tenhienthi
        } = req.query
        

        const object = {
            id_tinhthanhpho : thanhpho ? thanhpho : null,
            id_quanhuyen : huyen ? huyen : null,
            id_phuongxa : xa ? xa : null,
            tenthanhpho : tenthanhpho ? tenthanhpho : null,
            tenhuyen : tenhuyen ? tenhuyen : null,
            tenxa : tenxa ? tenxa : null,
        }

        //Handle get total list children...
        // upgrade get list children -> pagination
        const [rows, fields] = await QuanLyHoSoTreEm.getData(object);
        const total = await QuanLyHoSoTreEm.getTotal(object);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                total: total,
                username: tenhienthi,
                location : {
                    id_tinh: object.id_tinhthanhpho,
                    thanhpho: object.tenthanhpho,
                    id_quan: object.id_quanhuyen,
                    quanhuyen: object.tenhuyen,
                    id_xa: object.id_phuongxa,
                    phuongxa: object.tenxa,
                },
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.getInfoChildrenWhenSearchController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        //Handle get total list children...
        // upgrade get list children -> pagination

        // Handle for APidoc because in Api when not pass variable then it not undefined that it empty String
        const {query} = req
        let checker;
        // const dataQuery = {}
        for(var k in query){
            if(query[k] == undefined){
                continue
            }
            if(typeof query[k] === 'string'){
                checker = query[k].trim() == 0
                if(checker){
                    query[k] = undefined
                }
            }
        }

        const data = {

            hoten:query.hoten,
            nguoinuoi:query.nguoinuoi,
            tinhthanhpho:query.thanhpho,
            quanhuyen:query.huyen,
            phuongxa:query.xa,
            thon:query.thon,
            id_giadinh:query.id_giadinh,
            thungrac:query.thungrac,
            dantoc:query.dantoc,
            gioitinh:query.gioitinh
        }
console.log("------------------------------------------")        
console.log({data})
        const object = {
            id_tinhthanhpho : query.thanhpho ? query.thanhpho : null,
            id_quanhuyen : query.huyen ? query.huyen : null,
            id_phuongxa : query.xa ? query.xa : null,
            tenthanhpho : query.tenthanhpho ? query.tenthanhpho : null,
            tenhuyen : query.tenhuyen ? query.tenhuyen : null,
            tenxa : query.tenxa ? query.tenxa : null,
            tenhienthi : query.tenhienthi
        }
        const [rows, fields] = await QuanLyHoSoTreEm.searchInfoChildren(data);
        const total = rows.length;
        // PageSize is a Option can change: 
        const pageSize = query.pageSize || 10
        const pageNumber = query.search || 1
        const ResultRows = paginationSearch(rows, pageSize, pageNumber)

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize:pageSize,
                pageNumber:pageNumber,
                total: total,
                username: object.tenhienthi,
                location : {
                    id_tinh: object.id_tinhthanhpho,
                    thanhpho: object.tenthanhpho,
                    id_quan: object.id_quanhuyen,
                    quanhuyen: object.tenhuyen,
                    id_xa: object.id_phuongxa,
                    phuongxa: object.tenxa,
                },
                result: ResultRows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.getDetailChildrenController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        /////////////////////
        const {id} = req.params

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        // const [checkParent, fieldsParent] = await QuanLyHoSoTreEm.getInfoPerson(id);

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null,
        }
        
        const [rowsInfo, fieldsInfo] = await QuanLyHoSoTreEm.getInfoChildren(id,object);
        const [rows1, fields1] = await QuanLyHoSoTreEm.getIdChildren(id)
        const [rowsHTTG, fieldsHTTG] = await QuanLyHoSoTreEm.getIdChildrenHTTG(id)
        const [rows, fields] =   await QuanLyHoSoTreEm.getChildrenHCDB();
        const [rows2, fields2] = await QuanLyHoSoTreEm.getChildrenNCHCDB();
        const [rows3, fields3] = await QuanLyHoSoTreEm.getChildrenHCK();
        const HCDB = getAllHoanCanh(rows);
        const NCRVHCDB = getAllHoanCanh(rows2);
        const HCK = getAllHoanCanh(rows3);

        // const resultNCHCDB = detailHoanCanh(rows1,cha2);
        // const resultHCK = detailHoanCanh(rows1, cha3);
        eachRow1(HCDB,rows1)
        eachRow1(NCRVHCDB,rows1)
        eachRow1(HCK,rows1)
        // const resultHCDB = detailHTTG(rows1, cha1);
        // const resultNCHCDB = detailHTTG(rows1,cha2);
        // const resultHCK = 

        let sum = 0;
        let result = [];
        let n = 0;
        const [rows4, fields4] = await QuanLyHoSoTreEm.getAllHTTG();
        const data = dequi(rows4,sum,result,n);
        const HinhThucTroGiup = detailHTTG(data);
        eachRow(HinhThucTroGiup,rowsHTTG)


        rowsInfo[0].Hinh_Thuc_Tro_Giup=HinhThucTroGiup
        rowsInfo[0].Hoan_Canh_Dac_Biet=HCDB
        rowsInfo[0].Nguy_Co_Roi_Vao_HCDB=NCRVHCDB
        rowsInfo[0].Hoan_Canh_Khac=HCK

        if(rowsInfo[0].trinhdohocvan == 'null'){
            rowsInfo[0].trinhdohocvan = JSON.parse('null')
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result:rowsInfo[0],
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.AddChildrenWithFamilyController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }

        const data = {

            tinhthanhpho:req.body.tinhthanhpho,
            quanhuyen:req.body.huyen,
            phuongxa:req.body.xa,
            id_giadinh:req.body.id_giadinh,
            id_thon:req.body.id_thon,
            hoten:req.body.hoten,
            ngaysinh:req.body.ngaysinh,
            gioitinh:req.body.gioitinh,
            dantoc: req.body.dantoc,
            trinhdohocvan:req.body.trinhdohocvan,
            trangthai:req.body.trangthai,
            ghichu:req.body.ghichu,
            cha:req.body.cha,
            me:req.body.me,
            id_hoancanh:req.body.id_hoancanh==undefined ? [] : req.body.id_hoancanh,
            id_trogiup:req.body.id_trogiup==undefined ? [] : req.body.id_trogiup,
        }

        function validateInfo(data){ 
            const JoiSchema = Joi.object({ 
                hoten: Joi.string().required(),
                ngaysinh: Joi.date().less('now'),
                dantoc: Joi.string().required(),
                gioitinh: Joi.string().required(),
            }).options({ abortEarly: false }); 
            return JoiSchema.validate(data) 
        }

        const validation = validateInfo({
            hoten : data.hoten,
            ngaysinh : data.ngaysinh,
            dantoc : data.dantoc,
            gioitinh : data.gioitinh
        });
        if(validation.error){
            return res.status(httpStatus.BAD_REQUEST)
            .json({
                code: httpStatus.BAD_REQUEST,
                message: 'Error Validation',
                // totalError: validation.error.details.length,
                result: validation.error.details,
                address: object,
            })
            .end();
        }

        const idChildren = await QuanLyHoSoTreEm.EnterInfoChildren_WithFamily(data)
        const [addChild, addChildFields] = await QuanLyHoSoTreEm.AddChildrenIntoFamily__(idChildren, data.id_giadinh)
        const addHTTG = await QuanLyHoSoTreEm.AddHTTG_WithFamily_OtherFamily(data,idChildren)
        const addHoanCanh = await QuanLyHoSoTreEm.AddHoanCanh_WithFamily_OtherFamily(data,idChildren)

        // Test Handle have true or false
        let result = "Add Fail";
        if(addHTTG==1 && addHoanCanh==1){
            result = "Add Success";
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: result,
                address: object
            })
            .end();

    } catch (error) {
        next(error);
    }
}


module.exports.AddChildrenOtherFamilyController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }
/* ISSUE : BODY { id_thon: require! } */
        const data = {

            hotencha:req.body.hotencha,
            hotenme:req.body.hotenme,
            cha: req.body.cha,
            me: req.body.me,
            idNguoiNuoi: req.body.idNguoiNuoi,
            nguoinuoiduong:req.body.nguoinuoiduong,
            sodienthoai:req.body.sodienthoai,
            diachi:req.body.diachi,
            hoancanh:req.body.hoancanh,
            id_thon:req.body.id_thon,
            hoten:req.body.hoten,
            ngaysinh:req.body.ngaysinh,
            gioitinh:req.body.gioitinh,
            dantoc: req.body.dantoc,
            trinhdohocvan:req.body.trinhdohocvan,
            ghichu:req.body.ghichu,
            id_hoancanh:req.body.id_hoancanh,
            id_trogiup:req.body.id_trogiup
        }

        const idFamily = await QuanLyHoSoTreEm.AddFamily_OtherFamily(data);
        const idChildren = await QuanLyHoSoTreEm.EnterInfoChildren_OtherFamily(data)
        const [addAll, addAllField] = await QuanLyHoSoTreEm.AddMember_OtherFamily(data, idFamily, idChildren)
        const addHTTG = await QuanLyHoSoTreEm.AddHTTG_WithFamily_OtherFamily(data,idChildren)
        const addHoanCanh = await QuanLyHoSoTreEm.AddHoanCanh_WithFamily_OtherFamily(data,idChildren)

        // Test Handle have true or false
        let result = "Add Fail";
        if(addHTTG==1 && addHoanCanh==1){
            result = "Add Success";
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: result,
                address : object
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.getInfoPersonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa,
        } = req.query

        const {
            id
        } = req.params

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }

        const [rows, fields] = await QuanLyHoSoTreEm.getInfoPerson(id);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Response Success',
                result: rows,
                address: object,
                id_person: id
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.logicalDeleteChildrenController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }

        let id_treem = req.params.id_treem;
        const [rows, fields] = await QuanLyHoSoTreEm.logicalDeleteChildren(id_treem);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Delete Children Into Trash Success',
                result: rows,
                address: object,
                id_chidlren: id_treem
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.physicalDeleteChildrenController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }

        let id_treem = req.params.id_treem;
        const [rows, fields] = await QuanLyHoSoTreEm.physicalDeleteChildren(id_treem);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: rows,
                address: object,
                id_chidlren: id_treem
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.updateDataChildrenController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const data = {
            hotencha:req.body.hotencha,
            hotenme:req.body.hotenme,
            cha: req.body.cha,
            me: req.body.me,
            id_giadinh:req.body.id_giadinh,
            idNguoiNuoi: req.body.idNguoiNuoi,
            nguoinuoiduong:req.body.nguoinuoiduong,
            sodienthoai:req.body.sodienthoai,
            diachi:req.body.diachi,
            hoancanh:req.body.hoancanh,
            id_thon:req.body.id_thon,
            hoten:req.body.hoten,
            ngaysinh:req.body.ngaysinh,
            gioitinh:req.body.gioitinh,
            dantoc: req.body.dantoc,
            trinhdohocvan:req.body.trinhdohocvan,
            ghichu:req.body.ghichu,
            id_hoancanh:req.body.id_hoancanh,
            id_trogiup:req.body.id_trogiup
        }

        let id = req.params.id

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }
   
        const [rows1, fields1] = await QuanLyHoSoTreEm.updateFamily(data);
        const [rows2, fields2] = await QuanLyHoSoTreEm.updateMemberFamily(data);
        const [rows3, fields3] = await QuanLyHoSoTreEm.updateData(data,id);
        const [rows4, fields4] = await QuanLyHoSoTreEm.deleteHoanCanh(id);
        const [rows5, fields5] = await QuanLyHoSoTreEm.deleteTroGiup(id);

        const addHoancanh = await QuanLyHoSoTreEm.updateHoanCanh(data, id);
        const updateTroGiup = await QuanLyHoSoTreEm.updateTroGiup(data, id);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Update successful',
                result: rows3,
                address: object,
                id_treem: id
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.updateDataPersonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        let data = req.body
        let id = req.params.id

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }
   
        const [rows, fields] = await QuanLyHoSoTreEm.updatePerson(data, id);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Update successful',
                result: rows,
                address: object,
                id_person: id,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.addDataPersonController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        let data = req.body

        const Object = {
            hoten : data.hoten==undefined ? '' : data.hoten,
            cmnd : data.cmnd==undefined ? '' : data.cmnd,
            ngaysinh : data.ngaysinh==undefined ? '' : data.ngaysinh,
            gioitinh : data.gioitinh==undefined ? '' : data.gioitinh,
            dantoc : data.dantoc==undefined ? '' : data.dantoc,
            sodienthoai : data.sodienthoai==undefined ? null : data.sodienthoai,
            trinhdohocvan : data.trinhdohocvan==undefined ? null : data.trinhdohocvan
        }

        function validateInfo(data){ 
            const JoiSchema = Joi.object({ 
                hoten: Joi.string().required(),
                cmnd: Joi.string().required().regex(/^\d{9}$/),
                ngaysinh: Joi.date().less('now').required(),
                dantoc: Joi.string().required(),
                gioitinh: Joi.string().required(),
            }).options({ abortEarly: false }); 
            return JoiSchema.validate(data) 
        }

        const validation = validateInfo({
            hoten : Object.hoten,
            cmnd: Object.cmnd,
            ngaysinh : Object.ngaysinh,
            dantoc : Object.dantoc,
            gioitinh : Object.gioitinh
        });

        if(validation.error){
            return res.status(httpStatus.BAD_REQUEST)
            .json({
                code: httpStatus.BAD_REQUEST,
                message: 'Error Validation',
                // totalError: validation.error.details.length,
                result: validation.error.details,
            })
            .end();
        }

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }
   
        const [rows, fields] = await QuanLyHoSoTreEm.addPerson(Object);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Add successful',
                result: rows,
                address: object,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.logicalDeleteChildrenMultiController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }

        const {
            listId
        } = req.body
console.log({listId})
        const isSuccess = await QuanLyHoSoTreEm.logicalDeleteChildrenMulti(listId);

        if(!isSuccess){
            return res.status(httpStatus.INTERNAL_SERVER_ERROR)
            .json({
                code: httpStatus.INTERNAL_SERVER_ERROR,
                message: 'Delete Children Into Trash Error',
                address: object,
            })
            .end();
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'Delete Children Into Trash Success',
                address: object,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.restoreChildrenMultiController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const object = {
            tinhthanhpho : thanhpho ? thanhpho : null,
            quanhuyen : huyen ? huyen : null,
            phuongxa : xa ? xa : null
        }

        const {
            listId
        } = req.body

        const isSuccess = await QuanLyHoSoTreEm.restoreChildrenMulti(listId);

        if(!isSuccess){
            return res.status(httpStatus.INTERNAL_SERVER_ERROR)
            .json({
                code: httpStatus.INTERNAL_SERVER_ERROR,
                message: 'Restore Children Error',
                address: object,
            })
            .end();
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'restore Children Success',
                address: object,
            })
            .end();

    } catch (error) {
        next(error);
    }
}
