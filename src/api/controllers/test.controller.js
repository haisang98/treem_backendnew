const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
/* SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
https://stackoverflow.com/questions/41887460/select-list-is-not-in-group-by-clause-and-contains-nonaggregated-column-inc
*/

module.exports.testController = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            test1,
        } = new Model({ sqlConn });

        const [rows, fields] = await test1.getData("sample data");

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

    module.exports.listUserController = async (req, res, next) => {
        try {
            const {
                sqlConn,
            } = req.app.locals;
    
            const {
                test1,
            } = new Model({ sqlConn });
    
            const [rows, fields] = await test1.listUser("data");
    
            return res.status(httpStatus.OK)
                .json({
                    code: httpStatus.OK,
                    message: 'responseMessage New',
                    result: rows,
                })
                .end();
    
        } catch (error) {
            next(error);
        }
    }

/* 
// for(let i=columnsInfoChildren.length+1, j=0; i<columnsInfoChildren.length+1+HCDB.length; i++, j++){
        //     columnsInfoChildren.push({
        //         header: 'Hoan Canh Dac Biet',
        //         key: 'hcdb',
        //         width: 15, 
        //         style: { alignment: { vertical:"middle", horizontal:"center" }  }
        //     })
        //     // worksheet.getRow(5).getCell(i).value = HCDB[j].ten_hoancanh;
        //     // worksheet.getRow(5).getCell(i).style = { alignment: { wrapText: true, horizontal:"center", vertical:"middle" }}
        // }

        // for(let i=columnsInfoChildren.length+HCDB.length+2, j=0; i<columnsInfoChildren.length+HCDB.length+2+NCHCDB.length; i++, j++){
        //     worksheet.getRow(5).getCell(i).value = NCHCDB[j].ten_hoancanh;
        //     worksheet.getRow(5).getCell(i).style = { alignment: { wrapText: true, horizontal:"center", vertical:"middle" }}
        // }

        // for(let i=columnsInfoChildren.length+HCDB.length+3+NCHCDB.length, j=0; i<columnsInfoChildren.length+HCDB.length+NCHCDB.length+3+HCK.length; i++, j++){
        //     worksheet.getRow(5).getCell(i).value = HCK[j].ten_hoancanh;
        //     worksheet.getRow(5).getCell(i).style = { alignment: { wrapText: true, horizontal:"center", vertical:"middle", shrinkToFit:true }}
        // }

        // for(let i=columnsInfoChildren.length+HCDB.length+4+NCHCDB.length+HCK.length, j=0; i<columnsInfoChildren.length+HCDB.length+NCHCDB.length+4+HCK.length+HTTG.length; i++, j++){
        //     worksheet.getRow(5).getCell(i).value = HTTG[j].tenhinhthuc;
        //     worksheet.getRow(5).getCell(i).style = { alignment: { wrapText: true, horizontal:"center", vertical:"middle" }}
        // }

        // worksheet.mergeCells(1,1,1,columnsInfoChildren.length+HCDB.length+NCHCDB.length+HCK.length+HTTG.length);
        // worksheet.mergeCells(3,1,1,columnsInfoChildren.length+HCDB.length+NCHCDB.length+HCK.length+HTTG.length);
        // worksheet.getCell('A1').style = { alignment: { horizontal:"left", vertical:"middle" } }
        // worksheet.getCell('C1').style = { alignment: { horizontal:"left", vertical:"middle" } }
*/
