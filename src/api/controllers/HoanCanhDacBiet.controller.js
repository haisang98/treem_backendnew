const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
var empty = require('is-empty');


module.exports.getData_HCDB_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            HoanCanhDacBiet,
        } = new Model({ sqlConn });

        const data = {
            id_tinhthanhpho: req.query.thanhpho,
            id_quanhuyen: req.query.huyen,
            id_phuongxa: req.query.xa
        }

        

        var getData = "";
        if(data.id_phuongxa == undefined){
            if(data.id_quanhuyen == undefined){
                getData = "ct.id_tinhthanhpho = " + data.id_tinhthanhpho
            }else{
                getData = "ct.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "qh.id_quanhuyen  = " + data.id_quanhuyen
            }
        }else{
            getData = "ct.id_tinhthanhpho = " + data.id_tinhthanhpho + " AND " + "qh.id_quanhuyen = " + data.id_quanhuyen + " AND " + "px.id_phuongxa  = " + data.id_phuongxa
        }
        
        

        const [rows, fields] = await HoanCanhDacBiet.getData_HoanCanhDacBiet(getData);
        const getData_DM_HCDB = await HoanCanhDacBiet.getData_DM_HCDB();
        const getData_DM_CT_HCDB = await HoanCanhDacBiet.getData_DM_CT_HCDB();
        // total = rows.length;
        

        var hoancanhdacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];

            if(getData_DM_CT_HCDB[0][i].cha == 0){
                var dcha1 =getData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =getData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }

                
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];
                var dcha1 = getData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = getData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
        

            hoancanhdacbiet.push(rows[i]);
            //
        }

        var ds_f1 = [];
        ds_f1.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < ds_f1.length; j ++){
                if(rows[i].id_treem == ds_f1[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(ds_f1[j].hoancanhdacbiet[k].value == "true"){
                            rows[i].hoancanhdacbiet[k].value = "true";
                        }
                    }
                    ds_f1.splice(j, 1);
                }
            }
            ds_f1.push(rows[i]);
        }
        

        let page = req.query.page;
        var pagesize = req.query.pagesize; //-------------------------------------------------

        if(empty(page)){
            page = 1;
        }
        
        if(empty(pagesize)){
            pagesize = 20;
        }



        var start = (page - 1) * pagesize;
        var end = pagesize*page - 1;
        var pagination = [];
        var total = ds_f1.length;
        
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(ds_f1[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(ds_f1[start]);
            }
        }

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: Number(pagesize),
                pageNumber:  Number(page),
                total: total,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}


module.exports.searchData_HCDB_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            HoanCanhDacBiet,
        } = new Model({ sqlConn });

        const {
            thon,
            hoancanh,
            magiadinh,
            tentreem,
            timestart,
            timefinish
        } = req.query
        var req_f = req.query;

        
        const Object = {
            // id_tinhthanhpho: thanhpho ? thanhpho : null,
            // id_quanhuyen: huyen ? huyen : null,
            // id_phuongxa: xa ? xa : null,
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            id_hoancanh: hoancanh ? hoancanh : null,
            id_giadinh: magiadinh ? (isNaN(magiadinh) ? -123456789 : magiadinh) : null,
            hoten: tentreem ? tentreem : null,
            ts: timestart ? timestart : null,
            tf: timefinish ? timefinish : null
        };  

        
        timeS = Object.ts;
        timeF = Object.tf;
        // 
        // 

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()
        //

        
        function timeD(a, b){
            var temp = "";
            if(a != null && b != null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
            }else if(a != null && b == null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        var search = "";
        for(const k in Object){
            if(Object[k]  != null && k != 'ts' && k != 'tf'){
                if(empty(search)){
                    if(k == 'hoten'){
                        search = k + " LIKE '%" + Object[k] + "%'";
                    }else{
                        search = k + " = " + Object[k];
                    }  
                }else{
                    if(k == 'hoten'){
                        search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                    } else{
                        search += " AND " + k + " = " + Object[k] ;
                    }  
                }
            }
        } 

        if(empty(search)){
            search = timeD(timeS, timeF);
        }else{
            if(timeD(timeS, timeF) == ""){
                search += "";
            }else{
                search += " AND " + timeD(timeS, timeF);
            }
        }

        if(empty(search)){
            search = " 1 ";
        }

        //---------------------------------------------------------------------------
        
        const [rows, fields] = await HoanCanhDacBiet.searchData_HCDB(search);
        const getData_DM_HCDB = await HoanCanhDacBiet.getData_DM_HCDB();
        const searchData_DM_CT_HCDB = await HoanCanhDacBiet.searchData_DM_CT_HCDB(search);

        var hoancanhdacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];

            if(searchData_DM_CT_HCDB[0][i].cha == 0){
                var dcha1 =searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }

                
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];
                var dcha1 = searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
        

            hoancanhdacbiet.push(rows[i]);
            //
        }

        var ds_f1 = [];
        ds_f1.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < ds_f1.length; j ++){
                if(rows[i].id_treem == ds_f1[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(ds_f1[j].hoancanhdacbiet[k].value == "true"){
                            rows[i].hoancanhdacbiet[k].value = "true";
                        }
                    }
                    ds_f1.splice(j, 1);
                }
            }
            ds_f1.push(rows[i]);
        }

        if(!ds_f1[0]){
            ds_f1 = [...ds_f1.slice(1)];
        }

        let page = req.query.page;
        var pagesize = req.query.pagesize; //-------------------------------------------------
        if(empty(page)){
            page = 1;
        }
        
        if(empty(pagesize)){
            pagesize = 10;
        }



        var start = (page - 1) * pagesize;
        var end = pagesize*page - 1;
        var pagination = [];
        var total = ds_f1.length;
        if(total > end){
            for(start; start <= end; start++){
                pagination.push(ds_f1[start]);
            }
        }else{
            for(start; start < total; start++){
                pagination.push(ds_f1[start]);
            }
        }


        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                pageSize: pagesize,
                pageNumber:  Number(page),
                total: total,
                search: req_f,
                result: pagination,
            })
            .end();

    } catch (error) {
        next(error);
    }
}