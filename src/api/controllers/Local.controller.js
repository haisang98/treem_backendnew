const httpStatus = require('http-status');
const Model = require('../models');
const request = require('request');
var empty = require('is-empty');
const {
    cloneGetAllHoanCanhFormatTreeSelect,
    deQuyTreeSelect,
    detailHTTGTreeSelect
} = require('../helpers/functionGetDetailChildren');


module.exports.getDistrict_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            Local,
        } = new Model({ sqlConn });
        
        let thanhpho = req.params.thanhpho
        const [rows, fields] = await Local.showListDistrict(thanhpho);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                total: rows.length,
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}



module.exports.getWard_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            Local,
        } = new Model({ sqlConn });
        
        let huyen = req.params.huyen
        const [rows, fields] = await Local.showListWard(huyen);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                total: rows.length,
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}


module.exports.getVillage_Controller = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            Local,
        } = new Model({ sqlConn });
        
        let xa = req.params.xa
        const [rows, fields] = await Local.showListVillage(xa);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                total: rows.length,
                result: rows,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.getList_HCDB = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm
        } = new Model({ sqlConn });
        
        const [rows, fields] = await QuanLyHoSoTreEm.cloneGetChildrenHCDB();

        const HCDB = cloneGetAllHoanCanhFormatTreeSelect(rows)

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: HCDB,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.getList_NCHCDB = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm
        } = new Model({ sqlConn });
        
        const [rows, fields] = await QuanLyHoSoTreEm.cloneGetChildrenNCHCDB();

        const HCDB = cloneGetAllHoanCanhFormatTreeSelect(rows)

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: HCDB,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.getList_HCK = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm
        } = new Model({ sqlConn });
        
        const [rows, fields] = await QuanLyHoSoTreEm.cloneGetChildrenHCK();

        const HCDB = cloneGetAllHoanCanhFormatTreeSelect(rows)

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: HCDB,
            })
            .end();

    } catch (error) {
        next(error);
    }
}

module.exports.getList_HTTG = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyHoSoTreEm
        } = new Model({ sqlConn });
        
        const [rows, fields] = await QuanLyHoSoTreEm.cloneGetAllHTTG();

        let sum = 0;
        let result = [];
        let n = 0;
        const data = deQuyTreeSelect(rows,sum,result,n);
        const HCDB = detailHTTGTreeSelect(data);

        return res.status(httpStatus.OK)
            .json({
                code: httpStatus.OK,
                message: 'responseMessage',
                result: HCDB,
            })
            .end();

    } catch (error) {
        next(error);
    }
}