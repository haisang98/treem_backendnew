const Router = require('express').Router();

const {
    getData_NguyCoHCDB_Controller,
    searchData_NCHCDB_Controller
} = require('../../controllers/NguyCoHCDB.controller');
const {
    isRoleLocal
} = require('../../middlewares/Local.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')

/**
 * @api {get} /v1/api/nguy-co-hcdb Get List Risk Of Falling Into Special Circumstances

 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/nguy-co-hcdb
 * 
 * @apiName  ListRiskOfFallingIntoSpecialCircumstances
 * @apiGroup RiskOfFallingIntoSpecialCircumstances
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.nguycoHCDB  result.nguycoHCDB Response result.nguycoHCDB
 * @apiSuccess {String} result.nguycoHCDB.name RiskOfFallingIntoSpecialCircumstances name
 * @apiSuccess {String} result.nguycoHCDB.value RiskOfFallingIntoSpecialCircumstances value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * 
 * @apiSuccessExample {json} success Response
 *  {
 *     "code": 200,
 *     "message": "responseMessage",
 *     "pageSize": 10,
 *     "pageNumber": 1,
 *     "total": 1,
 *     "result": [
 *         {
 *             "id_treem": 230071,
 *             "hoten": "Hoàng Minh Tú",
 *             "ngaysinh": "2009-09-27T17:00:00.000Z",
 *             "id_giadinh": 161663,
 *             "nguoinuoiduong": "",
 *             "hotencha": "",
 *             "hotenme": "Đoàn Thị Ngoan",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nam",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "nguycoHCDB": [
 *                 {
 *                     "name": "1 Trẻ em sống trong gia đình nghèo, cận nghèo ",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "2 Trẻ em bỏ học - chưa học xong chương trình THCS ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "3 Trẻ em sống trong các gia đình có vấn đề xã hội (cha mẹ ly hôn, bạo lực gia đình, có người nhiễm HIV/AIDS)",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "4 Trẻ em sống trong gia đình có người mắc tệ nạn xã hội (cha, mẹ, người nuôi dưỡng hoặc thành viên trong gia đình trực tiếp nghiệm ma túy, cờ bạc, trộm cắp, mại dâm) ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "5 Trẻ em sống trong gia đình có người vi phạm pháp luật (cha, mẹ, người nuôi dưỡng hoặc thành viên gia đình đang trong thời gian thi hành án) ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "6 Trẻ em sống trong gia đình có cha mẹ làm ăn xa (trẻ em dưới 16 tuổi phải sống xa bố hoặc mẹ, hoặc cả bố và mẹ liên tục từ 6 tháng trở lên do bố mẹ đi làm ăn xa) ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "7 Trẻ em mồi côi cha hoặc mồ côi mẹ ",
 *                     "value": "false"
 *                 }
 *             ]
 *         }
 *     ]
 *  }
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *          message: "API Not Found GET (endpoint)",
 *      }
 * 
 */

 /**
 * @api {get} /v1/api/nguy-co-hcdb/search Search List Risk Of Falling Into Special Circumstances

 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/nguy-co-hcdb/search
 * 
 * @apiName  SearchListRiskOfFallingIntoSpecialCircumstances
 * @apiGroup RiskOfFallingIntoSpecialCircumstances
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.id_thon Village unique ID
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.id_phuongxa Ward unique ID
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.id_quanhuyen District unique ID
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.id_tinhthanhpho City unique ID
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.nguycoHCDB  result.nguycoHCDB Response result.nguycoHCDB
 * @apiSuccess {String} result.nguycoHCDB.name RiskOfFallingIntoSpecialCircumstances name
 * @apiSuccess {String} result.nguycoHCDB.value RiskOfFallingIntoSpecialCircumstances value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * @apiParam {number} thanhpho City unique ID
 * @apiParam {number} huyen District unique ID
 * @apiParam {number} xa Ward unique ID
 * @apiParam {number} thon Village unique ID
 * @apiParam {String} tentreem  Children name
 * @apiParam {number} hoancanh Circumstances unique ID
 * @apiParam {number} magiadinh Famity unique ID
 * @apiParam {date} timestart Search Start Time
 * @apiParam {date} timefinish Search End Time
 * 
 * @apiSuccessExample {json} success Response
 *  {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "pageSize": 10,
 *      "pageNumber": 1,
 *      "total": 1,
 *      "search": {
 *         "page": "1",
 *         "pagesize": "10",
 *         "thanhpho": "",
 *         "huyen": "",
 *         "xa": "",
 *         "thon": "",
 *         "tentreem": "Hoàng Minh Tú",
 *         "hoancanh": "",
 *         "magiadinh": "",
 *         "timestart": "",
 *         "timefinish": ""
 *      },
 *      "result": [
 *         {
 *             "id_treem": 230071,
 *             "hoten": "Hoàng Minh Tú",
 *             "ngaysinh": "2009-09-27T17:00:00.000Z",
 *             "id_giadinh": 161663,
 *             "nguoinuoiduong": "",
 *             "hotencha": "",
 *             "hotenme": "Đoàn Thị Ngoan",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nam",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "nguycoHCDB": [
 *                 {
 *                     "name": "1 Trẻ em sống trong gia đình nghèo, cận nghèo ",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "2 Trẻ em bỏ học - chưa học xong chương trình THCS ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "3 Trẻ em sống trong các gia đình có vấn đề xã hội (cha mẹ ly hôn, bạo lực gia đình, có người nhiễm HIV/AIDS)",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "4 Trẻ em sống trong gia đình có người mắc tệ nạn xã hội (cha, mẹ, người nuôi dưỡng hoặc thành viên trong gia đình trực tiếp nghiệm ma túy, cờ bạc, trộm cắp, mại dâm) ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "5 Trẻ em sống trong gia đình có người vi phạm pháp luật (cha, mẹ, người nuôi dưỡng hoặc thành viên gia đình đang trong thời gian thi hành án) ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "6 Trẻ em sống trong gia đình có cha mẹ làm ăn xa (trẻ em dưới 16 tuổi phải sống xa bố hoặc mẹ, hoặc cả bố và mẹ liên tục từ 6 tháng trở lên do bố mẹ đi làm ăn xa) ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "7 Trẻ em mồi côi cha hoặc mồ côi mẹ ",
 *                     "value": "false"
 *                 }
 *             ]
 *         }
 *     ]
 *  }
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *          message: "API Not Found GET (endpoint)",
 *      }
 * 
 */

Router.route('/').get(
    isAuth,
    isRoleLocal,
    getData_NguyCoHCDB_Controller
)
Router.route('/search').get(
    isAuth,
    isRoleLocal,
    searchData_NCHCDB_Controller
)


module.exports = Router;