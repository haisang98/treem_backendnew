const Router = require('express').Router();

const {
    getDistrict_Controller, getVillage_Controller, getWard_Controller,
    getList_HCDB, getList_NCHCDB, getList_HCK, getList_HTTG,
} = require('../../controllers/Local.controller');

/**
 * @api {get} /v1/api/local Get List District
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/local/thanhpho/60
 * 
 * @apiName ListDistrict
 * @apiGroup Local
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_tinhthanhpho City unique ID
 * @apiSuccess {String} result.ten_tinhthanhpho City name
 * @apiSuccess {String} result.id_quanhuyen District unique ID
 * @apiSuccess {String} result.ten_quanhuyen District name
 * 
 * @apiSuccessExample {json} success Response
 *   
 *  {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "total": 11,
 *      "result": [
 *          {
 *              "id_tinhthanhpho": 60,
 *              "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *              "id_quanhuyen": 660,
 *              "ten_quanhuyen": "Thành phố Mỹ Tho"
 *          },
 *              .
 *              .
 *              .
 *              .
 *              .
 *  }
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */

 //------------------------------------------------------------------------

 /**
 * @api {get} /v1/api/local Get List Ward
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/local/thanhpho/huyen/660
 * 
 * @apiName ListWard
 * @apiGroup Local
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_quanhuyen District unique ID
 * @apiSuccess {String} result.ten_quanhuyen District name
 * @apiSuccess {String} result.id_phuongxa Ward unique ID
 * @apiSuccess {String} result.ten_phuongxa Ward name
 * 
 * @apiSuccessExample {json} success Response
 *   
 *  {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "total": 17,
 *      "result": [
 *          {
 *              "id_quanhuyen": 660,
 *              "ten_quanhuyen": "Thành phố Mỹ Tho",
 *              "id_phuongxa": 15028,
 *              "ten_phuongxa": "Phường 5"
 *          },
 *              .
 *              .
 *              .
 *              .
 *              .
 *      ]
 *  }
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */

 //------------------------------------------------------------------------

 /**
 * @api {get} /v1/api/local Get List Village
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/local/thanhpho/huyen/xa/15028
 * 
 * @apiName ListVillage
 * @apiGroup Local
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_phuongxa Ward unique ID
 * @apiSuccess {String} result.ten_phuongxa Ward name
 * @apiSuccess {String} result.id_thon Village unique ID
 * @apiSuccess {String} result.tenthon Village name
 * 
 * @apiSuccessExample {json} success Response
 *   
 *  {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "total": 11,
 *      "result": [
 *          {
 *              "id_phuongxa": 15028,
 *              "ten_phuongxa": "Phường 5",
 *              "id_thon": 88215,
 *              "tenthon": "Khu phố 10"
 *          },
 *              .
 *              .
 *              .
 *              .
 *              .
 *      ]
 *  }
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */

Router.route('/thanhpho/:thanhpho').get(
    getDistrict_Controller
)

Router.route('/thanhpho/huyen/:huyen').get(
    getWard_Controller
)

Router.route('/thanhpho/huyen/xa/:xa').get(
    getVillage_Controller
)

Router.route('/list/hoancanhdacbiet').get(
    getList_HCDB
)

Router.route('/list/nguycohoancanhdacbiet').get(
    getList_NCHCDB
)

Router.route('/list/hoancanhkhac').get(
    getList_HCK
)

Router.route('/list/hinhthuctrogiup').get(
    getList_HTTG
)


module.exports = Router;