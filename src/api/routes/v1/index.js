const Router = require('express').Router();

const testRouter = require('./test.router');
const tinhThanhPhoRouter = require('./tinhthanhpho.router')
const quanLyHoSoTreEmRouter = require('./QuanLyHoSoTreEm.router')
const NguyCoHCDBRouter = require('./NguyCoHCDB.router')
const HoanCanhDacBietRouter = require('./HoanCanhDacBiet.router')
const HoanCanhKhacRouter = require('./HoanCanhKhac.router')
const HinhThucTroGiup = require('./HinhThucTroGiup.router')
const LocalRouter = require('./Local.router')
const DanhMucRouter = require('./DanhMuc.router')
const Authentication = require('./User.router')
const QuanLyThon = require('./QuanLyThon.router')
const DSHoGiaDinh = require('./DanhSachHoGiaDinh.router')
const Account = require('./Account.router')
const Excel = require('./Excel.router')

const { env } = require('../../../config/vars');
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')
/**
 * APIs doc - Only available in development environment
 * @dev
 */
if (env === 'development') {
    Router.use('/v1/api/test', testRouter);
    Router.use('/v1/api/tinhthanhpho', tinhThanhPhoRouter);
    Router.use('/v1/api/quan-ly-tre-em', quanLyHoSoTreEmRouter);
    Router.use('/v1/api/nguy-co-hcdb', NguyCoHCDBRouter);
    Router.use('/v1/api/tre-em-hoan-canh-dac-biet', HoanCanhDacBietRouter);
    Router.use('/v1/api/tre-em-hoan-canh-khac', HoanCanhKhacRouter);
    Router.use('/v1/api/hinh-thuc-tro-giup', HinhThucTroGiup);
    Router.use('/v1/api/local', LocalRouter);
    Router.use('/v1/api/danh-muc', DanhMucRouter);
    Router.use('/v1/api/user', Authentication);
    Router.use('/v1/api/quan-ly-thon', QuanLyThon);
    Router.use('/v1/api/danh-sach-ho-gia-dinh', DSHoGiaDinh)
    Router.use('/v1/api/account', Account)
    Router.use('/v1/api/excel', Excel)
}


/**
 * API routes
 */

module.exports = Router;
