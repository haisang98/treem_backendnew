const router = require('express').Router();
const {
    LoginController,
    RefreshTokenController,
    LogoutController,
    ChangePasswordController,
    RegisterController
} = require('../../controllers/Authentication.controller')

const {isAuth} = require('../../middlewares/Authentication.middleware');

/**
* @api {post} /v1/api/user/login Login
* @apiVersion 1.0.0
* @apiSampleRequest http://localhost:5000/v1/api/user/login
*
* @apiExample Example usage:
*          body:
*          {
*              "username":"admin",
               "password":"12345678"
*          }
* 
*
* @apiName Login
* @apiGroup User
 * @apiHeader access-token
*
* @apiParam {String} username Username
* @apiParam {String} password Password
*
* @apiSuccess {Code} code return code status
* @apiSuccess {String} message Login Success
* @apiSuccess {Object[]} result Returned object
* @apiSuccess {String} result.accessToken return access-token
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*           code: 200,
*           message:"Login Success",
*           result: {
*               accessToken:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRnX2N0XzEiLCJpYXQiOjE1OTczOTg3MjQsImV4cCI6MTU5NzM5OTA4NH0.99uLl2LLPfQtBdW4muM7FW9PzH2A0ZTVXzqiEA3Bhic",
*           },
*     }
*
*
* @apiError (404 Not Found) API not found
* @apiError (401 Login Fail) Username or Password not true
* @apiErrorExample {json} 404-Response
*      HTTP/1.1 404 Not Found
*      {
*          code: 404,
*          message: "API Not Found GET {endpoint}",
*      }
*
*      HTTP/1.1 401 Username not exists
*      {
*          code: 401,
*          message: "User not exists",
*      }
*
*      HTTP/1.1 401 Password false
*      {
*          code: 401,
*          message: "Password Invalid",
*      }
*
*/
router.route('/login').post(
    LoginController
)

router.route('/products').get(
    isAuth,(req, res, next) => {
        res.send("List Products: Orange, Mango, Cherry, ...")
    }
)

router.route('/refresh-token').post(
    RefreshTokenController
)

/**
* @api {post} /v1/api/user/logout Logout
* @apiVersion 1.0.0
* @apiSampleRequest http://localhost:5000/v1/api/user/logout
*
* @apiName Logout
* @apiGroup User
 * @apiHeader access-token
*
* @apiHeader {String} access-token Users unique access-key.
* @apiHeaderExample {json} Request-Example:
*      { 
*          "access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRnX2N0XzEiLCJpYXQiOjE1OTczOTg3MjQsImV4cCI6MTU5NzM5OTA4NH0.99uLl2LLPfQtBdW4muM7FW9PzH2A0ZTVXzqiEA3Bhic" 
*      }
*
*
* @apiSuccess {Number} code return code status
* @apiSuccess {String} message Login Success
* @apiSuccess {Object[]} result Returned object
* @apiSuccess {String} result.accessToken return access-token
* @apiParam (body) {String} accessToken Access Token
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*           code: httpStatus.OK,
*           message: "responseMessage",
*           result: {
*               message: "Login Success",
*               accessToken:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRnX2N0XzEiLCJpYXQiOjE1OTczOTg3MjQsImV4cCI6MTU5NzM5OTA4NH0.99uLl2LLPfQtBdW4muM7FW9PzH2A0ZTVXzqiEA3Bhic",
*           },
*     }
*
* @apiError (404 Not Found) API not found
* @apiError (401 Login Fail) Username or Password not true

* @apiErrorExample {json} 404-Response
*      HTTP/1.1 404 Not Found
*      {
*          code: 404,
*          message: "API Not Found GET {endpoint}",
*      }
*
*      HTTP/1.1 401 Username not exists
*      {
*          code: 401,
*          message: "User not exists",
*      }
*
*      HTTP/1.1 401 Password false
*      {
*          code: 401,
*          message: "Password Invalid",
*      }
*
*/
router.route('/logout').post(
    LogoutController
)

router.route('/changepassword').post(
    isAuth,
    ChangePasswordController
)

router.route('/register').post(
    isAuth,
    RegisterController
)
module.exports = router
