const Router = require('express').Router();

const {
    getData_HCDB_Controller, searchData_HCDB_Controller
} = require('../../controllers/HoanCanhDacBiet.controller');

const {
    isRoleLocal
} = require('../../middlewares/Local.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')


/**
 * @api {get} /v1/api/tre-em-hoan-canh-dac-biet Get List Special Circumstances
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/tre-em-hoan-canh-dac-biet
 * 
 * @apiName ListSpecialCircumstances
 * @apiGroup SpecialCircumstances
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} pageSize Response Page Size
 * @apiSuccess {Number} pageNumber Response Page Number
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.hoancanhdacbiet  result.hoancanhdacbiet Response result.hoancanhdacbiet
 * @apiSuccess {String} result.hoancanhdacbiet.name SpecialCircumstances name
 * @apiSuccess {String} result.hoancanhdacbiet.value SpecialCircumstances value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * 
 * @apiSuccessExample {json} success Response
 *  {
 *     "code": 200,
 *    "message": "responseMessage",
 *     "pageSize": 20,
 *     "pageNumber": 1,
 *     "total": 10,
 *     "result": [
 *         {
 *             "id_treem": 230069,
 *             "hoten": "Trần Thị Bê",
 *             "ngaysinh": "2019-12-31T17:00:00.000Z",
 *             "id_giadinh": 161662,
 *             "nguoinuoiduong": "",
 *             "hotencha": "Trần Văn Cê",
 *             "hotenme": "Nguyễn Thị A",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nữ",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "id_chitietloaihoancanh": 287,
 *             "hoancanhdacbiet": [
 *                 {
 *                     "name": "1 Trẻ em mồ côi cả cha và mẹ ",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "2 Trẻ em bị bỏ rơi ",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "3 Trẻ em không nơi nương tự",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "4.Trẻ em khuyết tật",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "5 Trẻ em nhiễm HIV/AIDS",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "6 Trẻ em vi phạm pháp luật",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "7 Trẻ em nghiện ma túy",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "8 Trẻ em phải bỏ học kiếm sống chưa hoàn thành phổ cập giáo dục trung học cơ sở",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "9 Trẻ em bị tổn hại nghiêm trọng về thể chất và tinh thần do bị bạo lực ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "10 Trẻ em bị bóc lột",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "11 Trẻ em bị xâm hại tình dục",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "12 Trẻ em bị mua bán",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "13 Trẻ em mắc bệnh hiểm nghèo hoặc bệnh phải điều trị dài ngày thuộc hộ nghèo hoặc hộ cận nghèo",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "14 Trẻ em di cư, trẻ em lánh nạn, tị nạn chưa xác định được cha mẹ hoặc không có người chăm sóc",
 *                     "value": "false"
 *                 }
 *             ]
 *         },
 *           .
 *           .
 *           .
 *      ]
 *  }
 * 
 * 
 * 
 * 
 * 
 * 
 *  
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */





 /**
 * @api {get} /v1/api/tre-em-hoan-canh-dac-biet/search  Search List Special Circumstances
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/tre-em-hoan-canh-dac-biet/search
 * 
 * @apiName SearchListSpecialCircumstances
 * @apiGroup SpecialCircumstances
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} search Response search
 * @apiSuccess {Number} search.page Response Page Number
 * @apiSuccess {Number} search.pagesize Response Page Size Number
 * @apiSuccess {Number} search.thanhpho Response city name
 * @apiSuccess {String} search.xa Village unique ID
 * @apiSuccess {String} search.thon Ward unique ID
 * @apiSuccess {String} search.huyen District unique ID
 * @apiSuccess {String} search.thanhpho City unique ID
 * @apiSuccess {String} search.hoancanh Circumstances unique ID
 * @apiSuccess {String} search.tentreem  Children name
 * @apiSuccess {number} search.magiadinh Famity unique ID
 * @apiSuccess {date} search.timestart Search Start Time
 * @apiSuccess {date} search.timefinish Search End Time
 * 
 * 
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.id_thon Village unique ID
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.id_phuongxa Ward unique ID
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.id_quanhuyen District unique ID
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.id_tinhthanhpho City unique ID
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.hoancanhdacbiet  result.hoancanhdacbiet Response result.hoancanhdacbiet
 * @apiSuccess {String} result.hoancanhdacbiet.name SpecialCircumstances name
 * @apiSuccess {String} result.hoancanhdacbiet.value SpecialCircumstances value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * @apiParam {number} xa Ward unique ID
 * @apiParam {number} thon Village unique ID
 * @apiParam {String} tentreem  Children name
 * @apiParam {number} hoancanh FormOfHelp unique ID
 * @apiParam {number} magiadinh Famity unique ID
 * @apiParam {date} timestart Search Start Time
 * @apiParam {date} timefinish Search End Time
 * 
 * @apiSuccessExample {json} success Response
 *   {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "pageSize": 10,
 *      "pageNumber": 1,
 *      "total": 1,
 *      "search": {
 *          "page": "1",
 *          "pagesize": "10",
 *          "thanhpho": "",
 *          "huyen": "",
 *          "xa": "",
 *          "thon": "",
 *          "tentreem": "Lê Bảo Nghi",
 *          "hoancanh": "",
 *          "magiadinh": "",
 *          "timestart": "",
 *          "timefinish": ""
 *      },
 *      "result": [
 *          {
 *              "id_treem": 406053,
 *              "hoten": "Lê Bảo Nghi",
 *              "ngaysinh": "2020-01-31T17:00:00.000Z",
 *              "id_giadinh": 293810,
 *              "nguoinuoiduong": "",
 *              "hotencha": "",
 *              "hotenme": "Lê Ngọc Diễm",
 *              "dantoc": "Kinh",
 *              "gioitinh": "Nữ",
 *              "tenthon": "Ấp Me",
 *              "id_thon": 22451,
 *              "ten_phuongxa": "Thị trấn Tân Hiệp",
 *              "id_phuongxa": 15123,
 *              "ten_quanhuyen": "Huyện Châu Thành",
 *              "id_quanhuyen": 665,
 *              "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *              "id_tinhthanhpho": 60,
 *              "id_chitietloaihoancanh": 289,
 *              "hoancanhdacbiet": [
 *                  {
 *                      "name": "1 Trẻ em mồ côi cả cha và mẹ ",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "2 Trẻ em bị bỏ rơi ",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "3 Trẻ em không nơi nương tự",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "4.Trẻ em khuyết tật",
 *                      "value": "true"
 *                  },
 *                  {
 *                      "name": "5 Trẻ em nhiễm HIV/AIDS",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "6 Trẻ em vi phạm pháp luật",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "7 Trẻ em nghiện ma túy",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "8 Trẻ em phải bỏ học kiếm sống chưa hoàn thành phổ cập giáo dục trung học cơ sở",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "9 Trẻ em bị tổn hại nghiêm trọng về thể chất và tinh thần do bị bạo lực ",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "10 Trẻ em bị bóc lột",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "11 Trẻ em bị xâm hại tình dục",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "12 Trẻ em bị mua bán",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "13 Trẻ em mắc bệnh hiểm nghèo hoặc bệnh phải điều trị dài ngày thuộc hộ nghèo hoặc hộ cận nghèo",
 *                      "value": "false"
 *                  },
 *                  {
 *                      "name": "14 Trẻ em di cư, trẻ em lánh nạn, tị nạn chưa xác định được cha mẹ hoặc không có người chăm sóc",
 *                      "value": "false"
 *                  }
 *              ]
 *          }
 *      ]
 *  }
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */
Router.route('/').get(
    isAuth,
    isRoleLocal,
    getData_HCDB_Controller
)

Router.route('/search').get(
    isAuth,
    isRoleLocal,
    searchData_HCDB_Controller
)

// * @apiParam {number} huyen District unique ID
// * @apiParam {number} thanhpho City unique ID
// * @apiParam {number} huyen District unique ID



module.exports = Router;