const Router = require('express').Router();
const {
    addThonController, getThonController,
    deleteThonController, updateThonController,
    mergeThonController,
} = require('../../controllers/QuanLyThon.controller');
const {
    isRoleManagerVillage,
    isExistsChildrenInLocal,
} = require('../../middlewares/QuanLyThon.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')

/**
 * @api {get} /v1/api/test/ Get testing route
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/test
 * 
 * @apiName FirstTest
 * @apiGroup Test
 * @apiHeader access-token
 * 
 */
Router.route('/').get(
    isAuth,
    isRoleManagerVillage,
    getThonController
)

Router.route('/add').post(
    isAuth,
    isRoleManagerVillage,
    addThonController
)

Router.route('/delete/:id').delete(
    isAuth,
    isRoleManagerVillage,
    isExistsChildrenInLocal,
    deleteThonController
)

Router.route('/update/:id').put(
    isAuth,
    isRoleManagerVillage,
    updateThonController
)

Router.route('/merge/:id').put(
    isAuth,
    isRoleManagerVillage,
    mergeThonController,
)

module.exports = Router;