const Router = require('express').Router();

const {
    getAccountController,
    updateAccountController
} = require('../../controllers/Account.controller');
const {
    isRoleManagerAccount
} = require('../../middlewares/Account.middleware')
const {
    isRoleLocal,
} = require('../../middlewares/Local.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')
const upload = require('../../middlewares/Excel.middleware')

const { 
    getExcelSample,
    uploadFile,
    downloadFileExcel,
    downloadFileExcelNCHCDB,
    downloadFileExcelHCK,
    downloadFileExcelHTTG,
    downloadFileExcelFamily,
    downloadFileExcelChildrenKTDL,
    downloadFileExcelChildrenNormalKTDL,
    downloadFileExcelChildrenFollowObjectKTDL,
    getDocHDSD,
} = require('../../controllers/Excel.controller')

Router.route('/sample').get(
    getExcelSample
)

Router.route('/HDSD').get(
    getDocHDSD
)

Router.route('/upload').post(
    isAuth,
    isRoleLocal,
    upload.single("file"),
    uploadFile,
)

Router.route('/download').get(
    isAuth,
    isRoleLocal,
    downloadFileExcel,
)

Router.route('/download/nchcdb').get(
    isAuth,
    isRoleLocal,
    downloadFileExcelNCHCDB,
)

Router.route('/download/hck').get(
    isAuth,
    isRoleLocal,
    downloadFileExcelHCK,
)

Router.route('/download/httg').get(
    isAuth,
    isRoleLocal,
    downloadFileExcelHTTG,
)

Router.route('/download/family').get(
    isAuth,
    isRoleLocal,
    downloadFileExcelFamily,
)

Router.route('/download/khaithacdulieu').get(
    isAuth,
    isRoleLocal,
    downloadFileExcelChildrenKTDL,
)

Router.route('/download/khaithacdulieu/binhthuong').get(
    isAuth,
    isRoleLocal,
    downloadFileExcelChildrenNormalKTDL,
)

Router.route('/download/khaithacdulieu/doituong').get(
    isAuth,
    isRoleLocal,
    downloadFileExcelChildrenFollowObjectKTDL,
)

module.exports = Router;