const Router = require('express').Router();

const {
    getData_HTTG_Controller, searchData_HTTG_Controller
} = require('../../controllers/HinhThucTroGiup.controller');
const {
    isRoleLocal
} = require('../../middlewares/Local.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')

/**
 * @api {get} /v1/api/hinh-thuc-tro-giup Get List Form Of Help
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/hinh-thuc-tro-giup
 * 
 * @apiName ListFormOfHelp
 * @apiGroup FormOfHelp
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.hinhthuctrogiup  result.hinhthuctrogiup Response result.hinhthuctrogiup
 * @apiSuccess {String} result.hinhthuctrogiup.name FormOfHelp name
 * @apiSuccess {String} result.hinhthuctrogiup.value FormOfHelp value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * 
 * @apiSuccessExample {json} success Response
 *  {
 *     "code": 200,
 *     "message": "responseMessage",
 *     "pageSize": 10,
 *     "pageNumber": 1,
 *     "total": 11,
 *     "result": [
 *         {
 *             "id_treem": 230069,
 *             "hoten": "Trần Thị Bê",
 *             "ngaysinh": "2019-12-31T17:00:00.000Z",
 *             "id_giadinh": 161662,
 *             "nguoinuoiduong": "",
 *             "hotencha": "Trần Văn Cê",
 *             "hotenme": "Nguyễn Thị A",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nữ",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "hinhthuctrogiup": [
 *                 {
 *                     "name": "1 Trợ giúp xã hội",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "2 Trợ giúp y tế",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "3 Trợ giúp giáo dục đào tạo và giáo dục nghề nghiệ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "4 Trợ giúp pháp lý, hỗ trợ tư vấn",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "5 Trợ giúp khác ",
 *                     "value": "false"
 *                 }
 *             ]
 *         },
 *   
 *           .
 *           .
 *           .
 *       ]
 *   }
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */





 /**
 * @api {get} /v1/api/hinh-thuc-tro-giup/search Search Form Of Help
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/hinh-thuc-tro-giup/search
 * 
 * @apiName SearchListFormOfHelp
 * @apiGroup FormOfHelp
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.id_thon Village unique ID
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.id_phuongxa Ward unique ID
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.id_quanhuyen District unique ID
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.id_tinhthanhpho City unique ID
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.hinhthuctrogiup  result.hinhthuctrogiup Response result.hinhthuctrogiup
 * @apiSuccess {String} result.hinhthuctrogiup.name FormOfHelp name
 * @apiSuccess {String} result.hinhthuctrogiup.value FormOfHelp value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * @apiParam {number} thanhpho City unique ID
 * @apiParam {number} huyen District unique ID
 * @apiParam {number} xa Ward unique ID
 * @apiParam {number} thon Village unique ID
 * @apiParam {String} tentreem  Children name
 * @apiParam {number} hinhthuc FormOfHelp unique ID
 * @apiParam {number} magiadinh Famity unique ID
 * @apiParam {date} timestart Search Start Time
 * @apiParam {date} timefinish Search End Time
 * 
 * 
 * 
 * @apiSuccessExample {json} success Response
*  {
 *     "code": 200,
 *     "message": "responseMessage",
 *     "pageSize": 10,
 *     "pageNumber": 1,
 *     "total": 1,
 *     "search": {
 *          "page": "1",
 *          "pagesize": "10",
 *          "thanhpho": "",
 *          "huyen": "",
 *          "xa": "",
 *          "thon": "",
 *          "tentreem": "Trần Thị Bê",
 *          "hinhthuc": "",
 *          "magiadinh": "",
 *          "timestart": "",
 *          "timefinish": ""
 *     },
 *     "result": [
 *         {
 *             "id_treem": 230069,
 *             "hoten": "Trần Thị Bê",
 *             "ngaysinh": "2019-12-31T17:00:00.000Z",
 *             "id_giadinh": 161662,
 *             "nguoinuoiduong": "",
 *             "hotencha": "Trần Văn Cê",
 *             "hotenme": "Nguyễn Thị A",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nữ",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "hinhthuctrogiup": [
 *                 {
 *                     "name": "1 Trợ giúp xã hội",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "2 Trợ giúp y tế",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "3 Trợ giúp giáo dục đào tạo và giáo dục nghề nghiệ",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "4 Trợ giúp pháp lý, hỗ trợ tư vấn",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "5 Trợ giúp khác ",
 *                     "value": "false"
 *                 }
 *             ]
 *          }
 *      ]
 *   }
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */
Router.route('/').get(
    isAuth,
    isRoleLocal,
    getData_HTTG_Controller
)

Router.route('/search').get(
    isAuth,
    isRoleLocal,
    searchData_HTTG_Controller
)



module.exports = Router;