const Router = require("express").Router();

const {
    getDataController, 
    getInfoChildrenWhenSearchController,
    getDetailChildrenController,
    AddChildrenWithFamilyController,
    AddChildrenOtherFamilyController,
    logicalDeleteChildrenController,
    physicalDeleteChildrenController,
    updateDataChildrenController,
    updateDataPersonController,
    addDataPersonController,
    logicalDeleteChildrenMultiController,
    getInfoPersonController,
    restoreChildrenMultiController,
} = require("../../controllers/QuanLyHoSoTreEm.controller");
const {
    isAuth
} = require("../../middlewares/Authentication.middleware")
const {
    isRoleLocal
} = require("../../middlewares/Local.middleware");
const { 
    isCheckIdChildrenBeLongTo,
    isCheckUserPhuongXa,
    isCheckRoleDeleteLogicalChildren,
    isCheckRoleDeletePhysicalChildren,
    isCheckRoleUpdateChildren
} = require("../../middlewares/QuanLyTreEm.middleware");

/**
 * @api {get} /v1/api/quan-ly-tre-em/all ListAllChildren
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/all
 * 
 * @apiName ListAllChildren
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 * 
 * 
 * 
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {Number} result.id_giadinh Family unique ID
 * @apiSuccess {String} result.hoten Fullname Children
 * @apiSuccess {Date} result.ngaysinh Date Children
 * @apiSuccess {String} result.hotencha Fullname Parent
 * @apiSuccess {String} result.gioitinh Gender
 * @apiSuccess {String} result.tenthon Name Thôn
 * @apiSuccess {String} result.ten_phuongxa Name Phường Xã
 * @apiSuccess {String} result.ten_quanhuyen Name Quận Huyện
 * @apiSuccess {String} result.ten_tinhthanhpho Name Tỉnh Thành Phố
 * 
 * @apiSuccessExample {json} Success-Response
 *      HTTP/1.1 200 OK
 *      {
 *          "code":200,
 *          "message":"responseMessage",
 *          "total":100,
 *          "result": [
 *              {
 *                  "id_giadinh":161662,
 *                  "hoten":"Nguyễn Đăng Khoa",
 *                  "ngaysinh":"2013-06-18T17:00:00.000Z",
 *                  "hotencha":"Nguyễn Thanh Toàn",
 *                  "dantoc":"Kinh",
 *                  "gioitinh":"Nam",
 *                  "tenthon":"Ấp Me",
 *                  "ten_phuongxa":"Thị trấn Tân Hiệp",
 *                  "ten_quanhuyen":"Huyện Châu Thành",
 *                  "ten_tinhthanhpho":"Tỉnh Tiền Giang"
 *              }
 *          ]
 *      }
 * 
 * @apiError (404 Not Found) API not found
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *          code:404,
 *          message:"API Not Found GET {endpoint}",
 *      }
 * 
 * 
 * 
 */


 /**
 * @api {get} /v1/api/quan-ly-tre-em/all ListAllChildren
 * @apiVersion 1.1.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/all
 * 
 * @apiName ListAllChildren
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 * 
 *
 * 
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {Number} result.id_giadinh Family unique ID
 * @apiSuccess {String} result.hoten Fullname Children
 * @apiSuccess {Date} result.ngaysinh Date Children
 * @apiSuccess {String} result.hotencha Fullname Parent
 * @apiSuccess {String} result.gioitinh Gender
 * @apiSuccess {String} result.tenthon Name Thôn
 * @apiSuccess {String} result.ten_phuongxa Name Phường Xã
 * @apiSuccess {String} result.ten_quanhuyen Name Quận Huyện
 * @apiSuccess {String} result.ten_tinhthanhpho Name Tỉnh Thành Phố
 * 
 * @apiSuccessExample {json} Success-Response
 *      HTTP/1.1 200 OK
 *      {
 *          "code":200,
 *          "message":"responseMessage",
 *          "total":100,
 *          "result":[
 *              {
 *                  "id_giadinh":161662,
 *                  "hoten":"Nguyễn Đăng Khoa",
 *                  "ngaysinh":"2013-06-18T17:00:00.000Z",
 *                  "hotencha":"Nguyễn Thanh Toàn",
 *                  "dantoc":"Kinh",
 *                  "gioitinh":"Nam",
 *                  "tenthon":"Ấp Me",
 *                  "ten_phuongxa":"Thị trấn Tân Hiệp",
 *                  "ten_quanhuyen":"Huyện Châu Thành",
 *                  "ten_tinhthanhpho":"Tỉnh Tiền Giang"
 *              }
 *          ]
 *      }
 * 
 * @apiError (404 Not Found) API Not Found
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *          code:404,
 *          message:"API Not Found GET {endpoint}",
 *      }
 * 
 * 
 * 
 */
Router.route("/all").get(
    isAuth,
    isRoleLocal,
    getDataController
)


/**
 * @api {get} /v1/api/quan-ly-tre-em/timkiem SearchChildren
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/timkiem
 * 
 * @apiName SearchChildren
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 * 
 * @apiParam {String} search number page of pagination. Default if not pass search then search=1 and not display on url
 * @apiParam {String} pageSize total result on a page
 * @apiParam {number} tinhthanhpho City
 * @apiParam {number} quanhuyen District
 * @apiParam {number} phuongxa Ward
 * @apiParam {number} thon Village
 * @apiParam {String} hoten Children name
 * @apiParam {String} nguoinuoi Caregiver cha/mẹ/nguoinuoiduong
 * @apiParam {number} id_giadinh Famity unique ID
 * @apiParam {number} thungrac Status delete or delete yet children get out list 
 * @apiParam {String} dantoc Nation
 * @apiParam {String} gioitinh Gender Nam/Nu/Khac
 * 
 * 
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} pageSize Response pageSize
 * @apiSuccess {Number} pageNumber Response pageNumber
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {Number} result.id_treem Childrem unique ID
 * @apiSuccess {Number} result.id_giadinh Family unique ID
 * @apiSuccess {String} result.hoten Fullname Children
 * @apiSuccess {Date} result.ngaysinh Date Children
 * @apiSuccess {String} result.hotencha Fullname Parent
 * @apiSuccess {String} result.hotenme Fullname Mother
 * @apiSuccess {String} result.nguoinuoiduong Fullname Người nuôi dưỡng
 * @apiSuccess {String} result.gioitinh Gender
 * @apiSuccess {String} result.dantoc Nation
 * @apiSuccess {String} result.tenthon Name Thôn
 * @apiSuccess {String} result.ten_phuongxa Name Phường Xã
 * @apiSuccess {String} result.ten_quanhuyen Name Quận Huyện
 * @apiSuccess {String} result.ten_tinhthanhpho Name Tỉnh Thành Phố
 * 
 * @apiSuccessExample {json} Success-Response
 *      HTTP/1.1 200 OK
 *      {
 *          "code":200,
 *          "message":"responseMessage",
 *          "pageSize":10,
 *          "pageNumber":1,
 *          "total":1
 *          "result":[
 *              {
 *                  "id_treem":""
 *                  "id_giadinh":161662,
 *                  "hoten":"Nguyễn Đăng Khoa",
 *                  "ngaysinh":"2013-06-18T17:00:00.000Z",
 *                  "hotencha":"Nguyễn Thanh Toàn",
 *                  "hotenme": "",
 *                  "nguoinuoiduong":""
 *                  "dantoc":"Kinh",
 *                  "gioitinh":"Nam",
 *                  "tenthon":"Ấp Me",
 *                  "ten_phuongxa":"Thị trấn Tân Hiệp",
 *                  "ten_quanhuyen":"Huyện Châu Thành",
 *                  "ten_tinhthanhpho":"Tỉnh Tiền Giang"
 *              }
 *          ]
 *      }
 * 
 * @apiError (404 Not Found) API not found
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *          code:404,
 *          message:"API Not Found GET {endpoint}",
 *      }
 * 
 * 
 * 
 */

 
Router.route("/timkiem").get(
    isAuth,
    isRoleLocal,
    getInfoChildrenWhenSearchController
)



/**
* @api {get} /v1/api/quan-ly-tre-em/detail/:id ViewDetailChildren
* @apiVersion 1.0.0
* @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/detail/:id
*
* @apiName ViewDetailChildren
* @apiGroup Quản Lý Hồ Sơ Trẻ Em 
* @apiHeader access-token
*
*
* @apiParam {Number} id Children unique
*
* @apiParamExample {json} Request-Example:
*     {
*       "id":161662
*     }
*
* @apiSuccess {Code} code Response code
* @apiSuccess {String} message Response message
* @apiSuccess {Object[]} InfoChildren Returned object
* @apiSuccess {Object[]} hinhThucTroGiup Returned object
* @apiSuccess {Object[]} HoanCanhDacBiet Returned object
* @apiSuccess {Object[]} NguyCoRoiVaoHCDB Returned object
* @apiSuccess {Object[]} HoanCanhKhac Returned object
* @apiSuccess {String} InfoChildren.ten_tinhthanhpho Name Tỉnh Thành Phố
* @apiSuccess {String} InfoChildren.ten_quanhuyen Name Quận Huyện
* @apiSuccess {String} InfoChildren.ten_phuongxa Name Phường Xã
* @apiSuccess {String} InfoChildren.tenthon Name Thôn Ấp
* @apiSuccess {Number} InfoChildren.id_giadinh Mã gia đình
* @apiSuccess {String} InfoChildren.hotencha Fulname Parent
* @apiSuccess {String} InfoChildren.hotenme Fulname Mother
* @apiSuccess {String} InfoChildren.nguoinuoiduong Fulname Người nuôi dưỡng
* @apiSuccess {String} InfoChildren.sodienthoai Number Phone
* @apiSuccess {String} InfoChildren.diachi Address
* @apiSuccess {Number} InfoChildren.hoancanh value=0 => Không thuộc diện nào, value=1 => Thuộc diện nghèo, value=2 => THuộc diện cận nghèo
* @apiSuccess {Number} InfoChildren.id_treem Mã trẻ em unique
* @apiSuccess {date} InfoChildren.ngaysinh Ngày sinh của trẻ em
* @apiSuccess {String} InfoChildren.gioitinh gender
* @apiSuccess {String} InfoChildren.hoten Fulname Children
* @apiSuccess {String} InfoChildren.dantoc Dân tộc
* @apiSuccess {String} InfoChildren.tinhtranghoctap Tình trạng học tập
* @apiSuccess {String} InfoChildren.lophoccaonhat Lớp học cao nhất
*
* @apiSuccess {Number} hinhThucTroGiup.id_hinhthuc Mã hình thức unique
* @apiSuccess {String} hinhThucTroGiup.tenhinhthuc Tên hình thức
* @apiSuccess {Number} hinhThucTroGiup.id_chahinhthuc Mã cha hình thức
* @apiSuccess {Boolean} hinhThucTroGiup.value Trạng thái hình thức value=True => trẻ em nhận được sự hỗ trợ từ hình thức trợ giúp đó value=False => ngược lại
* @apiSuccess {Object[]} hinhThucTroGiup.chilren Children of id_hinhthuc 
*
* @apiSuccess {Number} HoanCanhDacBiet.id_chitietloaihoancanh Mã chi tiết loại hoàn cảnh Thuộc diện Hoàn cảnh đặc biệt unique
* @apiSuccess {String} HoanCanhDacBiet.ten_hoancanh Tên hoàn cảnh
* @apiSuccess {Number} HoanCanhDacBiet.id_loaihoancanh Mã loại hoàn cảnh from bảng loaihoancanh
* @apiSuccess {Number} HoanCanhDacBiet.cha Mã cha chi tiết loai hoàn cảnh
* @apiSuccess {Boolean} HoanCanhDacBiet.value trạng thái chi tiết loại hoàn cảnh value=True => trẻ em thuộc hoàn cảnh nào đó, value=False => ngược lại
* @apiSuccess {Object[]} HoanCanhDacBiet.chilren Children of id_chitietloaihoancanh 
*
* @apiSuccess {Number} NguyCoRoiVaoHCDB.id_chitietloaihoancanh Mã chi tiết loại hoàn cảnh thuộc diện có nguy cơ rơi vào hoàn cảnh đặc biệt unique
* @apiSuccess {String} NguyCoRoiVaoHCDB.ten_hoancanh Tên hoàn cảnh
* @apiSuccess {Number} NguyCoRoiVaoHCDB.id_loaihoancanh Mã loại hoàn cảnh from bảng loaihoancanh
* @apiSuccess {Number} NguyCoRoiVaoHCDB.cha Mã cha chi tiết loai hoàn cảnh
* @apiSuccess {Boolean} NguyCoRoiVaoHCDB.value trạng thái chi tiết loại hoàn cảnh value=True => trẻ em thuộc hoàn cảnh nào đó, value=False => ngược lại
* @apiSuccess {Object[]} NguyCoRoiVaoHCDB.chilren Children of id_chitietloaihoancanh 
*
* @apiSuccess {Number} HoanCanhKhac.id_chitietloaihoancanh Mã chi tiết loại hoàn cảnh thuộc diện rơi vào hoàn cảnh khác unique
* @apiSuccess {String} HoanCanhKhac.ten_hoancanh Tên hoàn cảnh
* @apiSuccess {Number} HoanCanhKhac.id_loaihoancanh Mã loại hoàn cảnh from bảng loaihoancanh
* @apiSuccess {Number} HoanCanhKhac.cha Mã cha chi tiết loai hoàn cảnh
* @apiSuccess {Boolean} HoanCanhKhac.value trạng thái chi tiết loại hoàn cảnh value=True => trẻ em thuộc hoàn cảnh nào đó, value=False => ngược lại
* @apiSuccess {Object[]} HoanCanhKhac.chilren Children of id_chitietloaihoancanh 
* 
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
* {
    "code":200,
    "message":"responseMessage",
    "InfoChildren":[
        {
            "ten_tinhthanhpho":"Tỉnh Tiền Giang",
            "ten_quanhuyen":"Huyện Châu Thành",
            "ten_phuongxa":"Thị trấn Tân Hiệp",
            "tenthon":"Ấp Me",
            "id_giadinh": 161663,
            "hotencha":"",
            "hotenme":"Đoàn Thị Ngoan",
            "nguoinuoiduong":"",
            "sodienthoai":"0375376508",
            "diachi":"Ấp Me - Thị trấn Tân Hiệp - Huyện Châu Thành - Tỉnh Tiền Giang",
            "hoancanh": 0,
            "id_treem": 230071,
            "ngaysinh":"2009-09-27T17:00:00.000Z",
            "gioitinh":"Nam",
            "hoten":"Hoàng Minh Tú",
            "dantoc":"Kinh",
            "tinhtranghoctap":"Ngừng học",
            "lophoccaonhat":"5"
        }
    ],
    "hinhThucTroGiup": [
        {
            "id_hinhthuc": 310,
            "tenhinhthuc":"1 Trợ giúp xã hội",
            "chahinhthuc": 0,
            "value": true,
            "children": [
                {
                    "id_hinhthuc":315,
                    "tenhinhthuc":"1.1 Trợ giúp thường xuyên tại cộng đồng ",
                    "chahinhthuc":310,
                    "value":true,
                    "children": [
                        {
                            "id_hinhthuc": 317,
                            "tenhinhthuc":"1.1.1 Trợ giúp tại cộng đồng ",
                            "chahinhthuc": 315,
                            "value": true,
                            "children": []
                        },
                        {
                            "id_hinhthuc":318,
                            "tenhinhthuc":"1.1.2 Nuôi dưỡng tập chung tại các trung tâm / cơ ",
                            "chahinhthuc":315,
                            "value":true,
                            "children":[]
                        }
                    ]
                },
                {
                    "id_hinhthuc":316,
                    "tenhinhthuc":"1.2 Trợ giúp đột xuất ",
                    "chahinhthuc":310,
                    "value": true,
                    "children": []
                }
                {...}
            ],
        "HoanCanhDacBiet": [
        {
            "id_chitietloaihoancanh": 286,
            "ten_hoancanh":"1 Trẻ em mồ côi cả cha và mẹ ",
            "cha": 0,
            "id_loaihoancanh": 165,
            "value": false,
            "children": [
                {
                    "id_chitietloaihoancanh": 331,
                    "ten_hoancanh":"1.1 Không có người chăm sóc",
                    "cha": 286,
                    "id_loaihoancanh": 165,
                    "value": false,
                    "children": []
                },
                {
                    "id_chitietloaihoancanh": 332,
                    "ten_hoancanh":"1.2 Được nuôi dưỡng trong cơ sở cung cấp dịch vụ bảo vệ trẻ em hoặc cơ sở trợ giúp xã hội ",
                    "cha": 286,
                    "id_loaihoancanh": 165,
                    "value": false,
                    "children": []
                },
                {
                    "id_chitietloaihoancanh": 333,
                    "ten_hoancanh":"1.3 Sống với người thân tích",
                    "cha": 286,
                    "id_loaihoancanh": 165,
                    "value": false,
                    "children": []
                },
                {
                    "id_chitietloaihoancanh": 334,
                    "ten_hoancanh":"1.4 Được nhận chăm sóc thay thế bởi cá nhân, gia đình không phải người thân thích, trừ trường hợp nhận làm con nuôi ",
                    "cha": 286,
                    "id_loaihoancanh": 165,
                    "value": false,
                    "children": []
                }
            ]
        },
        {...}
    ],
    "NguyCoRoiVaoHCDB":[
        "similar: HoanCanhDacBiet"
    ],
    "HoanCanhKhac":[
        "similar: HoanCanhDacBiet"
    ]
  }
*
* @apiError (404 Not Found) API not found
*
* @apiErrorExample {json} 404-Response:
*     HTTP/1.1 404 Not Found
*     {
*       code:404
*       message: "API Not Found GET {endpoint}"
*     }
*
*/
Router.route("/detail/:id").get(
    isAuth,
    isRoleLocal,
    isCheckIdChildrenBeLongTo,
    getDetailChildrenController
)

/**
 * @api {post} /v1/api/quan-ly-tre-em/addwithfamily Add Children With Family
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/addwithfamily
 *
 * 
 * @apiExample Example usage:
 *          body:
 *          {
 *              "id_giadinh":161662
 *              "id_thon":22451
 *              "hoten":"Nguyễn Sang"
 *              "ngaysinh":"1998-11-20"
 *              "gioitinh":"Nam"
 *              "dantoc":"Kinh"
 *              "lophoccaonhat":"5"
 *              "tinhtranghoctap":"Nghỉ"
 *              "ghichu":"Tốt"
 *              "id_hoancanh":["345","346"]
 *              "id_trogiup":["315", "316"]
 *          }
 * 
 * @apiName AddChildrenWithFamily
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 * 
 * 
 * @apiParam {Number} id_giadinh Mã Gia Đình
 * @apiParam {Number} id_thon Village
 * @apiParam {String} hoten Fullname Children
 * @apiParam {String} ngaysinh Date of birth
 * @apiParam {String} gioitinh Gender
 * @apiParam {String} dantoc nation
 * @apiParam {String} lophoccaonhat highest class
 * @apiParam {String} tinhtranghoctap academic status
 * @apiParam {String} ghichu note 
 * @apiParam {Array} id_hoancanh Mã Hoàn Cảnh
 * @apiParam {Array} id_trogiup Mã Trợ Giúp
 * 
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {String} result Response result
 * 
 * @apiSuccessExample {json} Success-Response
 *      HTTP/1.1 200 OK
 *      {
 *          "code":200,
 *          "message":"responseMessage",
 *          "result":"Add Success"
 *      }
 * 
 * @apiError (404 Not Found) API not found
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *          code:404,
 *          message:"API Not Found GET {endpoint}",
 *      }
 * 
 * 
 * 
 */

Router.route("/addwithfamily").post(
    isAuth,
    isRoleLocal,
    // isCheckUserPhuongXa,
    AddChildrenWithFamilyController
)



/**
 * @api {post} /v1/api/quan-ly-tre-em/addotherfamily Add Children Other Family
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/addotherfamily
 * 
 * @apiExample Example usage:
 *          body:
 *          {
 *              "hotencha":"Huỳnh Chí Khang"
 *              "hotenme":"Bành Thị Hồng"
 *              "nguoinuoiduong":"PHan Thị Hồng Gấm"
 *              "sodienthoai":"0967458622"
 *              "hoancanh":1
 *              "diachi":"Ấp Me - Thị Trấn Tân Hiệp - Huyện Châu Thành - Tỉnh Tiền Giang"
 *              "id_thon":22451
 *              "hoten":"Nguyễn Sang"
 *              "ngaysinh":"1998-11-20"
 *              "gioitinh":"Nam"
 *              "dantoc":"Kinh"
 *              "lophoccaonhat":"5"
 *              "tinhtranghoctap":"Nghỉ"
 *              "ghichu":"Tốt"
 *              "id_hoancanh":["345","346"]
 *              "id_trogiup":["315", "316"]
 *          }
 * 
 * @apiName AddChildrenOtherFamily
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 * 
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {String} result Response result
 * 
 * @apiParam {String} hotencha Fullname Parent
 * @apiParam {String} hotenme Fullname Mother
 * @apiParam {String} nguoinuoiduong Fullname take care of
 * @apiParam {String} sodienthoai Phone Number
 * @apiParam {Number} hoancanh Hoàn Cảnh
 * @apiParam {String} diachi Address
 * @apiParam {Number} id_thon Village
 * @apiParam {String} hoten Fullname Children
 * @apiParam {String} ngaysinh Date of birth
 * @apiParam {String} gioitinh Gender
 * @apiParam {String} dantoc nation
 * @apiParam {String} lophoccaonhat highest class
 * @apiParam {String} tinhtranghoctap academic status
 * @apiParam {String} ghichu note 
 * @apiParam {Array} id_hoancanh Mã Hoàn Cảnh
 * @apiParam {Array} id_trogiup Mã Trợ Giúp
 * 
 * 
 * @apiSuccessExample {json} Success-Response
 *      HTTP/1.1 200 OK
 *      {
 *          "code":200,
 *          "message":"responseMessage",
 *          "result":"Add Success"
 *      }
 * 
 * @apiError (404 Not Found) API not found
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *          code:404,
 *          message:"API Not Found GET {endpoint}",
 *      }
 * 
 * 
 * 
 */

Router.route("/addotherfamily").post(
    isAuth,
    isRoleLocal,
    isCheckUserPhuongXa,
    AddChildrenOtherFamilyController
)



/**
 * @api {put} /v1/api/quan-ly-tre-em/xoa/:id_treem put Children In Trash
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/xoa/:id_treem
 * 
 * @apiName putChildrenInTrash
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 * 
 * 
 * @apiParam {Number} id_treem Children unique
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "id_treem": 406074
 *     }
 * 
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Object} result Response result
 * @apiSuccess {Number} fieldCount Response fieldCount
 * @apiSuccess {Number} affectedRows Response affectedRows
 * @apiSuccess {Number} insertId Response insertId
 * @apiSuccess {String} info Response info
 * @apiSuccess {Number} serverStatus Response serverStatus
 * @apiSuccess {Number} warningStatus Response warningStatus
 * @apiSuccess {Number} changedRows Response changedRows
 * 
 * @apiSuccessExample {json} Success-Response
 *      HTTP/1.1 200 OK
 *      {
 *          "code":200,
 *          "message":"responseMessage",
 *          "result": {
 *              "fieldCount": 0,
 *              "affectedRows": 1,
 *              "insertId": 0,
 *              "info":"Rows matched: 1  Changed: 1  Warnings: 0",
 *              "serverStatus": 2,
 *              "warningStatus": 0,
 *              "changedRows": 1
 *           }
 *      }
 * 
 * @apiError (404 Not Found) API not found
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *          code:404,
 *          message:"API Not Found GET {endpoint}",
 *      }
 * 
 * 
 * 
 */

Router.route("/xoa/:id_treem").put(
    isAuth,
    isRoleLocal,
    isCheckRoleDeleteLogicalChildren,
    logicalDeleteChildrenController
)

/**
 * @api {delete} /v1/api/quan-ly-tre-em/xoaf/:id_treem Delete Children From Trash
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/xoaf/:id_treem
 * 
 * @apiName DeleteChildrenFromTrash
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 * 
 * 
 * @apiParam {Number} id_treem Children unique
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "id_treem": 406074
 *     }
 * 
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Object} result Response result
 * @apiSuccess {Number} fieldCount Response fieldCount
 * @apiSuccess {Number} affectedRows Response affectedRows
 * @apiSuccess {Number} insertId Response insertId
 * @apiSuccess {String} info Response info
 * @apiSuccess {Number} serverStatus Response serverStatus
 * @apiSuccess {Number} warningStatus Response warningStatus
 * 
 * @apiSuccessExample {json} Success-Response
 *      HTTP/1.1 200 OK
 *      {
 *          "code":200,
 *          "message":"responseMessage",
 *          "result": {
 *              "fieldCount": 0,
 *              "affectedRows": 1,
 *              "insertId": 0,
 *              "info":"",
 *              "serverStatus": 2,
 *              "warningStatus": 0
 *           }
 *      }
 * 
 * @apiError (404 Not Found) API not found
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *          code:404,
 *          message:"API Not Found GET {endpoint}",
 *      }
 * 
 * 
 * 
 */

Router.route("/xoaf/:id_treem").delete(
    isAuth,
    isRoleLocal,
    isCheckRoleDeletePhysicalChildren,
    physicalDeleteChildrenController
)


/**
 * @api {put} /v1/api/quan-ly-tre-em/update/:id Update Info Children
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/quan-ly-tre-em/update/:id
 *
 * @apiName UpdateInfoChildren
 * @apiGroup Quản Lý Hồ Sơ Trẻ Em
 * @apiHeader access-token
 *
 *  @apiExample Example usage:
 *          endpoint: http://localhost:5000/v1/api/quan-ly-tre-em/update/230069
 *          body:
 *          {
 *              "hotencha":"Nguyễn Thị A",
 *              "hotenme":"Trần Văn Cê",
 *              "nguoinuoiduong":"",
 *              "sodienthoai":"0967458622",
 *              "hoancanh":1,
 *              "diachi":"Ấp Me - Thị Trấn Tân Hiệp - Huyện Châu Thành - Tỉnh Tiền Giang",
 *              "id_thon":22451,
 *              "hoten":"Trần Thị Bê",
 *              "ngaysinh":"2020-01-01",
 *              "gioitinh":"Nữ",
 *              "dantoc":"Kinh",
 *              "lophoccaonhat":"0",
 *              "tinhtranghoctap":"0",
 *              "ghichu":"Đã Sửa",
 *              "id_hoancanh":["345", "346"],
 *              "id_trogiup":["315", "316"]
 *          }
 * 
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       "id": 161662
 *     }
 *
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {Number} result.fieldCount Response field count
 * @apiSuccess {Number} result.affectedRows Response affected rows
 * @apiSuccess {Number} result.insertId Response insert id
 * @apiSuccess {String} result.info Response info
 * @apiSuccess {Number} result.serverStatus Response server status
 * @apiSuccess {Number} result.warningStatus Response warning status
 * @apiSuccess {Number} result.changedRows Response changed rows
 * 
 * 
 * @apiParam (params) {Number} id Children unique
 * @apiParam (body) {String} hotencha Fullname Parent
 * @apiParam (body) {String} hotenme Fullname Mother
 * @apiParam (body) {String} nguoinuoiduong Fullname take care of
 * @apiParam (body) {String} sodienthoai Phone Number
 * @apiParam (body) {Number} hoancanh Hoàn Cảnh
 * @apiParam (body) {String} diachi Address
 * @apiParam (body) {Number} idthon Village
 * @apiParam (body) {String} hoten Fullname Children
 * @apiParam (body) {String} ngaysinh Date of birth
 * @apiParam (body) {String} gioitinh Gender
 * @apiParam (body) {String} dantoc nation
 * @apiParam (body) {String} lophoccaonhat highest class
 * @apiParam (body) {String} tinhtranghoctap academic status
 * @apiParam (body) {String} ghichu note 
 * @apiParam (body) {Array} id_hoancanh Mã Hoàn Cảnh
 * @apiParam (body) {Array} id_trogiup Mã Trợ Giúp
 * @apiSuccessExample {json} Success-Response:
 *  {
 *      "code":200,
 *      "message":"Update successful",
 *      "result": {
 *          "fieldCount": 0,
 *          "affectedRows": 3,
 *          "insertId": 0,
 *          "info":"Rows matched: 3  Changed: 0  Warnings: 0",
 *          "serverStatus": 34,
 *          "warningStatus": 0,
 *          "changedRows": 0
 *      }
 *  }
 * @apiError (404 Not Found) API not found
 * @apiErrorExample {json} 404-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       code: "404"
 *       message: "API Not Found GET {endpoint}"
 *     }
 *
 */

Router.route("/update/:id").put(
    isAuth,
    isRoleLocal,
    isCheckRoleUpdateChildren,
    updateDataChildrenController
)

Router.route("/update/person/:id").put(
    isAuth,
    isRoleLocal,
    // isCheckRoleUpdateChildren,
    updateDataPersonController
)

Router.route("/add/person").post(
    isAuth,
    isRoleLocal,
    // isCheckRoleUpdateChildren,
    addDataPersonController,
)

Router.route("/xoa-multi").post(
    isAuth,
    isRoleLocal,
    // isCheckRoleDeleteLogicalChildren,
    logicalDeleteChildrenMultiController
)

Router.route("/khoiphuc-multi").post(
    isAuth,
    isRoleLocal,
    // isCheckRoleDeleteLogicalChildren,
    restoreChildrenMultiController,
)

Router.route("/info/person/:id").get(
    isAuth,
    isRoleLocal,
    getInfoPersonController
)

module.exports = Router;