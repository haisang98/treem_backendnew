const Router = require('express').Router();

const {
    getData_HCK_Controller, searchData_HCK_Controller
} = require('../../controllers/HoanCanhKhac.controller');

const {
    isRoleLocal
} = require('../../middlewares/Local.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')

/**
 * @api {get} /v1/api/tre-em-hoan-canh-khac Get List Other Circumstances
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/tre-em-hoan-canh-khac
 * 
 * @apiName ListOtherCircumstances
 * @apiGroup OtherCircumstances
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} pageSize Response Page Size
 * @apiSuccess {Number} pageNumber Response Page Number
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.hoancanhkhac  result.hoancanhkhac Response result.hoancanhkhac
 * @apiSuccess {String} result.hoancanhkhac.name OtherCircumstances name
 * @apiSuccess {String} result.hoancanhkhac.value OtherCircumstances value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * 
 * @apiSuccessExample {json} success Response
 *  {
 *     "code": 200,
 *     "message": "responseMessage",
 *     "pageSize": 20,
 *     "pageNumber": 1,
 *     "total": 3,
 *     "result": [
 *         {
 *             "id_treem": 230097,
 *             "hoten": "Dương Tấn Đạt",
 *             "ngaysinh": "2016-08-15T17:00:00.000Z",
 *             "id_giadinh": 161684,
 *             "nguoinuoiduong": "",
 *             "hotencha": "",
 *             "hotenme": "Trần Thị Phú",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nữ",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "id_chitietloaihoancanh": 302,
 *             "hoancanhkhac": [
 *                 {
 *                     "name": "1 Trẻ em bị bắt cóc, chiếm đoạt, đánh tráo",
 *                     "value": "true"
 *                 },
 *                 {
 *                     "name": "2 Trẻ em bị tai nạn thương tích",
 *                     "value": "false"
 *                 }
 *             ]
 *         },
 *         {
 *             "id_treem": 230109,
 *             "hoten": "Hồ Lâm Tấn Phát",
 *             "ngaysinh": "2011-09-09T17:00:00.000Z",
 *             "id_giadinh": 161693,
 *             "nguoinuoiduong": "",
 *             "hotencha": "",
 *             "hotenme": "Lâm Mỹ Cẩm Trang",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nam",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "id_chitietloaihoancanh": 401,
 *             "hoancanhkhac": [
 *                 {
 *                     "name": "1 Trẻ em bị bắt cóc, chiếm đoạt, đánh tráo",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "2 Trẻ em bị tai nạn thương tích",
 *                     "value": "true"
 *                 }
 *             ]
 *         },
 *         {
 *             "id_treem": 230140,
 *             "hoten": "Lê Ngọc Tường Lam",
 *             "ngaysinh": "2014-05-20T17:00:00.000Z",
 *             "id_giadinh": 161716,
 *             "nguoinuoiduong": "",
 *             "hotencha": "Lê Phước Trọng",
 *             "hotenme": "",
 *             "dantoc": "Kinh",
 *             "gioitinh": "Nữ",
 *             "tenthon": "Ấp Me",
 *             "ten_phuongxa": "Thị trấn Tân Hiệp",
 *             "ten_quanhuyen": "Huyện Châu Thành",
 *             "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *             "id_chitietloaihoancanh": 400,
 *             "hoancanhkhac": [
 *                 {
 *                     "name": "1 Trẻ em bị bắt cóc, chiếm đoạt, đánh tráo",
 *                     "value": "false"
 *                 },
 *                 {
 *                     "name": "2 Trẻ em bị tai nạn thương tích",
 *                     "value": "true"
 *                 }
 *             ]
 *         }
 *     ]
 *  }
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */





 /**
 * @api {get} /v1/api/tre-em-hoan-canh-khac/search  Search List Other Circumstances
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/tre-em-hoan-canh-khac/search
 * 
 * @apiName SearchListOtherCircumstances
 * @apiGroup OtherCircumstances
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Object[]} search Response search
 * @apiSuccess {Number} search.page Response Page Number
 * @apiSuccess {Number} search.thanhpho Response city name
 * @apiSuccess {String} search.xa Village unique ID
 * @apiSuccess {String} search.thon Ward unique ID
 * @apiSuccess {String} search.huyen District unique ID
 * @apiSuccess {String} search.thanhpho City unique ID
 * @apiSuccess {String} search.hoancanh Circumstances unique ID
 * @apiSuccess {String} search.tentreem  Children name
 * @apiSuccess {number} search.magiadinh Famity unique ID
 * @apiSuccess {date} search.timestart Search Start Time
 * @apiSuccess {date} search.timefinish Search End Time
 * 
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Family unique ID
 * @apiSuccess {String} result.hoten Children name
 * @apiSuccess {Date} result.ngaysinh Children date of birth
 * @apiSuccess {String} result.id_giadinh Famity unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.dantoc Nation names for children
 * @apiSuccess {String} result.gioitinh gender of children
 * @apiSuccess {String} result.id_thon Village unique ID
 * @apiSuccess {String} result.tenthon Village name
 * @apiSuccess {String} result.id_phuongxa Ward unique ID
 * @apiSuccess {String} result.phuongxa Ward name
 * @apiSuccess {String} result.id_quanhuyen District unique ID
 * @apiSuccess {String} result.quanhuyen District name
 * @apiSuccess {String} result.id_tinhthanhpho City unique ID
 * @apiSuccess {String} result.tinhthanhpho City name
 * @apiSuccess {Objects[]} result.hoancanhkhac  result.hoancanhkhac Response result.hoancanhkhac
 * @apiSuccess {String} result.hoancanhkhac.name OtherCircumstances name
 * @apiSuccess {String} result.hoancanhkhac.value OtherCircumstances value
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * @apiParam {number} thon Village unique ID
 * @apiParam {String} tentreem  Children name
 * @apiParam {number} hoancanh Other Circumstances unique ID
 * @apiParam {number} magiadinh Famity unique ID
 * @apiParam {date} timestart Search Start Time
 * @apiParam {date} timefinish Search End Time
 * 
 * @apiSuccessExample {json} success Response
 *  {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "pageSize": 20,
 *      "pageNumber": 1,
 *      "total": 1,
 *      "search": {
 *          "page": "",
 *          "pagesize": "",
 *          "thanhpho": "",
 *          "huyen": "",
 *          "xa": "",
 *          "thon": "",
 *          "tentreem": "Dương Tấn Đạt",
 *          "hinhthuc": "",
 *          "magiadinh": "",
 *          "timestart": "",
 *          "timefinish": ""
 *      },
 *      "result": [
 *          {
 *              "id_treem": 230097,
 *              "hoten": "Dương Tấn Đạt",
 *              "ngaysinh": "2016-08-15T17:00:00.000Z",
 *              "id_giadinh": 161684,
 *              "nguoinuoiduong": "",
 *              "hotencha": "",
 *              "hotenme": "Trần Thị Phú",
 *              "dantoc": "Kinh",
 *              "gioitinh": "Nữ",
 *              "tenthon": "Ấp Me",
 *              "id_thon": 22451,
 *              "ten_phuongxa": "Thị trấn Tân Hiệp",
 *              "id_phuongxa": 15123,
 *              "ten_quanhuyen": "Huyện Châu Thành",
 *              "id_quanhuyen": 665,
 *              "ten_tinhthanhpho": "Tỉnh Tiền Giang",
 *              "id_tinhthanhpho": 60,
 *              "id_chitietloaihoancanh": 302,
 *              "hoancanhkhac": [
 *                  {
 *                      "name": "1 Trẻ em bị bắt cóc, chiếm đoạt, đánh tráo",
 *                      "value": "true"
 *                  },
 *                  {
 *                      "name": "2 Trẻ em bị tai nạn thương tích",
 *                      "value": "false"
 *                  }
 *              ]
 *          }
 *      ]
 *  }
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */
Router.route('/').get(
    isAuth,
    isRoleLocal,
    getData_HCK_Controller
)

Router.route('/search').get(
    isAuth,
    isRoleLocal,
    searchData_HCK_Controller
)

//  * @apiParam {number} thanhpho City unique ID
//  * @apiParam {number} huyen District unique ID
//  * @apiParam {number} xa Ward unique ID


module.exports = Router;