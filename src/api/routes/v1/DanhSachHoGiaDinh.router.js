const Router = require('express').Router();

const {
    getData_HoGiaDinh_Controller,
    search_HoGiaDinh_Controller,
    searchFatherController,
    searchMotherController,
    searchNguoiNuoiController,
} = require('../../controllers/DanhSachHoGiaDinh.controller');
const {
    isRoleLocal
} = require('../../middlewares/Local.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')

/**
 * @api {get} /v1/api/danh-sach-ho-gia-dinh Get List Family

 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/danh-sach-ho-gia-dinh
 * 
 * @apiName  GetListFamily
 * @apiGroup  ListFamily
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Number} pageSize Response Page size
 * @apiSuccess {Number} pageNumber Response Page number
 * @apiSuccess {Object[]} result Response result
 * 
 * @apiSuccess {String} result.id_treem Children unique ID
 * @apiSuccess {String} result.id_giadinh Family unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.diachi Address
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * 
 * @apiSuccessExample {json} success Response
 * 
 *  {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "pageSize": 20,
 *      "pageNumber": 1,
 *      "total": 98,
 *      "result": [
 *          {
 *              "id_treem": 230069,
 *              "id_giadinh": 161662,
 *              "nguoinuoiduong": "",
 *              "hotencha": "Trần Văn Cê",
 *              "hotenme": "Nguyễn Thị A",
 *              "diachi": "Ấp Me - Thị trấn Tân Hiệp - Huyện Châu Thành - Tỉnh Tiền Giang"
 *          },
 *          .
 *          .
 *          .
 *      ]
 *  }
 *  
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *          message: "API Not Found GET (endpoint)",
 *      }
 * 
 */


 /**
 * @api {get} /v1/api/danh-sach-ho-gia-dinh/search Search List Family

 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/danh-sach-ho-gia-dinh/search
 * 
 * @apiName  SearchListFamily
 * @apiGroup  ListFamily
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Number} pageSize Response Page size
 * @apiSuccess {Number} pageNumber Response Page number
 * @apiSuccess {Object[]} search Response search
 * @apiSuccess {number} search.page Page Number'
 * @apiSuccess {number} search.pagesize Page Size Number
 * @apiSuccess {number} search.tenchame Parent name
 * @apiSuccess {number} search.thon Village unique ID
 * @apiSuccess {number} search.magiadinh Family unique ID
 * @apiSuccess {number} search.tentreem Children Name
 * @apiSuccess {number} search.xa Ward unique ID
 * @apiSuccess {number} search.huyen District unique ID
 * @apiSuccess {number} search.thanhpho city unique ID
 * 
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.id_treem Children unique ID
 * @apiSuccess {String} result.id_giadinh Family unique ID
 * @apiSuccess {String} result.nguoinuoiduong Fosterer name
 * @apiSuccess {String} result.hotencha Dad name
 * @apiSuccess {String} result.hotenme Mom name
 * @apiSuccess {String} result.diachi Address
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * @apiParam {String} tenchame Parents Name
 * @apiParam {Number} thon Village unique ID
 * @apiParam {number} magiadinh Family unique ID
 * @apiParam {String} tentreem Children Name
 * 
 * @apiSuccessExample {json} success Response
 * 
 *   {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "pageSize": 10,
 *      "pageNumber": 1,
 *      "total": 1,
 *      "search": {
 *          "page": "1",
 *          "pagesize": "10",
 *          "tenchame": "Nguyễn Thanh Sang",
 *          "thon": "22451",
 *          "magiadinh": "161667",
 *          "tentreem": "230075",
 *          "xa": 15123,
 *          "huyen": 665,
 *          "thanhpho": 60
 *      },
 *      "result": [
 *          {
 *              "id_treem": 230075,
 *              "id_giadinh": 161667,
 *              "nguoinuoiduong": "",
 *              "hotencha": "Nguyễn Thanh Sang",
 *              "hotenme": "",
 *              "diachi": "Ấp Me - Thị trấn Tân Hiệp - Huyện Châu Thành - Tỉnh Tiền Giang",
 *              "hoten": "Nguyễn Trúc Lam",
 *              "id_tinhthanhpho": 60,
 *              "id_quanhuyen": 665,
 *              "id_phuongxa": 15123,
 *              "id_thon": 22451
 *          }
 *      ]
 *  }
 *  
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *          message: "API Not Found GET (endpoint)",
 *      }
 * 
 */

Router.route('/').get(
    isAuth,
    isRoleLocal,
    getData_HoGiaDinh_Controller
)

Router.route('/search').get(
    isAuth,
    isRoleLocal,
    search_HoGiaDinh_Controller
)

Router.route('/search/father').get(
    isAuth,
    isRoleLocal,
    searchFatherController
)

Router.route('/search/mother').get(
    isAuth,
    isRoleLocal,
    searchMotherController
)

Router.route('/search/nguoinuoi').get(
    isAuth,
    isRoleLocal,
    searchNguoiNuoiController
)

module.exports = Router;