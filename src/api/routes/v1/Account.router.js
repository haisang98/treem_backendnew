const Router = require('express').Router();

const {
    getAccountController,
    updateAccountController,
    updatePasswordAccountController
} = require('../../controllers/Account.controller');
const {
    isRoleManagerAccount
} = require('../../middlewares/Account.middleware')
const {
    isAuth
} = require('../../middlewares/Authentication.middleware')

Router.route('/').get(
    isAuth,
    isRoleManagerAccount,
    getAccountController
)

/**
 * @api {get} /v1/api/account Get Account

 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/account
 * 
 * @apiName  Get Account
 * @apiGroup  Account
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Number} total Response total
 * @apiSuccess {Number} pageSize Response Page size
 * @apiSuccess {Number} pageNumber Response Page number
 * 
 * @apiSuccess {Object[]} search Response search
 * @apiSuccess {number} search.page Page Number
 * @apiSuccess {number} search.pagesize Page Size Number
 * @apiSuccess {number} search.id_tinhthanhpho city unique ID
 * @apiSuccess {number} search.id_quanhuyen District unique ID
 * @apiSuccess {number} search.id_phuongxa Ward unique ID
 * @apiSuccess {String} search.tentaikhoan User Name 
 * @apiSuccess {String} search.tenhienthi View Name
 * 
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {String} result.tentaikhoan User name
 * @apiSuccess {String} result.tenhienthi View name
 * @apiSuccess {String} result.email Email
 * @apiSuccess {Number} result.id_phuongxa Ward unique ID
 * @apiSuccess {Number} result.id_quanhuyen District unique ID
 * @apiSuccess {Number} result.id_tinhthanhpho city unique ID
 * 
 * @apiParam {number} page Page Number
 * @apiParam {number} pagesize Page Size Number
 * @apiParam {number} id_quanhuyen District unique ID
 * @apiParam {number} id_phuongxa Ward unique ID
 * @apiParam {String} tentaikhoan User Name 
 * @apiParam {String} tenhienthi Page View Name
 * 
 * 
 * @apiSuccessExample {json} success Response
 * 
 *  {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "pageSize": 10,
 *      "pageNumber": 1,
 *      "total": 1,
 *      "search": {
 *          "id_tinhthanhpho": 60,
 *          "id_quanhuyen": "665",
 *          "id_phuongxa": "15123",
 *          "tentaikhoan": "tg_ct_1",
 *          "tenhienthi": "Tân Hiệp"
 *      },
 *      "result": [
 *          {
 *              "tentaikhoan": "tg_ct_1",
 *              "tenhienthi": "Tân Hiệp",
 *              "email": "tanhiep_ct_tg@gmail.com",
 *              "id_phuongxa": 15123,
 *              "id_quanhuyen": 665,
 *              "id_tinhthanhpho": 60
 *          }
 *      ]
 *  }
 *  
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *          message: "API Not Found GET (endpoint)",
 *      }
 * 
 */


Router.route('/update/:id').put(
    isAuth,
    isRoleManagerAccount,
    updateAccountController
)

Router.route('/update/password/:id').put(
    isAuth,
    isRoleManagerAccount,
    updatePasswordAccountController
)

/**
 * @api {put} /v1/api/account/update/:id Update Account

 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/account/update/:id
 * 
 * @apiName  UpdateAccount
 * @apiGroup  Account
 * @apiHeader access-token
 * @apiSuccess {Code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {Number} result.fieldCount Response field count
 * @apiSuccess {Number} result.affectedRows Response affected rows
 * @apiSuccess {Number} result.insertId Response insert id
 * @apiSuccess {String} result.info Response info
 * @apiSuccess {Number} result.serverStatus Response server status
 * @apiSuccess {Number} result.warningStatus Response warning status
 * @apiSuccess {Number} result.changedRows Response changed rows
 * 
 * 
 * {
    "huyen": 665,
    "tentaikhoan": "A2",
    "tenhienthi": "A2",
    "email": "A2@gmail.com",
    "dakhoa": 0
}
 * @apiParam (params) {Number} id Account unique
 * @apiParam (body) {String} huyen Huyen
 * @apiParam (body) {String} xa Xa
 * @apiParam (body) {String} tentaikhoan User name
 * @apiParam (body) {String} tenhienthi View Name
 * @apiParam (body) {String} email Email
 * @apiParam (body) {Number} dakhoa block Account
 * 
 * 
 * @apiSuccessExample {json} success Response
 * 
 *  {
 *      "code": 200,
 *      "message": "Update successful",
 *      "result": {
 *          "fieldCount": 0,
 *          "affectedRows": 0,
 *          "insertId": 0,
 *          "info": "Rows matched: 0  Changed: 0  Warnings: 0",
 *          "serverStatus": 2,
 *          "warningStatus": 0,
 *          "changedRows": 0
 *      }
 *  }
 *  
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *          message: "API Not Found GET (endpoint)",
 *      }
 * 
 */


module.exports = Router;