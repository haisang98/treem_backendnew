const Router = require('express').Router();

const {
    getData_DanhMuc_Controller
} = require('../../controllers/DanhMuc.controller');

/**
 * @api {get} /v1/api/danh-muc Get Category
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/danh-muc
 * 
 * @apiName GetCategory
 * @apiGroup Category
 * @apiHeader access-token
 * @apiSuccess {code} code Response code
 * @apiSuccess {String} message Response message
 * @apiSuccess {Object[]} result Response result
 * @apiSuccess {Object[]} result.DANH_MUC_HOAN_CANH_DAC_BIET Response DANH_MUC_HOAN_CANH_DAC_BIET
 * @apiSuccess {String} result.DANH_MUC_HOAN_CANH_DAC_BIET.MA_CHI_TIET_HOAN_CANH Response MA_CHI_TIET_HOAN_CANH
 * @apiSuccess {String} result.DANH_MUC_HOAN_CANH_DAC_BIET.TEN_CHI_TIET_HOAN_CANH Response TEN_CHI_TIET_HOAN_CANH
 * @apiSuccess {String} result.DANH_MUC_HOAN_CANH_DAC_BIET.ID_CHA Response ID_CHA
 * @apiSuccess {Object[]} result.DANH_MUC_HOAN_CANH_DAC_BIET.CON Response CON
 * 
 * @apiSuccess {Object[]} result.DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET Response DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET
 * @apiSuccess {String} result.DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET.MA_CHI_TIET_HOAN_CANH Response MA_CHI_TIET_HOAN_CANH
 * @apiSuccess {String} result.DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET.TEN_CHI_TIET_HOAN_CANH Response TEN_CHI_TIET_HOAN_CANH
 * @apiSuccess {String} result.DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET.ID_CHA Response ID_CHA
 * @apiSuccess {Object[]} result.DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET.CON Response CON
 * 
 * @apiSuccess {Object[]} result.DANH_MUC_HOAN_CANH_KHAC Response DANH_MUC_HOAN_CANH_KHAC
 * @apiSuccess {String} result.DANH_MUC_HOAN_CANH_KHAC.MA_CHI_TIET_HOAN_CANH Response MA_CHI_TIET_HOAN_CANH
 * @apiSuccess {String} result.DANH_MUC_HOAN_CANH_KHAC.TEN_CHI_TIET_HOAN_CANH Response TEN_CHI_TIET_HOAN_CANH
 * @apiSuccess {String} result.DANH_MUC_HOAN_CANH_KHAC.ID_CHA Response ID_CHA
 * @apiSuccess {Object[]} result.DANH_MUC_HOAN_CANH_KHAC.CON Response CON
 * 
 * @apiSuccess {Object[]} result.DANH_MUC_HINH_THUC_TRO_GIUP Response DANH_MUC_HINH_THUC_TRO_GIUP
 * @apiSuccess {String} result.DANH_MUC_HINH_THUC_TRO_GIUP.MA_CHI_TIET_HINH_THUC Response MA_CHI_TIET_HINH_THUC
 * @apiSuccess {String} result.DANH_MUC_HINH_THUC_TRO_GIUP.TEN_CHI_TIET_HINH_THUC Response TEN_CHI_TIET_HINH_THUC
 * @apiSuccess {String} result.DANH_MUC_HINH_THUC_TRO_GIUP.ID_CHA Response ID_CHA
 * @apiSuccess {Object[]} result.DANH_MUC_HINH_THUC_TRO_GIUP.CON Response CON
 * 
 * 
 * @apiSuccessExample {json} success Response
 * 
 * {
 *      "code": 200,
 *      "message": "responseMessage",
 *      "result": {
 *              "DANH_MUC_HOAN_CANH_DAC_BIET": [
 *                      [
 *                          {
 *                              "MA_CHI_TIET_HOAN_CANH": 286,
 *                              "TEN_CHI_TIET_HOAN_CANH": "1 Trẻ em mồ côi cả cha và mẹ ",
 *                              "ID_CHA": 0,
 *                              "MA_LOAI_HOAN_CANH": 165,
 *                              "CON": [
 *                                  {
 *                                      "MA_CHI_TIET_HOAN_CANH": 331,
 *                                      "TEN_CHI_TIET_HOAN_CANH": "1.1 Không có người chăm sóc",
 *                                      "ID_CHA": 286,
 *                                      "MA_LOAI_HOAN_CANH": 165,
 *                                      "CON": []
 *                                  },
 *                                  {
 *                                      "MA_CHI_TIET_HOAN_CANH": 332,
 *                                      "TEN_CHI_TIET_HOAN_CANH": "1.2 Được nuôi dưỡng trong cơ sở cung cấp dịch vụ bảo vệ trẻ em hoặc cơ sở trợ giúp xã hội ",
 *                                      "ID_CHA": 286,
 *                                      "MA_LOAI_HOAN_CANH": 165,
 *                                      "CON": []
 *                                  },
 *                                  {
 *                                      "MA_CHI_TIET_HOAN_CANH": 333,
 *                                      "TEN_CHI_TIET_HOAN_CANH": "1.3 Sống với người thân tích",
 *                                      "ID_CHA": 286,
 *                                      "MA_LOAI_HOAN_CANH": 165,
 *                                      "CON": []
 *                                  },
 *                                  {
 *                                      "MA_CHI_TIET_HOAN_CANH": 334,
 *                                      "TEN_CHI_TIET_HOAN_CANH": "1.4 Được nhận chăm sóc thay thế bởi cá nhân, gia đình không phải người thân thích, trừ trường hợp nhận làm con nuôi ",
 *                                      "ID_CHA": 286,
 *                                      "MA_LOAI_HOAN_CANH": 165,
 *                                      "CON": []
 *                                  }
 *                              ]
 *                          },
 *                          .
 *                          .
 *                          .
 *                      ]
 *              ],
 *              "DANH_MUC_NGUY_CO_ROI_VAO_HOAN_CANH_DAC_BIET": [
 *                      [
 *                          {
 *                              "MA_CHI_TIET_HOAN_CANH": 300,
 *                              "TEN_CHI_TIET_HOAN_CANH": "1 Trẻ em sống trong gia đình nghèo, cận nghèo ",
 *                              "ID_CHA": 0,
 *                              "MA_LOAI_HOAN_CANH": 166,
 *                              "CON": [
 *                                  {
 *                                      "MA_CHI_TIET_HOAN_CANH": 398,
 *                                      "TEN_CHI_TIET_HOAN_CANH": "1.1 Trong đó trẻ em sống trong gia đình nghèo",
 *                                      "ID_CHA": 300,
 *                                      "MA_LOAI_HOAN_CANH": 166,
 *                                      "CON": []
 *                                  },
 *                                  {
 *                                      "MA_CHI_TIET_HOAN_CANH": 399,
 *                                      "TEN_CHI_TIET_HOAN_CANH": "1.2 Trẻ em sống trong gia đình cận nghèo",
 *                                      "ID_CHA": 300,
 *                                      "MA_LOAI_HOAN_CANH": 166,
 *                                      "CON": []
 *                                  }
 *                              ]
 *                          },
 *                          {
 *                              "MA_CHI_TIET_HOAN_CANH": 301,
 *                              "TEN_CHI_TIET_HOAN_CANH": "2 Trẻ em bỏ học - chưa học xong chương trình THCS ",
 *                              "ID_CHA": 0,
 *                              "MA_LOAI_HOAN_CANH": 166,
 *                              "CON": []
 *                          },
 *                          {
 *                              "MA_CHI_TIET_HOAN_CANH": 303,
 *                              "TEN_CHI_TIET_HOAN_CANH": "3 Trẻ em sống trong các gia đình có vấn đề xã hội (cha mẹ ly hôn, bạo lực gia đình, có người nhiễm HIV/AIDS)",
 *                              "ID_CHA": 0,
 *                              "MA_LOAI_HOAN_CANH": 166,
 *                              "CON": []
 *                          },
 *                          .
 *                          .
 *                          .
 *                      ]
 *              ],
 *              "DANH_MUC_HOAN_CANH_KHAC": [
 *                      [
 *                          {
 *                              "MA_CHI_TIET_HOAN_CANH": 302,
 *                              "TEN_CHI_TIET_HOAN_CANH": "1 Trẻ em bị bắt cóc, chiếm đoạt, đánh tráo",
 *                              "ID_CHA": 0,
 *                              "MA_LOAI_HOAN_CANH": 167,
 *                              "CON": []
 *                          },
 *                          {
 *                              "MA_CHI_TIET_HOAN_CANH": 397,
 *                              "TEN_CHI_TIET_HOAN_CANH": "2 Trẻ em bị tai nạn thương tích",
 *                              "ID_CHA": 0,
 *                              "MA_LOAI_HOAN_CANH": 167,
 *                              "CON": [
 *                                  {
 *                                      "MA_CHI_TIET_HOAN_CANH": 309,
 *                                      "TEN_CHI_TIET_HOAN_CANH": "2.1 Tử vong do tai nạn thương tích",
 *                                      "ID_CHA": 397,
 *                                      "MA_LOAI_HOAN_CANH": 167,
 *                                      "CON": [
 *                                          {
 *                                              "MA_CHI_TIET_HOAN_CANH": 400,
 *                                              "TEN_CHI_TIET_HOAN_CANH": "2.1.1 Tử  vong do đuối nước",
 *                                              "ID_CHA": 309,
 *                                              "MA_LOAI_HOAN_CANH": 167,
 *                                              "CON": []
 *                                          },
 *                                          {
 *                                              "MA_CHI_TIET_HOAN_CANH": 401,
 *                                              "TEN_CHI_TIET_HOAN_CANH": "2.1.2 Tử vong do tai nạn thương tích",
 *                                              "ID_CHA": 309,
 *                                              "MA_LOAI_HOAN_CANH": 167,
 *                                              "CON": []
 *                                          }
 *                                      ]
 *                                  }
 *                              ]
 *                          },
 *                          .
 *                          .
 *                          .
 *                      ]
 *              ],
 *              "DANH_MUC_HINH_THUC_TRO_GIUP": [
 *                      [
 *                          {
 *                              "MA_CHI_TIET_HINH_THUC": 310,
 *                              "TEN_CHI_TIET_HINH_THUC": "1 Trợ giúp xã hội",
 *                              "ID_CHA": 0,
 *                              "CON": [
 *                                  {
 *                                      "MA_CHI_TIET_HINH_THUC": 315,
 *                                      "TEN_CHI_TIET_HINH_THUC": "1.1 Trợ giúp thường xuyên tại cộng đồng ",
 *                                      "ID_CHA": 310,
 *                                      "CON": [
 *                                          {
 *                                              "MA_CHI_TIET_HINH_THUC": 317,
 *                                              "TEN_CHI_TIET_HINH_THUC": "1.1.1 Trợ giúp tại cộng đồng ",
 *                                              ID_CHA": 315,
 *                                              "CON": []
 *                                          },
 *                                          {
 *                                              "MA_CHI_TIET_HINH_THUC": 318,
 *                                              "TEN_CHI_TIET_HINH_THUC": "1.1.2 Nuôi dưỡng tập chung tại các trung tâm / cơ ",
 *                                              "ID_CHA": 315,
 *                                              "CON": []
 *                                          }
 *                                      ]
 *                                  },
 *                                  {
 *                                      "MA_CHI_TIET_HINH_THUC": 316,
 *                                      "TEN_CHI_TIET_HINH_THUC": "1.2 Trợ giúp đột xuất ",
 *                                      "ID_CHA": 310,
 *                                      "CON": []
 *                                  }
 *                              ]
 *                          },
 *                          .
 *                          .
 *                          .
 *                      ]
 *              ]
 *      }
 * }
 * 
 * 
 * 
 * 
 * @apiError (404 Not Found) APINotFound  API not found 
 * @apiErrorExample {json} 404-Response
 *      HTTP/1.1 404 Not Found
 *      {
 *           code: 404,
 *           message: "API Not Found GET {endpoint}",
 *      }
 * 
 */

Router.route('/').get(
    getData_DanhMuc_Controller
)

module.exports = Router;
