const Router = require('express').Router();

const {
    showListTP, addTinhThanhPho
} = require('../../controllers/tinhthanhpho.controller');

/**
 * @api {get} /v1/api/test/ Get testing route
 * @apiVersion 1.0.0
 * @apiSampleRequest http://localhost:5000/v1/api/test
 * 
 * @apiName FirstTest
 * @apiGroup Test
 * @apiHeader access-token
 * 
 * 
 *  @apiParam (Body params) {String} gioi tinh Tre em name
 * @apiParamExample {json} Request-example
 * {
 *      "gioitinh":"gsgds",
 * }
 */
Router.route('/add').post(
    addTinhThanhPho
)

Router.route('/').get(
    showListTP
)


module.exports = Router;