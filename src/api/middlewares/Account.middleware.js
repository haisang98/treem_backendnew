const Model = require('../models')

module.exports.isRoleManagerAccount = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            User
        } = new Model({sqlConn})

        const [usernameRows, usernameFields] = await User.getLocalUser(req.decoded.id_taikhoan)

        req.params.id_tinhthanhpho = usernameRows[0]['id_tinhthanhpho']

        if(usernameRows[0]['id_phuongxa'] == null && usernameRows[0]['id_quanhuyen'] == null && usernameRows[0]['id_tinhthanhpho'] != null){
            next()
        }else{
            res.status(401).json({
                message : "Bạn không có quyền truy cập"
            }).end()
        }

    } catch (error) {
        next(error);
    }
}