const Model = require('../models')

module.exports.isRolePhuongXa = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            User
        } = new Model({sqlConn})

        const [usernameRows, usernameFields] = await User.getLocalUser(req.decoded.id_taikhoan)
        req.params.id_phuongxa = usernameRows[0]['id_phuongxa']

        if(usernameRows[0]['id_phuongxa']){
            next()
        }else{
            res.status(401).json({
                message : "Bạn không có quyền truy cập"
            }).end()
        }

    } catch (error) {
        next(error);
    }
}