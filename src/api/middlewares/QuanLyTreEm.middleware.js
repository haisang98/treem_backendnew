const Model = require('../models');


module.exports.isCheckIdChildrenBeLongTo = async (req, res, next) => {
    try {
        const {
            sqlConn
        } = req.app.locals
        const {
            QuanLyHoSoTreEm
        } = new Model({sqlConn})
        
        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const address = {
            id_tinhthanhpho:thanhpho ? thanhpho : null,
            id_quanhuyen:huyen ? huyen : null,
            id_phuongxa : xa ? xa : null
        }

        const {
            id
        } = req.params

        const [rows, fields] = await QuanLyHoSoTreEm.CheckChildrenBeLongTo(id, address)

        if(rows.length <= 0){
            return res.status(401).json({
                message : "Bạn không có quyền truy cập"
            }).end()
        }

        next()

    } catch (error) {
        next(error)
    }
}


module.exports.isCheckUserPhuongXa = async (req, res, next) => {
    try {
        const {
            sqlConn
        } = req.app.locals
        const {
            QuanLyHoSoTreEm
        } = new Model({sqlConn})
        
        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const address = {
            id_tinhthanhpho:thanhpho ? thanhpho : null,
            id_quanhuyen:huyen ? huyen : null,
            id_phuongxa : xa ? xa : null
        }

        if(address.id_tinhthanhpho != null && address.id_quanhuyen != null && address.id_phuongxa != null){
            next()
        }else{
            res.status(401).json({
                code: 401,
                message : "Bạn không có quyền truy cập"
            }).end()
        }

    } catch (error) {
        next(error)
    }
}

module.exports.isCheckRoleDeleteLogicalChildren = async (req, res, next) => {
    try {
        const {
            sqlConn
        } = req.app.locals
        const {
            QuanLyHoSoTreEm
        } = new Model({sqlConn})
        
        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const address = {
            id_tinhthanhpho:thanhpho ? thanhpho : null,
            id_quanhuyen:huyen ? huyen : null,
            id_phuongxa : xa ? xa : null
        }

        const {
            id_treem
        } = req.params

        const [rows, fields] = await QuanLyHoSoTreEm.CheckChildrenBeLongTo(id_treem, address)

        if(rows.length <= 0){
            return res.status(401).json({
                message : "Bạn không có quyền truy cập",
                address,
            }).end()
        }

        next();

    } catch (error) {
        next(error)
    }
}

module.exports.isCheckRoleDeletePhysicalChildren = async (req, res, next) => {
    try {
        const {
            sqlConn
        } = req.app.locals
        const {
            QuanLyHoSoTreEm
        } = new Model({sqlConn})
        
        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const address = {
            id_tinhthanhpho:thanhpho ? thanhpho : null,
            id_quanhuyen:huyen ? huyen : null,
            id_phuongxa : xa ? xa : null
        }

        const {
            id_treem
        } = req.params

        const [rows, fields] = await QuanLyHoSoTreEm.CheckChildrenInTrash(id_treem, address)

        if(rows.length <= 0){
            return res.status(401).json({
                message : "Bạn không có quyền truy cập"
            }).end()
        }

        next()

    } catch (error) {
        next(error)
    }
}

module.exports.isCheckRoleUpdateChildren = async (req, res, next) => {
    try {
        const {
            sqlConn
        } = req.app.locals
        const {
            QuanLyHoSoTreEm
        } = new Model({sqlConn})
        
        const {
            thanhpho,
            huyen,
            xa
        } = req.query

        const address = {
            id_tinhthanhpho:thanhpho ? thanhpho : null,
            id_quanhuyen:huyen ? huyen : null,
            id_phuongxa : xa ? xa : null
        }

        const {
            id
        } = req.params

        const [rows, fields] = await QuanLyHoSoTreEm.CheckChildrenBeLongTo(id, address)

        if(rows.length <= 0){
            return res.status(401).json({
                message : "Bạn không có quyền truy cập"
            }).end()
        }

        next()

    } catch (error) {
        next(error)
    }
}