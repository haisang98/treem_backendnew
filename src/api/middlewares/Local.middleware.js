const Model = require('../models')

module.exports.isRoleLocal = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            User
        } = new Model({sqlConn})

        const [usernameRows, usernameFields] = await User.getLocalUser(req.decoded.id_taikhoan)
        const [{id_tinh, thanhpho}, {id_quan, quanhuyen}, {id_xa, phuongxa}] = await User.GetLocationUser(req.decoded.id_taikhoan)


        if(usernameRows[0]['id_phuongxa'] != null){
            req.query.xa = usernameRows[0]['id_phuongxa']
            req.query.huyen = usernameRows[0]['id_quanhuyen']
            req.query.thanhpho = usernameRows[0]['id_tinhthanhpho']
            req.query.tenxa = phuongxa
            req.query.tenhuyen = quanhuyen
            req.query.tenthanhpho = thanhpho
            req.query.tenhienthi = usernameRows[0]['tenhienthi']
            next()
        }else if(usernameRows[0]['id_phuongxa'] == null && usernameRows[0]['id_quanhuyen'] != null){
            req.query.huyen = usernameRows[0]['id_quanhuyen']
            req.query.thanhpho = usernameRows[0]['id_tinhthanhpho']
            req.query.tenhuyen = quanhuyen
            req.query.tenthanhpho = thanhpho
            req.query.tenhienthi = usernameRows[0]['tenhienthi']
            next()
        }else if(usernameRows[0]['id_phuongxa'] == null && usernameRows[0]['id_quanhuyen'] == null && usernameRows[0]['id_tinhthanhpho'] != null){
            req.query.thanhpho = usernameRows[0]['id_tinhthanhpho']
            req.query.tenthanhpho = thanhpho
            req.query.tenhienthi = usernameRows[0]['tenhienthi']
            next()
        }else{
            res.status(401).json({
                code : 401,
                message : "Bạn không có quyền truy cập"
            }).end()
        }

    } catch (error) {
        next(error);
    }
}
