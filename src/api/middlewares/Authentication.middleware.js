const { decode } = require('jsonwebtoken');
const {
    Verify
} = require('../helpers/HandleToken.helper')
const Model = require('../models')

module.exports.isAuth = async (req, res, next) => {
    const token = req.headers['access-token'];

    if(token){
        try{
            const decoded = await Verify(token,process.env.ACCESS_TOKEN);
            const {
                sqlConn
            } = req.app.locals

            const {
                User
            } = new Model({sqlConn})
            const [rows, fields] = await User.CheckUsername(decoded.username)
            
            const payload = {
                id_taikhoan:rows[0].id_taikhoan,
                email:rows[0].email,
                tenhienthi:rows[0].tenhienthi,
                id_phongban:rows[0].id_phongban,
                tentaikhoan:rows[0].tentaikhoan
            }

            req.decoded = payload;
            req.token_ = token;
            next()
        }catch(error){
            return res.status(401).json({
                code: 401,
                message: 'Unauthorization'
            }).end()
        }
    }else{
        return res.status(401).json({
            code : 401,
            message: 'No Token Provived'
        }).end()
    }
    
}