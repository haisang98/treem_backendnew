const Model = require('../models')

module.exports.isRoleManagerVillage = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            User
        } = new Model({sqlConn})

        const [usernameRows, usernameFields] = await User.getLocalUser(req.decoded.id_taikhoan)
        console.log(usernameRows);

        req.params.id_phuongxa = usernameRows[0]['id_phuongxa']

        if(usernameRows[0]['id_phuongxa']){
            next()
        }else{
            res.status(401).json({
                message : "Bạn không có quyền truy cập"
            }).end()
        }

    } catch (error) {
        next(error);
    }
}

module.exports.isExistsChildrenInLocal = async (req, res, next) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            QuanLyThon
        } = new Model({sqlConn})

        const [row, fields] = await QuanLyThon.isCheckChildrenHaveExists(req.params.id)

        if(row && row.length > 0){
            res.status(403).json({
                code: 403,
                message : "Thôn này đang được sử dụng. Bạn dùng chức năng gộp trước khi xóa"
            }).end()
            
        }else{
            next()
        }

    } catch (error) {
        next(error);
    }
}
