const jwt = require('jsonwebtoken');

module.exports.generator = async (payload, ACCESS_TOKEN_KEY, REFRESH_TOKEN_KEY, ACCESS_TOKEN_LIFE, REFRESH_TOKEN_LIFE) => {

    try {
        const accessToken = await jwt.sign(payload, ACCESS_TOKEN_KEY, {expiresIn:ACCESS_TOKEN_LIFE})
        // const refreshToken = await jwt.sign(payload, REFRESH_TOKEN_KEY, {expiresIn:REFRESH_TOKEN_LIFE})

        return [accessToken]

    } catch (error) {
        throw error
    }

}

module.exports.Verify = async (token, secret) => {
    try{
        const decoded = await jwt.verify(token,secret)
        return decoded
    }catch(err){
        throw err
    }
}

module.exports.renewAccessToken = async (payload, ACCESS_TOKEN_KEY,ACCESS_TOKEN_LIFE) => {

    try {
        const accessToken = await jwt.sign(payload, ACCESS_TOKEN_KEY, {expiresIn:ACCESS_TOKEN_LIFE})

        return accessToken

    } catch (error) {
        throw error
    }

}