module.exports.getAllHoanCanh = (rows) => {
    const length = rows.length
    for(var i = 0; i<length; i++){
        rows[i].value = false;
        rows[i].children = [];
    }

    let cha1 = [];
    let cha2 = [];
    let cha3 = [];

    // Push cha1
    for(var i = 0; i<length; i++){
        if(rows[i].cha == 0){
            cha1.push(rows[i]);
        }
    }

    // Push cha1, cha2
    const lengthCha1 = cha1.length;
    for(var i = 0; i<length; i++){
        for(var j = 0; j<lengthCha1; j++){
            if(rows[i].cha == cha1[j].id_hoancanh){
                cha1[j].children.push(rows[i]);
                cha2.push(rows[i])
            }
        }
    }

    //Push cha2, cha3
    const lengthCha2 = cha2.length;
    for(var i = 0; i<length; i++){
        for(var j = 0; j<lengthCha2; j++){
            if(rows[i].cha == cha2[j].id_hoancanh){
                cha3.push(rows[i])
            }
        }
    }

    // Push cha1 From cha3
    const lengthCha3 = cha3.length;
    for(var i = 0; i<lengthCha1; i++){
        for(var j = 0; j<cha1[i].children.length; j++){
            for(var k = 0; k<lengthCha3; k++){
                if(cha3[k].cha == cha1[i].children[j].id_hoancanh){
                    cha1[i].children[j].children.push(cha3[k]);
                }
            }
        }
    }

    return cha1;
}


/* Clone getAllHoanCanh convert Format TreeSelect */
module.exports.cloneGetAllHoanCanhFormatTreeSelect = (rows) => {
    const length = rows.length
    for(var i = 0; i<length; i++){
        rows[i].children = [];
    }

    let cha1 = [];
    let cha2 = [];
    let cha3 = [];

    // Push cha1
    for(var i = 0; i<length; i++){
        if(rows[i].cha == 0){
            cha1.push(rows[i]);
        }
    }

    // Push cha1, cha2
    const lengthCha1 = cha1.length;
    for(var i = 0; i<length; i++){
        for(var j = 0; j<lengthCha1; j++){
            if(rows[i].cha == cha1[j].value){
                cha1[j].children.push(rows[i]);
                cha2.push(rows[i])
            }
        }
    }

    //Push cha2, cha3
    const lengthCha2 = cha2.length;
    for(var i = 0; i<length; i++){
        for(var j = 0; j<lengthCha2; j++){
            if(rows[i].cha == cha2[j].value){
                cha3.push(rows[i])
            }
        }
    }

    // Push cha1 From cha3
    const lengthCha3 = cha3.length;
    for(var i = 0; i<lengthCha1; i++){
        for(var j = 0; j<cha1[i].children.length; j++){
            for(var k = 0; k<lengthCha3; k++){
                if(cha3[k].cha == cha1[i].children[j].value){
                    cha1[i].children[j].children.push(cha3[k]);
                }
            }
        }
    }

    return cha1;
}

// module.exports.reTest = (rows1, cha)

module.exports.detailHoanCanh = (rows1, cha1) => {

    const lengthInfoChildren = rows1.length

    for(var i = 0; i<lengthInfoChildren; i++){
        // IF rows1[i] == 0 => rows1 is root (parent == 0) then set rows1[i].value = 0 and all child myself == (value = true)
        if(rows1[i].parent == 0){

            let lengthRoot = cha1.length
            for(var l = 0; l<lengthRoot; l++){
                if(cha1[l].id_chitietloaihoancanh == rows1[i].id_){
                    cha1[l].value = true
                    let lengthCha1_ = cha1[l].children.length
                    for(var j = 0; j<lengthCha1_; j++){
                        cha1[l].children[j].value = true
                        let lengthCha1Children = cha1[l].children[j].children.length
                        for(var k = 0; k<lengthCha1Children; k++){
                            cha1[l].children[j].children[k].value = true
                        }
                    }
                }
            }
        }
        else{ // else find rows1[i] at level 2 or level 3
            let lengthCha1__ = cha1.length
            for(var x = 0; x<lengthCha1__; x++){
                // IF level 2 set parent rows1[i] (value==true) and find it, set value=true for rows1[i]
                if(cha1[x].id_chitietloaihoancanh == rows1[i].parent){
                    cha1[x].value = true
                    let length_level2 = cha1[x].children.length
                    for(var c = 0; c<length_level2; c++){
                        if(cha1[x].children[c].id_chitietloaihoancanh == rows1[i].id_){
                            cha1[x].children[c].value = true
                        }
                    }
                }else{ // IF rows1[i] level 3. handle same level 2. But handle at level 3 instead of at level 2
                    let length_level2 = cha1[x].children.length
                    for(var z = 0; z<length_level2; z++){
                        if(cha1[x].children[z].id_chitietloaihoancanh == rows1[i].parent){
                            cha1[x].children[z].value = false
                            // Set value root = 0
                            let lengthRoot_ = cha1.length
                            for(var h = 0; h<lengthRoot_; h++){
                                if(cha1[x].children[z].cha == cha1[h].id_chitietloaihoancanh){
                                    cha1[h].value = true
                                }
                            }
                            let length_level3 = cha1[x].children[z].children.length
                            for(var b = 0; b<length_level3; b++){
                                if(cha1[x].children[z].children[b].id_chitietloaihoancanh == rows1[i].id_){
                                    cha1[x].children[z].children[b].value = true
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return cha1;
}

listRoot = (rows) => {
    const root = []
    const length = rows.length;
    for(var i = 0; i<length; i++){
        if(rows[i].chahinhthuc == 0){
            root.push(rows[i]);
        }
    }
    return root
}

module.exports.dequi = (rows, sum=0, result, n=0,) => {
    
    if(n == 0){
        let root = listRoot(rows)
        result[n] = [...root]
        sum += result[n].length
        return this.dequi(rows, sum, result, n+1)
    }

    const length = rows.length;
    const lengthResult = result[n-1].length

    let array = [];
    for(var i = 0; i<lengthResult; i++){
        for(var j = 0; j<length; j++){
            if(rows[j].chahinhthuc == result[n-1][i].id_hinhthuc){
                array.push(rows[j])
            }
        }
    }

    result[n] = [...array]

    sum += result[n].length;

    if(sum < rows.length)
        return this.dequi(rows, sum, result, n+1);

    if(sum == rows.length){
        return result
    }
}

/* De Qui Follow TreeSelect */
module.exports.deQuyTreeSelect = (rows, sum=0, result, n=0,) => {
    
    if(n == 0){
        let root = listRoot(rows)
        result[n] = [...root]
        sum += result[n].length
        return this.deQuyTreeSelect(rows, sum, result, n+1)
    }

    const length = rows.length;
    const lengthResult = result[n-1].length

    let array = [];
    for(var i = 0; i<lengthResult; i++){
        for(var j = 0; j<length; j++){
            if(rows[j].chahinhthuc == result[n-1][i].value){
                array.push(rows[j])
            }
        }
    }

    result[n] = [...array]

    sum += result[n].length;

    if(sum < rows.length)
        return this.deQuyTreeSelect(rows, sum, result, n+1);

    if(sum == rows.length){
        return result
    }
}

module.exports.detailHTTG = (data) => {
    for(var i = 0; i < data.length; i++){
        for(var j = 0; j < data[i].length; j++){
            data[i][j].value = false;   
            data[i][j].children = [];
        }
    }

    function addChildIntoParent(parent, child){

        for(var i = 0; i < parent.length; i++){
            for(var j = 0; j < child.length; j++){
                if(parent[i].id_hinhthuc == child[j].chahinhthuc){
                    parent[i].children.push(child[j]);
                }
            }
        }
        return parent;
    }

    var root = [];
    for(var i = data.length-1; i > 0; i--){
        root = addChildIntoParent(data[i-1], data[i]);
    }

    return root
}

/* Detail HTTG Tree Select */
module.exports.detailHTTGTreeSelect = (data) => {
    for(var i = 0; i < data.length; i++){
        for(var j = 0; j < data[i].length; j++){
            data[i][j].children = [];
        }
    }

    function addChildIntoParent(parent, child){

        for(var i = 0; i < parent.length; i++){
            for(var j = 0; j < child.length; j++){
                if(parent[i].value == child[j].chahinhthuc){
                    parent[i].children.push(child[j]);
                }
            }
        }
        return parent;
    }

    var root = [];
    for(var i = data.length-1; i > 0; i--){
        root = addChildIntoParent(data[i-1], data[i]);
    }

    return root
}

let parents = [];

const checkValueForAllChildren =(children)=>{
    children.forEach(child=> {
       child.value =true;
       if(child.children.length>0) checkValueForAllChildren(child.children)
   });
}


const checkValue = (data, target)=>{
    for(let i=0;i<data.length;i++){
        if(data[i].id_hinhthuc === target.id_hinhthuc){
            data[i].value = true;
            checkValueForAllChildren(data[i].children);
            return;
        }else{
            checkValue(data[i].children,target);
        }
    }

    findParents(data, target.id_hinhthuc)

    return parents
}


const CheckLevelOtherRoot = (data, id) => {
    const length = data.length
    for(var i = 0; i<length; i++){
        if(data[i].id_hinhthuc == id){
            data[i].value = true
            return;
        }
        CheckLevelOtherRoot(data[i].children,id)
    }
}

module.exports.eachRow = (data, rows) => {
    const lengthRows = rows.length;
    for(var i = 0; i<lengthRows; i++){     
       let newData = checkValue(data,rows[i])
       parents = [];
    //    if(newData !== undefined && newData.length > 0){
    //         newData.forEach(e => {
    //             const idx = data.findIndex(({ id_hinhthuc }) => id_hinhthuc===e)
    //             if(idx !== -1){
    //                 data[idx].value = true
    //             }
    //         })
    //    }
    }
}

/* Use For Download File Excel Follow Object Children */
module.exports.eachRowObject = (data, rows) => {
    const lengthRows = rows.length;
    for(var i = 0; i<lengthRows; i++){     
       let newData = checkValue(data,rows[i])
       parents = [];
       if(newData !== undefined && newData.length > 0){
            newData.forEach(e => {
                const idx = data.findIndex(({ id_hinhthuc }) => id_hinhthuc===e)
                if(idx !== -1){
                    data[idx].value = true
                }
            })
       }
    }
}

module.exports.eachRow1Object = (data, rows) => {
    const lengthRows = rows.length;
    for(var i = 0; i<lengthRows; i++){
        let newData = checkValue1(data,rows[i])
        parents = [];
        if(newData !== undefined && newData.length > 0){
            newData.forEach(e => {
                const idx = data.findIndex(({ id_hoancanh }) => id_hoancanh===e)
                if(idx !== -1){
                    data[idx].value = true
                }
            })
        }
    }
}

const checkValue1 = (data, target)=>{
    for(let i=0;i<data.length;i++){
        if(data[i].id_hoancanh === target.id_){
            data[i].value = true;
            checkValueForAllChildren(data[i].children);
            return;
        }else{
            checkValue1(data[i].children,target);
        }
    }

    findParents2(data, target.id_)

    return parents
}

module.exports.eachRow1 = (data, rows) => {
    const lengthRows = rows.length;
    for(var i = 0; i<lengthRows; i++){
        let newData = checkValue1(data,rows[i])
        parents = [];
        // if(newData !== undefined && newData.length > 0){
        //     newData.forEach(e => {
        //         const idx = data.findIndex(({ id_hoancanh }) => id_hoancanh===e)
        //         if(idx !== -1){
        //             data[idx].value = true
        //         }
        //     })
        // }
    }
}

/* UPDATE */
const findParents = (data, id) =>{
    for (let i=0;i<data.length;i++){
        if(data[i].id_hinhthuc == id) {

            parents.push(data[i].id_hinhthuc)
            return true;
        }
        if(data[i].children.length>0) {
            let parent = data[i].id_hinhthuc;
            if(findParents(data[i].children,id)){
                parents.push(parent);
                return true
            };
        } 
    }    
    return false
}

const findParents2 = (data, id) =>{
    for (let i=0;i<data.length;i++){
        if(data[i].id_hoancanh == id) {

            parents.push(data[i].id_hoancanh)
            return true;
        }
        if(data[i].children.length>0) {
            let parent = data[i].id_hoancanh;
            if(findParents2(data[i].children,id)){
                parents.push(parent);
                return true
            };
        } 
    }    
    return false
}