const Model = require('../models');
const empty = require('is-empty');

module.exports.onHandleHCDB = async (req) => {
    try {

        const {
            sqlConn
        } = req.app.locals

        const {
            HoanCanhDacBiet
        } = new Model({ sqlConn })

        const {
            thon,
            hoancanh,
            magiadinh,
            tentreem,
            timestart,
            timefinish
        } = req.query
    
        
        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            id_hoancanh: hoancanh ? hoancanh : null,
            id_giadinh: magiadinh ? (isNaN(magiadinh) ? -123456789 : magiadinh) : null,
            hoten: tentreem ? tentreem : null,
            ts: timestart ? timestart : null,
            tf: timefinish ? timefinish : null
        };  
    
        
        timeS = Object.ts;
        timeF = Object.tf;
        // 
        // 
    
        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()
        //
    
        
        function timeD(a, b){
            var temp = "";
            if(a != null && b != null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
            }else if(a != null && b == null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }
    
        var search = "";
        for(const k in Object){
            if(Object[k]  != null && k != 'ts' && k != 'tf'){
                if(empty(search)){
                    if(k == 'hoten'){
                        search = k + " LIKE '%" + Object[k] + "%'";
                    }else{
                        search = k + " = " + Object[k];
                    }  
                }else{
                    if(k == 'hoten'){
                        search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                    } else{
                        search += " AND " + k + " = " + Object[k] ;
                    }  
                }
            }
        } 
    
        if(empty(search)){
            search = timeD(timeS, timeF);
        }else{
            if(timeD(timeS, timeF) == ""){
                search += "";
            }else{
                search += " AND " + timeD(timeS, timeF);
            }
        }
    
        if(empty(search)){
            search = " 1 ";
        }
    
        //---------------------------------------------------------------------------
        
        const [rows, fields] = await HoanCanhDacBiet.searchData_HCDB(search);
        const getData_DM_HCDB = await HoanCanhDacBiet.getData_DM_HCDB();
        const searchData_DM_CT_HCDB = await HoanCanhDacBiet.searchData_DM_CT_HCDB(search);
    
        var hoancanhdacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];
    
            if(searchData_DM_CT_HCDB[0][i].cha == 0){
                var dcha1 =searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }
    
                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
    
                
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];
                var dcha1 = searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }
    
                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
        
    
            hoancanhdacbiet.push(rows[i]);
            //
        }
    
        var ds_f1 = [];
        ds_f1.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < ds_f1.length; j ++){
                if(rows[i].id_treem == ds_f1[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(ds_f1[j].hoancanhdacbiet[k].value == "true"){
                            rows[i].hoancanhdacbiet[k].value = "true";
                        }
                    }
                    ds_f1.splice(j, 1);
                }
            }
            ds_f1.push(rows[i]);
        }
    
        if(!ds_f1[0]){
            ds_f1 = [...ds_f1.slice(1)];
        }
    
        return ds_f1;
    } catch (error) {
        
    }
}

module.exports.onHandleNCHCDB = async (req) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            NguyCoHCDB,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa,
            thon,
            hoancanh,
            magiadinh,
            tentreem,
            timestart,
            timefinish
        } = req.query

        const Object = {
            id_tinhthanhpho: thanhpho ? thanhpho : null,
            id_quanhuyen: huyen ? huyen : null,
            id_phuongxa: xa ? xa : null,
            id_thon: thon ? thon : null,
            id_hoancanh: hoancanh ? hoancanh : null,
            id_giadinh: magiadinh ? (isNaN(magiadinh) ? -123456789 : magiadinh) : null,
            hoten: tentreem ? tentreem : null,
            ts: timestart ? timestart : null,
            tf: timefinish ? timefinish : null
        };  

        // 
        timeS = Object.ts;
        timeF = Object.tf;

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()

        
        function timeD(a, b){
            var temp = "";
            if(a != null && b != null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
            }else if(a != null && b == null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        var search = "";
        for(const k in Object){
            if(Object[k]  != null && k != 'ts' && k != 'tf'){
                if(empty(search)){
                    if(k == 'hoten'){
                        search = k + " LIKE '%" + Object[k] + "%'";
                    }else{
                        search = k + " = " + Object[k];
                    }  
                }else{
                    if(k == 'hoten'){
                        search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                    } else{
                        search += " AND " + k + " = " + Object[k] ;
                    }  
                }
            }
        } 

        if(empty(search)){
            search = timeD(timeS, timeF);
        }else{
            if(timeD(timeS, timeF) == ""){
                search += "";
            }else{
                search += " AND " + timeD(timeS, timeF);
            }
        }

        if(empty(search)){
            search = " 1 ";
        }

        //---------------------------------------------------------------------------
        // 
        
        const [rows, fields] = await NguyCoHCDB.searchData_NCHCDB(search);
        const getData_DM_NCHCDB = await NguyCoHCDB.getData_DM_NCHCDB();
        const searchData_DM_CT_NCHCDB = await NguyCoHCDB.searchData_DM_CT_NCHCDB(search);

        var nguycodacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false"];

            if(searchData_DM_CT_NCHCDB[0][i].cha == 0){

                var dcha1 = searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
               if(dcha1 > 9){
                   dcha = dcha1;
               }else{
                    dcha =  searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: getData_DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false"];

                var dcha1 = searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =  searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: getData_DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
            nguycodacbiet.push(rows[i]);
        }

        var listObjectNonDuplicate = [];
        listObjectNonDuplicate.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < listObjectNonDuplicate.length; j ++){
                if(rows[i].id_treem == listObjectNonDuplicate[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(listObjectNonDuplicate[j].nguycoHCDB[k].value == "true"){
                            rows[i].nguycoHCDB[k].value = "true";
                        }
                    }
                    listObjectNonDuplicate.splice(j, 1);
                }
            }
            listObjectNonDuplicate.push(rows[i]);
        }

        if(!listObjectNonDuplicate[0]){
            listObjectNonDuplicate = [...listObjectNonDuplicate.slice(1)];
        }

        return listObjectNonDuplicate;
    } catch (error) {
        
    }
}

module.exports.onHandleHCK = async (req) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            HoanCanhKhac,
        } = new Model({ sqlConn });

        const {
            thon,
            hoancanh,
            magiadinh,
            tentreem,
            timestart,
            timefinish
        } = req.query
        var req_f = req.query;

        const Object = {
            // id_tinhthanhpho: thanhpho ? thanhpho : null,
            // id_quanhuyen: huyen ? huyen : null,
            // id_phuongxa: xa ? xa : null,
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            id_hoancanh: hoancanh ? hoancanh : null,
            id_giadinh: magiadinh ? (isNaN(magiadinh) ? -123456789 : magiadinh) : null,
            hoten: tentreem ? tentreem : null,
            ts: timestart ? timestart : null,
            tf: timefinish ? timefinish : null
        };  

        
        timeS = Object.ts;
        timeF = Object.tf;
        // 
        // 

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()
        //

        
        function timeD(a, b){
            var temp = "";
            if(a != null && b != null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
            }else if(a != null && b == null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        var search = "";
        for(const k in Object){
            if(Object[k]  != null && k != 'ts' && k != 'tf'){
                if(empty(search)){
                    if(k == 'hoten'){
                        search = k + " LIKE '%" + Object[k] + "%'";
                    }else{
                        search = k + " = " + Object[k];
                    }  
                }else{
                    if(k == 'hoten'){
                        search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                    } else{
                        search += " AND " + k + " = " + Object[k] ;
                    }  
                }
            }
        } 

        if(empty(search)){
            search = timeD(timeS, timeF);
        }else{
            if(timeD(timeS, timeF) == ""){
                search += "";
            }else{
                search += " AND " + timeD(timeS, timeF);
            }
        }

        if(empty(search)){
            search = " 1 ";
        }

        //---------------------------------------------------------------------------
        
        
        const [rows, fields] = await HoanCanhKhac.searchData_HCK(search);
        const getData_DM_HCK = await HoanCanhKhac.getData_DM_HCK();
        const searchData_DM_CT_HCK = await HoanCanhKhac.searchData_DM_CT_HCK(search);

        var hoancanhkhac = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false"];

            if(searchData_DM_CT_HCK[0][i].cha == 0){
                var dcha1 =searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhkhac = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhkhac.push({
                        name: getData_DM_HCK[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }

                
            }else{
                var dfield = ["false", "false"];
                var dcha1 = searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhkhac = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhkhac.push({
                        name: getData_DM_HCK[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
        

            hoancanhkhac.push(rows[i]);
            //
        }

        var ds_f1 = [];
        ds_f1.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < ds_f1.length; j ++){
                if(rows[i].id_treem == ds_f1[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(ds_f1[j].hoancanhkhac[k].value == "true"){
                            rows[i].hoancanhkhac[k].value = "true";
                        }
                    }
                    ds_f1.splice(j, 1);
                }
            }
            ds_f1.push(rows[i]);
        }

        if(!ds_f1[0]){
            ds_f1 = [...ds_f1.slice(1)];
        }
        return ds_f1;
    } catch (error) {
        
    }
}

module.exports.onHandleHTTG = async (req) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            HinhThucTroGiup,
        } = new Model({ sqlConn });

        const {
            thanhpho,
            huyen,
            xa,
            thon,
            hinhthuc,
            magiadinh,
            tentreem,
            timestart,
            timefinish
        } = req.query

        const Object = {
            id_tinhthanhpho: thanhpho ? thanhpho : null,
            id_quanhuyen: huyen ? huyen : null,
            id_phuongxa: xa ? xa : null,
            id_thon: thon ? thon : null,
            id_hinhthuc: hinhthuc ? hinhthuc : null,
            id_giadinh: magiadinh ? (isNaN(magiadinh) ? -123456789 : magiadinh) : null,
            hoten: tentreem ? tentreem : null,
            ts: timestart ? timestart : null,
            tf: timefinish ? timefinish : null
        };  

        
        timeS = Object.ts;
        timeF = Object.tf;
        // 
        // 

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()
        //

        
        function timeD(a, b){
            var temp = "";
            if(a != null && b != null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + b + "')";
            }else if(a != null && b == null){
                temp = "(ngaysinh BETWEEN '" + a + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + b + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        var search = "";
        for(const k in Object){
            if(Object[k]  != null && k != 'ts' && k != 'tf'){
                if(empty(search)){
                    if(k == 'hoten'){
                        search = k + " LIKE '%" + Object[k] + "%'";
                    }else{
                        search = k + " = " + Object[k];
                    }  
                }else{
                    if(k == 'hoten'){
                        search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                    } else{
                        search += " AND " + k + " = " + Object[k] ;
                    }  
                }
            }
        } 

        if(empty(search)){
            search = timeD(timeS, timeF);
        }else{
            if(timeD(timeS, timeF) == ""){
                search += "";
            }else{
                search += " AND " + timeD(timeS, timeF);
            }
        }

        if(empty(search)){
            search = " 1 ";
        }

        //---------------------------------------------------------------------------
        
        
        const [rows, fields] = await HinhThucTroGiup.searchData_HTTG(search);
        const getData_DM_HTTG = await HinhThucTroGiup.getData_DM_HTTG();
        const searchData_DM_CT_HTTG = await HinhThucTroGiup.searchData_DM_CT_HTTG(search);

        var hinhthuctrogiup = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false"];

            if(searchData_DM_CT_HTTG[0][i].cha == 0){
                var dcha1 =searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hinhthuctrogiup = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hinhthuctrogiup.push({
                        name: getData_DM_HTTG[0][j].tenhinhthuc,
                        value: dfield[j],
                    });
                }

                
            }else{
                var dfield = ["false", "false", "false", "false", "false"];
                var dcha1 = searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hinhthuctrogiup = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hinhthuctrogiup.push({
                        name: getData_DM_HTTG[0][j].tenhinhthuc,
                        value: dfield[j],
                    });
                }
            }
        

            hinhthuctrogiup.push(rows[i]);
            //
        }

        var listObjectNonDuplicate = [];
        listObjectNonDuplicate.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < listObjectNonDuplicate.length; j ++){
                if(rows[i].id_treem == listObjectNonDuplicate[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(listObjectNonDuplicate[j].hinhthuctrogiup[k].value == "true"){
                            rows[i].hinhthuctrogiup[k].value = "true";
                        }
                    }
                    listObjectNonDuplicate.splice(j, 1);
                }
            }
            listObjectNonDuplicate.push(rows[i]);
        }

        if(!listObjectNonDuplicate[0]){
            listObjectNonDuplicate = [...listObjectNonDuplicate.slice(1)];
        }

        return listObjectNonDuplicate;
    } catch (error) {
        
    }
}

/* CHI TIEU */
module.exports.onHandleObjectHCDB = async (req) => {
    try {

        const {
            sqlConn
        } = req.app.locals

        const {
            HoanCanhDacBiet
        } = new Model({ sqlConn })

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, dotuoitu, dotuoiden
        } = req.query
    
        
        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            dotuoitu: dotuoitu ? dotuoitu : null,
            dotuoiden: dotuoiden ? dotuoiden : null,
        };
    
        
        
        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()
    
        function CalculatorAge(fromAge, toAge){
            const currentDate = new Date();
            const from_ = currentDate.getFullYear() - fromAge;
            const to_ = currentDate.getFullYear() - toAge;
            const tuoiTu = new Date(`${from_}-01-01`);
            const tuoiDen = new Date(`${to_}-01-01`);
            const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
            const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
            return [tT,tD];
        }

        function aboutAge(a, b){
            let temp = "";
            if(a != null && b != null){
                const [from, to] = CalculatorAge(a,b);
                temp = "(ngaysinh BETWEEN '" + from + "' AND '" + to + "')";
            }else if(a != null && b == null){
                /* TH fromAge Exists AND toAge undefined */
                const from_ = currentDate.getFullYear() - a;
                const tuoiTu = new Date(`${from_}-01-01`);
                const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
                temp = "(ngaysinh BETWEEN '" + tT + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                /* TH fromAge undefined AND toAge Exists */
                const to_ = currentDate.getFullYear() - a;
                const tuoiDen = new Date(`${to_}-01-01`);
                const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + tD + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        function whereSearch(Object){
            let agesFrom = Object.dotuoitu;
            let comeOfAge = Object.dotuoiden;
            let search = "";
            for(const k in Object){
                if(Object[k]  != null && k != 'dotuoitu' && k != 'dotuoiden'){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }

            if(empty(search)){
                search = aboutAge(agesFrom, comeOfAge);
            }else{
                if(aboutAge(agesFrom, comeOfAge) == ""){
                    search += "";
                }else{
                    search += " AND " + aboutAge(agesFrom, comeOfAge);
                }
            }

            if(empty(search)){
                search = " 1 ";
            }


            return search;
        }
    
        //---------------------------------------------------------------------------
        
        const [rows, fields] = await HoanCanhDacBiet.searchData_HCDB(whereSearch(Object));
        const getData_DM_HCDB = await HoanCanhDacBiet.getData_DM_HCDB();
        const searchData_DM_CT_HCDB = await HoanCanhDacBiet.searchData_DM_CT_HCDB(whereSearch(Object));
    
        var hoancanhdacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];
    
            if(searchData_DM_CT_HCDB[0][i].cha == 0){
                var dcha1 =searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }
    
                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
    
                
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"];
                var dcha1 = searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = searchData_DM_CT_HCDB[0][i].ten_hoancanh.substr(0, 1);
                }
    
                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhdacbiet = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhdacbiet.push({
                        name: getData_DM_HCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
        
    
            hoancanhdacbiet.push(rows[i]);
            //
        }
    
        var ds_f1 = [];
        ds_f1.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < ds_f1.length; j ++){
                if(rows[i].id_treem == ds_f1[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(ds_f1[j].hoancanhdacbiet[k].value == "true"){
                            rows[i].hoancanhdacbiet[k].value = "true";
                        }
                    }
                    ds_f1.splice(j, 1);
                }
            }
            ds_f1.push(rows[i]);
        }
    
        if(!ds_f1[0]){
            ds_f1 = [...ds_f1.slice(1)];
        }
    
        return ds_f1;
    } catch (error) {
        
    }
}

module.exports.onHandleObjectNCHCDB = async (req) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            NguyCoHCDB,
        } = new Model({ sqlConn });

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, dotuoitu, dotuoiden
        } = req.query

        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            dotuoitu: dotuoitu ? dotuoitu : null,
            dotuoiden: dotuoiden ? dotuoiden : null,
        }; 

        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()

        function CalculatorAge(fromAge, toAge){
            const currentDate = new Date();
            const from_ = currentDate.getFullYear() - fromAge;
            const to_ = currentDate.getFullYear() - toAge;
            const tuoiTu = new Date(`${from_}-01-01`);
            const tuoiDen = new Date(`${to_}-01-01`);
            const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
            const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
            return [tT,tD];
        }

        function aboutAge(a, b){
            let temp = "";
            if(a != null && b != null){
                const [from, to] = CalculatorAge(a,b);
                temp = "(ngaysinh BETWEEN '" + from + "' AND '" + to + "')";
            }else if(a != null && b == null){
                /* TH fromAge Exists AND toAge undefined */
                const from_ = currentDate.getFullYear() - a;
                const tuoiTu = new Date(`${from_}-01-01`);
                const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
                temp = "(ngaysinh BETWEEN '" + tT + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                /* TH fromAge undefined AND toAge Exists */
                const to_ = currentDate.getFullYear() - a;
                const tuoiDen = new Date(`${to_}-01-01`);
                const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + tD + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        function whereSearch(Object){
            let agesFrom = Object.dotuoitu;
            let comeOfAge = Object.dotuoiden;
            let search = "";
            for(const k in Object){
                if(Object[k]  != null && k != 'dotuoitu' && k != 'dotuoiden'){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }

            if(empty(search)){
                search = aboutAge(agesFrom, comeOfAge);
            }else{
                if(aboutAge(agesFrom, comeOfAge) == ""){
                    search += "";
                }else{
                    search += " AND " + aboutAge(agesFrom, comeOfAge);
                }
            }

            if(empty(search)){
                search = " 1 ";
            }


            return search;
        }

        //---------------------------------------------------------------------------
        // 
        
        const [rows, fields] = await NguyCoHCDB.searchData_NCHCDB(whereSearch(Object));
        const getData_DM_NCHCDB = await NguyCoHCDB.getData_DM_NCHCDB();
        const searchData_DM_CT_NCHCDB = await NguyCoHCDB.searchData_DM_CT_NCHCDB(whereSearch(Object));

        var nguycodacbiet = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false", "false", "false"];

            if(searchData_DM_CT_NCHCDB[0][i].cha == 0){

                var dcha1 = searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
               if(dcha1 > 9){
                   dcha = dcha1;
               }else{
                    dcha =  searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: getData_DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }else{
                var dfield = ["false", "false", "false", "false", "false", "false", "false"];

                var dcha1 = searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =  searchData_DM_CT_NCHCDB[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }

                rows[i].nguycoHCDB = [];

                for(var j = 0; j < dfield.length; j++){
                    rows[i].nguycoHCDB.push({
                        name: getData_DM_NCHCDB[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
            nguycodacbiet.push(rows[i]);
        }

        var listObjectNonDuplicate = [];
        listObjectNonDuplicate.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < listObjectNonDuplicate.length; j ++){
                if(rows[i].id_treem == listObjectNonDuplicate[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(listObjectNonDuplicate[j].nguycoHCDB[k].value == "true"){
                            rows[i].nguycoHCDB[k].value = "true";
                        }
                    }
                    listObjectNonDuplicate.splice(j, 1);
                }
            }
            listObjectNonDuplicate.push(rows[i]);
        }

        if(!listObjectNonDuplicate[0]){
            listObjectNonDuplicate = [...listObjectNonDuplicate.slice(1)];
        }

        return listObjectNonDuplicate;
    } catch (error) {
        
    }
}

module.exports.onHandleObjectHCK = async (req) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            HoanCanhKhac,
        } = new Model({ sqlConn });

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, dotuoitu, dotuoiden
        } = req.query
    
        
        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            dotuoitu: dotuoitu ? dotuoitu : null,
            dotuoiden: dotuoiden ? dotuoiden : null,
        };
    
        
        
        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()
    
        function CalculatorAge(fromAge, toAge){
            const currentDate = new Date();
            const from_ = currentDate.getFullYear() - fromAge;
            const to_ = currentDate.getFullYear() - toAge;
            const tuoiTu = new Date(`${from_}-01-01`);
            const tuoiDen = new Date(`${to_}-01-01`);
            const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
            const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
            return [tT,tD];
        }

        function aboutAge(a, b){
            let temp = "";
            if(a != null && b != null){
                const [from, to] = CalculatorAge(a,b);
                temp = "(ngaysinh BETWEEN '" + from + "' AND '" + to + "')";
            }else if(a != null && b == null){
                /* TH fromAge Exists AND toAge undefined */
                const from_ = currentDate.getFullYear() - a;
                const tuoiTu = new Date(`${from_}-01-01`);
                const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
                temp = "(ngaysinh BETWEEN '" + tT + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                /* TH fromAge undefined AND toAge Exists */
                const to_ = currentDate.getFullYear() - a;
                const tuoiDen = new Date(`${to_}-01-01`);
                const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + tD + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        function whereSearch(Object){
            let agesFrom = Object.dotuoitu;
            let comeOfAge = Object.dotuoiden;
            let search = "";
            for(const k in Object){
                if(Object[k]  != null && k != 'dotuoitu' && k != 'dotuoiden'){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }

            if(empty(search)){
                search = aboutAge(agesFrom, comeOfAge);
            }else{
                if(aboutAge(agesFrom, comeOfAge) == ""){
                    search += "";
                }else{
                    search += " AND " + aboutAge(agesFrom, comeOfAge);
                }
            }

            if(empty(search)){
                search = " 1 ";
            }


            return search;
        }

        //---------------------------------------------------------------------------
        
        
        const [rows, fields] = await HoanCanhKhac.searchData_HCK(whereSearch(Object));
        const getData_DM_HCK = await HoanCanhKhac.getData_DM_HCK();
        const searchData_DM_CT_HCK = await HoanCanhKhac.searchData_DM_CT_HCK(whereSearch(Object));

        var hoancanhkhac = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false"];

            if(searchData_DM_CT_HCK[0][i].cha == 0){
                var dcha1 =searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhkhac = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhkhac.push({
                        name: getData_DM_HCK[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }

                
            }else{
                var dfield = ["false", "false"];
                var dcha1 = searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = searchData_DM_CT_HCK[0][i].ten_hoancanh.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hoancanhkhac = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hoancanhkhac.push({
                        name: getData_DM_HCK[0][j].ten_hoancanh,
                        value: dfield[j],
                    });
                }
            }
        

            hoancanhkhac.push(rows[i]);
            //
        }

        var ds_f1 = [];
        ds_f1.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < ds_f1.length; j ++){
                if(rows[i].id_treem == ds_f1[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(ds_f1[j].hoancanhkhac[k].value == "true"){
                            rows[i].hoancanhkhac[k].value = "true";
                        }
                    }
                    ds_f1.splice(j, 1);
                }
            }
            ds_f1.push(rows[i]);
        }

        if(!ds_f1[0]){
            ds_f1 = [...ds_f1.slice(1)];
        }
        return ds_f1;
    } catch (error) {
        
    }
}

module.exports.onHandleObjectHTTG = async (req) => {
    try {
        const {
            sqlConn,
        } = req.app.locals;

        const {
            HinhThucTroGiup,
        } = new Model({ sqlConn });

        const {
            xa, huyen, thanhpho, tenxa, tenhuyen,
            tenthanhpho , thon, dotuoitu, dotuoiden
        } = req.query
    
        
        const Object = {
            id_tinhthanhpho: req.query.thanhpho ? req.query.thanhpho : null,
            id_quanhuyen: req.query.huyen ? req.query.huyen : null,
            id_phuongxa: req.query.xa ? req.query.xa : null,
            id_thon: thon ? thon : null,
            dotuoitu: dotuoitu ? dotuoitu : null,
            dotuoiden: dotuoiden ? dotuoiden : null,
        };
    
        
        
        var pastDate = "1900-01-01";
        var currentDate = new Date(); 
        var datetime =  currentDate.getFullYear() + "-" + (currentDate.getMonth()+1)  + "-" + currentDate.getDate()
    
        function CalculatorAge(fromAge, toAge){
            const currentDate = new Date();
            const from_ = currentDate.getFullYear() - fromAge;
            const to_ = currentDate.getFullYear() - toAge;
            const tuoiTu = new Date(`${from_}-01-01`);
            const tuoiDen = new Date(`${to_}-01-01`);
            const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
            const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
            return [tT,tD];
        }

        function aboutAge(a, b){
            let temp = "";
            if(a != null && b != null){
                const [from, to] = CalculatorAge(a,b);
                temp = "(ngaysinh BETWEEN '" + from + "' AND '" + to + "')";
            }else if(a != null && b == null){
                /* TH fromAge Exists AND toAge undefined */
                const from_ = currentDate.getFullYear() - a;
                const tuoiTu = new Date(`${from_}-01-01`);
                const tT =  tuoiTu.getFullYear() + "-" + (tuoiTu.getMonth()+1)  + "-" + tuoiTu.getDate();
                temp = "(ngaysinh BETWEEN '" + tT + "' AND '" + datetime + "')";
            }else if(a == null && b != null){
                /* TH fromAge undefined AND toAge Exists */
                const to_ = currentDate.getFullYear() - a;
                const tuoiDen = new Date(`${to_}-01-01`);
                const tD =  tuoiDen.getFullYear() + "-" + (tuoiDen.getMonth()+1)  + "-" + tuoiDen.getDate();
                temp = "(ngaysinh BETWEEN '" + pastDate + "' AND '" + tD + "')";
            }else if(a == null && b == null){
                temp = "";
            }
            return temp;
        }

        function whereSearch(Object){
            let agesFrom = Object.dotuoitu;
            let comeOfAge = Object.dotuoiden;
            let search = "";
            for(const k in Object){
                if(Object[k]  != null && k != 'dotuoitu' && k != 'dotuoiden'){
                    if(empty(search)){
                        if(k === 'hoten'){
                            search = k + " LIKE '%" + Object[k] + "%'";
                        }else if(k === 'tenchame'){
                            search = "(nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search = k + " = " + Object[k];
                        }    
                    }else{
                        if(k === 'hoten'){
                            search += " AND " + k + " LIKE '%" + Object[k] + "%'" ;
                        }else if(k === 'tenchame'){
                            search += " AND (nguoinuoiduong LIKE '%" + Object[k] + "%') OR (hotencha LIKE '%" + Object[k] + "%') OR (hotenme LIKE '%" + Object[k] + "%')";
                        }else{
                            search += " AND " + k + " = " + Object[k] ;
                        }  
                    }
                }
            }

            if(empty(search)){
                search = aboutAge(agesFrom, comeOfAge);
            }else{
                if(aboutAge(agesFrom, comeOfAge) == ""){
                    search += "";
                }else{
                    search += " AND " + aboutAge(agesFrom, comeOfAge);
                }
            }

            if(empty(search)){
                search = " 1 ";
            }


            return search;
        }

        //---------------------------------------------------------------------------
        
        
        const [rows, fields] = await HinhThucTroGiup.searchData_HTTG(whereSearch(Object));
        const getData_DM_HTTG = await HinhThucTroGiup.getData_DM_HTTG();
        const searchData_DM_CT_HTTG = await HinhThucTroGiup.searchData_DM_CT_HTTG(whereSearch(Object));

        var hinhthuctrogiup = [];
        
        for (var i = 0; i < rows.length; i++){
            var dfield = ["false", "false", "false", "false", "false"];

            if(searchData_DM_CT_HTTG[0][i].cha == 0){
                var dcha1 =searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha =searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hinhthuctrogiup = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hinhthuctrogiup.push({
                        name: getData_DM_HTTG[0][j].tenhinhthuc,
                        value: dfield[j],
                    });
                }

                
            }else{
                var dfield = ["false", "false", "false", "false", "false"];
                var dcha1 = searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 2);
                var dcha = 0;
                if(dcha1 > 9){
                    dcha = dcha1;
                }else{
                    dcha = searchData_DM_CT_HTTG[0][i].tenhinhthuc.substr(0, 1);
                }

                var dem = 1;
                for(var k = 0; k < dfield.length; k ++){
                    if(dcha == dem){
                        dfield[k] = "true";
                    }
                    dem = dem + 1;
                }
                
                rows[i].hinhthuctrogiup = [];
                for(var j = 0; j < dfield.length; j++){
                    rows[i].hinhthuctrogiup.push({
                        name: getData_DM_HTTG[0][j].tenhinhthuc,
                        value: dfield[j],
                    });
                }
            }
        

            hinhthuctrogiup.push(rows[i]);
            //
        }

        var listObjectNonDuplicate = [];
        listObjectNonDuplicate.push(rows[0]);
        for(var i = 1; i < rows.length; i ++){
            for(var j = 0; j < listObjectNonDuplicate.length; j ++){
                if(rows[i].id_treem == listObjectNonDuplicate[j].id_treem){
                    for(var k = 0; k < dfield.length; k++){
                        if(listObjectNonDuplicate[j].hinhthuctrogiup[k].value == "true"){
                            rows[i].hinhthuctrogiup[k].value = "true";
                        }
                    }
                    listObjectNonDuplicate.splice(j, 1);
                }
            }
            listObjectNonDuplicate.push(rows[i]);
        }

        if(!listObjectNonDuplicate[0]){
            listObjectNonDuplicate = [...listObjectNonDuplicate.slice(1)];
        }

        return listObjectNonDuplicate;
    } catch (error) {
        
    }
}