module.exports.columnsInfoChildrenOtherFamily = [
    {   
        header: 'Thôn/Xóm',
        key: 'tenthon',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'CMND của cha',
        key: 'cmndcha',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'CMND của mẹ',
        key: 'cmndme',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'CMND của Người nuôi dưỡng',
        key: 'cmndnguoinuoiduong',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Hộ nghèo',
        key: 'hongheo',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Hộ cận nghèo',
        key: 'hocanngheo',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Địa chỉ',
        key: 'diachi',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Họ và tên',
        key: 'hoten',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Ngày sinh',
        key: 'ngaysinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Dân tộc',
        key: 'dantoc',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Giới tính',
        key: 'gioitinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Trình độ học vấn',
        key: 'trinhdohocvan',
        width: 17,
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Ghi chú',
        key: 'ghichu',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
]

module.exports.columnsInfoChildren = [
    {   
        header: 'Mã gia đình',
        key: 'magiadinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Họ và tên',
        key: 'hoten',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Ngày sinh',
        key: 'ngaysinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Dân tộc',
        key: 'dantoc',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Giới tính',
        key: 'gioitinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Trình độ học vấn',
        key: 'trinhdohocvan',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Ghi chú',
        key: 'ghichu',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
]

module.exports.columnsInfoPerson = [
    {   
        header: 'Họ và tên',
        key: 'hoten',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Chứng minh nhân dân',
        key: 'cmnd',
        width: 20, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Ngày sinh',
        key: 'ngaysinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Dân tộc',
        key: 'dantoc',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Giới tính',
        key: 'gioitinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Số điện thoại',
        key: 'sodienthoai',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Trình độ học vấn',
        key: 'trinhdohocvan',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Ghi chú',
        key: 'ghichu',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
]

module.exports.columnsDownload = [
    {   
        header: 'Mã gia đình',
        key: 'id_giadinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Họ và tên',
        key: 'hoten',
        width: 30, 
        style: { alignment: { vertical:"middle", horizontal:"left" }  }
    },
    {   
        header: 'Ngày sinh',
        key: 'ngaysinh',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Dân tộc',
        key: 'dantoc',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Giới tính',
        key: 'gioitinh',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Họ tên cha',
        key: 'hotencha',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Họ tên mẹ',
        key: 'hotenme',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Người nuôi dưỡng',
        key: 'nguoinuoiduong',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Địa chỉ',
        key: 'diachi',
        width: 70, 
        style: { alignment: { vertical:"middle", wrapText:true }  }
    },

]

module.exports.columnsDownloadChildrenKTDL = [
    {   
        header: 'Mã gia đình',
        key: 'id_giadinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Mã trẻ em',
        key: 'id_treem',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Họ và tên',
        key: 'hoten',
        width: 30, 
        style: { alignment: { vertical:"middle", horizontal:"left" }  }
    },
    {   
        header: 'Ngày sinh',
        key: 'ngaysinh',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Học vấn',
        key: 'trinhdohocvan',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Dân tộc',
        key: 'dantoc',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Giới tính',
        key: 'gioitinh',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Họ tên cha',
        key: 'hotencha',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Họ tên mẹ',
        key: 'hotenme',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Người nuôi dưỡng',
        key: 'nguoinuoiduong',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Địa chỉ',
        key: 'diachi',
        width: 70, 
        style: { alignment: { vertical:"middle", wrapText:true }  }
    },

]

module.exports.columnsDownloadChildrenFollowObjectKTDL = [
    {   
        header: 'Mã trẻ em',
        key: 'id_treem',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Họ và tên',
        key: 'hoten',
        width: 30, 
        style: { alignment: { vertical:"middle", horizontal:"left" }  }
    },
    {   
        header: 'Ngày sinh',
        key: 'ngaysinh',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Dân tộc',
        key: 'dantoc',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Giới tính',
        key: 'gioitinh',
        width: 17, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Họ tên cha',
        key: 'hotencha',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Họ tên mẹ',
        key: 'hotenme',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
    {   
        header: 'Người nuôi dưỡng',
        key: 'nguoinuoiduong',
        width: 30, 
        style: { alignment: { vertical:"middle" }  }
    },
]

module.exports.columnsDownloadFamily = [
    {   
        header: 'Thôn/Xóm',
        key: 'tenthon',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Mã gia đình',
        key: 'id_giadinh',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Họ tên cha',
        key: 'hotencha',
        width: 25, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Họ tên mẹ',
        key: 'hotenme',
        width: 25, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Người nuôi dưỡng',
        key: 'nguoinuoiduong',
        width: 25, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Điện thoại',
        key: 'sodienthoai',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Hộ nghèo',
        key: 'hongheo',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Hộ cận nghèo',
        key: 'hocanngheo',
        width: 17, 
        style: { alignment: { vertical:"middle", horizontal:"center" }  }
    },
    {   
        header: 'Địa chỉ',
        key: 'diachi',
        width: 90, 
        style: { alignment: { vertical:"middle", horizontal:"center", wrapText:true }  }
    },

]

module.exports.dataFake = 
{
    magiadinh:123,
    hoten:'Trần Văn E',
    ngaysinh:'20/05/2014',
    dantoc:'Kinh',
    gioitinh:'Nam',
    trinhdohocvan:'Mẫu giáo',
    ghichu:'Ghi chú 1',
}

module.exports.dataFakeOtherFamily = 
{
    tenthon: 'Khu phố 1',
    cmndcha: '',
    cmdnme: '',
    cmndnguoinuoiduong: '',
    hongheo: 0,
    hocanngheo: 0,
    diachi: '',
    hoten:'Trần Văn Én',
    ngaysinh:'20/05/1978',
    dantoc:'Kinh',
    gioitinh:'Nam',
    trinhdohocvan:'',
    ghichu:'Ghi chú 1',
}

module.exports.dataFakePerson = 
{
    hoten:'Trần Văn Én',
    cmnd:'362484751',
    ngaysinh:'20/05/1978',
    dantoc:'Kinh',
    gioitinh:'Nam',
    trinhdohocvan:'',
    ghichu:'Ghi chú 1',
    sodienthoai: '0377553112'
}
